(function () {
  function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

  function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

  function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

  function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

  function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

  function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

  function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-todo-todo-module"], {
    /***/
    "./node_modules/@angular/cdk/fesm2015/drag-drop.js":
    /*!*********************************************************!*\
      !*** ./node_modules/@angular/cdk/fesm2015/drag-drop.js ***!
      \*********************************************************/

    /*! exports provided: CDK_DRAG_CONFIG, CDK_DRAG_CONFIG_FACTORY, CDK_DROP_LIST, CdkDrag, CdkDragHandle, CdkDragPlaceholder, CdkDragPreview, CdkDropList, CdkDropListGroup, DragDrop, DragDropModule, DragDropRegistry, DragRef, DropListRef, copyArrayItem, moveItemInArray, transferArrayItem, ɵangular_material_src_cdk_drag_drop_drag_drop_b */

    /***/
    function node_modulesAngularCdkFesm2015DragDropJs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CDK_DRAG_CONFIG", function () {
        return CDK_DRAG_CONFIG;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CDK_DRAG_CONFIG_FACTORY", function () {
        return CDK_DRAG_CONFIG_FACTORY;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CDK_DROP_LIST", function () {
        return CDK_DROP_LIST;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CdkDrag", function () {
        return CdkDrag;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CdkDragHandle", function () {
        return CdkDragHandle;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CdkDragPlaceholder", function () {
        return CdkDragPlaceholder;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CdkDragPreview", function () {
        return CdkDragPreview;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CdkDropList", function () {
        return CdkDropList;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CdkDropListGroup", function () {
        return CdkDropListGroup;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DragDrop", function () {
        return DragDrop;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DragDropModule", function () {
        return DragDropModule;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DragDropRegistry", function () {
        return DragDropRegistry;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DragRef", function () {
        return DragRef;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DropListRef", function () {
        return DropListRef;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "copyArrayItem", function () {
        return copyArrayItem;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "moveItemInArray", function () {
        return moveItemInArray;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "transferArrayItem", function () {
        return transferArrayItem;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ɵangular_material_src_cdk_drag_drop_drag_drop_b", function () {
        return CDK_DRAG_PARENT;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/fesm2015/common.js");
      /* harmony import */


      var _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/cdk/scrolling */
      "./node_modules/@angular/cdk/fesm2015/scrolling.js");
      /* harmony import */


      var _angular_cdk_platform__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/cdk/platform */
      "./node_modules/@angular/cdk/fesm2015/platform.js");
      /* harmony import */


      var _angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/cdk/coercion */
      "./node_modules/@angular/cdk/fesm2015/coercion.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/cdk/bidi */
      "./node_modules/@angular/cdk/fesm2015/bidi.js");
      /**
       * @fileoverview added by tsickle
       * Generated from: src/cdk/drag-drop/drag-styling.ts
       * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * @license
       * Copyright Google LLC All Rights Reserved.
       *
       * Use of this source code is governed by an MIT-style license that can be
       * found in the LICENSE file at https://angular.io/license
       */

      /**
       * Extended CSSStyleDeclaration that includes a couple of drag-related
       * properties that aren't in the built-in TS typings.
       * @record
       */


      function DragCSSStyleDeclaration() {}

      if (false) {}
      /**
       * Shallow-extends a stylesheet object with another stylesheet object.
       * \@docs-private
       * @param {?} dest
       * @param {?} source
       * @return {?}
       */


      function extendStyles(dest, source) {
        for (var key in source) {
          if (source.hasOwnProperty(key)) {
            dest[key] =
            /** @type {?} */
            source[key];
          }
        }

        return dest;
      }
      /**
       * Toggles whether the native drag interactions should be enabled for an element.
       * \@docs-private
       * @param {?} element Element on which to toggle the drag interactions.
       * @param {?} enable Whether the drag interactions should be enabled.
       * @return {?}
       */


      function toggleNativeDragInteractions(element, enable) {
        /** @type {?} */
        var userSelect = enable ? '' : 'none';
        extendStyles(element.style, {
          touchAction: enable ? '' : 'none',
          webkitUserDrag: enable ? '' : 'none',
          webkitTapHighlightColor: enable ? '' : 'transparent',
          userSelect: userSelect,
          msUserSelect: userSelect,
          webkitUserSelect: userSelect,
          MozUserSelect: userSelect
        });
      }
      /**
       * @fileoverview added by tsickle
       * Generated from: src/cdk/drag-drop/transition-duration.ts
       * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * @license
       * Copyright Google LLC All Rights Reserved.
       *
       * Use of this source code is governed by an MIT-style license that can be
       * found in the LICENSE file at https://angular.io/license
       */

      /**
       * Parses a CSS time value to milliseconds.
       * @param {?} value
       * @return {?}
       */


      function parseCssTimeUnitsToMs(value) {
        // Some browsers will return it in seconds, whereas others will return milliseconds.

        /** @type {?} */
        var multiplier = value.toLowerCase().indexOf('ms') > -1 ? 1 : 1000;
        return parseFloat(value) * multiplier;
      }
      /**
       * Gets the transform transition duration, including the delay, of an element in milliseconds.
       * @param {?} element
       * @return {?}
       */


      function getTransformTransitionDurationInMs(element) {
        /** @type {?} */
        var computedStyle = getComputedStyle(element);
        /** @type {?} */

        var transitionedProperties = parseCssPropertyValue(computedStyle, 'transition-property');
        /** @type {?} */

        var property = transitionedProperties.find(
        /**
        * @param {?} prop
        * @return {?}
        */
        function (prop) {
          return prop === 'transform' || prop === 'all';
        }); // If there's no transition for `all` or `transform`, we shouldn't do anything.

        if (!property) {
          return 0;
        } // Get the index of the property that we're interested in and match
        // it up to the same index in `transition-delay` and `transition-duration`.

        /** @type {?} */


        var propertyIndex = transitionedProperties.indexOf(property);
        /** @type {?} */

        var rawDurations = parseCssPropertyValue(computedStyle, 'transition-duration');
        /** @type {?} */

        var rawDelays = parseCssPropertyValue(computedStyle, 'transition-delay');
        return parseCssTimeUnitsToMs(rawDurations[propertyIndex]) + parseCssTimeUnitsToMs(rawDelays[propertyIndex]);
      }
      /**
       * Parses out multiple values from a computed style into an array.
       * @param {?} computedStyle
       * @param {?} name
       * @return {?}
       */


      function parseCssPropertyValue(computedStyle, name) {
        /** @type {?} */
        var value = computedStyle.getPropertyValue(name);
        return value.split(',').map(
        /**
        * @param {?} part
        * @return {?}
        */
        function (part) {
          return part.trim();
        });
      }
      /**
       * @fileoverview added by tsickle
       * Generated from: src/cdk/drag-drop/drag-ref.ts
       * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * Object that can be used to configure the behavior of DragRef.
       * @record
       */


      function DragRefConfig() {}

      if (false) {}
      /**
       * Options that can be used to bind a passive event listener.
       * @type {?}
       */


      var passiveEventListenerOptions = Object(_angular_cdk_platform__WEBPACK_IMPORTED_MODULE_3__["normalizePassiveListenerOptions"])({
        passive: true
      });
      /**
       * Options that can be used to bind an active event listener.
       * @type {?}
       */

      var activeEventListenerOptions = Object(_angular_cdk_platform__WEBPACK_IMPORTED_MODULE_3__["normalizePassiveListenerOptions"])({
        passive: false
      });
      /**
       * Time in milliseconds for which to ignore mouse events, after
       * receiving a touch event. Used to avoid doing double work for
       * touch devices where the browser fires fake mouse events, in
       * addition to touch events.
       * @type {?}
       */

      var MOUSE_EVENT_IGNORE_TIME = 800;
      /**
       * Internal compile-time-only representation of a `DragRef`.
       * Used to avoid circular import issues between the `DragRef` and the `DropListRef`.
       * \@docs-private
       * @record
       */

      function DragRefInternal() {}
      /**
       * Template that can be used to create a drag helper element (e.g. a preview or a placeholder).
       * @record
       * @template T
       */


      function DragHelperTemplate() {}

      if (false) {}
      /**
       * Reference to a draggable item. Used to manipulate or dispose of the item.
       * @template T
       */


      var DragRef = /*#__PURE__*/function () {
        /**
         * @param {?} element
         * @param {?} _config
         * @param {?} _document
         * @param {?} _ngZone
         * @param {?} _viewportRuler
         * @param {?} _dragDropRegistry
         */
        function DragRef(element, _config, _document, _ngZone, _viewportRuler, _dragDropRegistry) {
          var _this = this;

          _classCallCheck(this, DragRef);

          this._config = _config;
          this._document = _document;
          this._ngZone = _ngZone;
          this._viewportRuler = _viewportRuler;
          this._dragDropRegistry = _dragDropRegistry;
          /**
           * CSS `transform` applied to the element when it isn't being dragged. We need a
           * passive transform in order for the dragged element to retain its new position
           * after the user has stopped dragging and because we need to know the relative
           * position in case they start dragging again. This corresponds to `element.style.transform`.
           */

          this._passiveTransform = {
            x: 0,
            y: 0
          };
          /**
           * CSS `transform` that is applied to the element while it's being dragged.
           */

          this._activeTransform = {
            x: 0,
            y: 0
          };
          /**
           * Emits when the item is being moved.
           */

          this._moveEvents = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          /**
           * Subscription to pointer movement events.
           */

          this._pointerMoveSubscription = rxjs__WEBPACK_IMPORTED_MODULE_5__["Subscription"].EMPTY;
          /**
           * Subscription to the event that is dispatched when the user lifts their pointer.
           */

          this._pointerUpSubscription = rxjs__WEBPACK_IMPORTED_MODULE_5__["Subscription"].EMPTY;
          /**
           * Subscription to the viewport being scrolled.
           */

          this._scrollSubscription = rxjs__WEBPACK_IMPORTED_MODULE_5__["Subscription"].EMPTY;
          /**
           * Subscription to the viewport being resized.
           */

          this._resizeSubscription = rxjs__WEBPACK_IMPORTED_MODULE_5__["Subscription"].EMPTY;
          /**
           * Cached reference to the boundary element.
           */

          this._boundaryElement = null;
          /**
           * Whether the native dragging interactions have been enabled on the root element.
           */

          this._nativeInteractionsEnabled = true;
          /**
           * Elements that can be used to drag the draggable item.
           */

          this._handles = [];
          /**
           * Registered handles that are currently disabled.
           */

          this._disabledHandles = new Set();
          /**
           * Layout direction of the item.
           */

          this._direction = 'ltr';
          /**
           * Amount of milliseconds to wait after the user has put their
           * pointer down before starting to drag the element.
           */

          this.dragStartDelay = 0;
          this._disabled = false;
          /**
           * Emits as the drag sequence is being prepared.
           */

          this.beforeStarted = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          /**
           * Emits when the user starts dragging the item.
           */

          this.started = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          /**
           * Emits when the user has released a drag item, before any animations have started.
           */

          this.released = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          /**
           * Emits when the user stops dragging an item in the container.
           */

          this.ended = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          /**
           * Emits when the user has moved the item into a new container.
           */

          this.entered = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          /**
           * Emits when the user removes the item its container by dragging it into another container.
           */

          this.exited = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          /**
           * Emits when the user drops the item inside a container.
           */

          this.dropped = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          /**
           * Emits as the user is dragging the item. Use with caution,
           * because this event will fire for every pixel that the user has dragged.
           */

          this.moved = this._moveEvents.asObservable();
          /**
           * Handler for the `mousedown`/`touchstart` events.
           */

          this._pointerDown =
          /**
          * @param {?} event
          * @return {?}
          */
          function (event) {
            _this.beforeStarted.next(); // Delegate the event based on whether it started from a handle or the element itself.


            if (_this._handles.length) {
              /** @type {?} */
              var targetHandle = _this._handles.find(
              /**
              * @param {?} handle
              * @return {?}
              */
              function (handle) {
                /** @type {?} */
                var target = event.target;
                return !!target && (target === handle || handle.contains(
                /** @type {?} */
                target));
              });

              if (targetHandle && !_this._disabledHandles.has(targetHandle) && !_this.disabled) {
                _this._initializeDragSequence(targetHandle, event);
              }
            } else if (!_this.disabled) {
              _this._initializeDragSequence(_this._rootElement, event);
            }
          };
          /**
           * Handler that is invoked when the user moves their pointer after they've initiated a drag.
           */


          this._pointerMove =
          /**
          * @param {?} event
          * @return {?}
          */
          function (event) {
            // Prevent the default action as early as possible in order to block
            // native actions like dragging the selected text or images with the mouse.
            event.preventDefault();

            if (!_this._hasStartedDragging) {
              /** @type {?} */
              var pointerPosition = _this._getPointerPositionOnPage(event);
              /** @type {?} */


              var distanceX = Math.abs(pointerPosition.x - _this._pickupPositionOnPage.x);
              /** @type {?} */

              var distanceY = Math.abs(pointerPosition.y - _this._pickupPositionOnPage.y);
              /** @type {?} */

              var isOverThreshold = distanceX + distanceY >= _this._config.dragStartThreshold; // Only start dragging after the user has moved more than the minimum distance in either
              // direction. Note that this is preferrable over doing something like `skip(minimumDistance)`
              // in the `pointerMove` subscription, because we're not guaranteed to have one move event
              // per pixel of movement (e.g. if the user moves their pointer quickly).

              if (isOverThreshold) {
                /** @type {?} */
                var isDelayElapsed = Date.now() >= _this._dragStartTime + _this._getDragStartDelay(event);

                if (!isDelayElapsed) {
                  _this._endDragSequence(event);

                  return;
                } // Prevent other drag sequences from starting while something in the container is still
                // being dragged. This can happen while we're waiting for the drop animation to finish
                // and can cause errors, because some elements might still be moving around.


                if (!_this._dropContainer || !_this._dropContainer.isDragging()) {
                  _this._hasStartedDragging = true;

                  _this._ngZone.run(
                  /**
                  * @return {?}
                  */
                  function () {
                    return _this._startDragSequence(event);
                  });
                }
              }

              return;
            } // We only need the preview dimensions if we have a boundary element.


            if (_this._boundaryElement) {
              // Cache the preview element rect if we haven't cached it already or if
              // we cached it too early before the element dimensions were computed.
              if (!_this._previewRect || !_this._previewRect.width && !_this._previewRect.height) {
                _this._previewRect = (_this._preview || _this._rootElement).getBoundingClientRect();
              }
            }
            /** @type {?} */


            var constrainedPointerPosition = _this._getConstrainedPointerPosition(event);

            _this._hasMoved = true;

            _this._updatePointerDirectionDelta(constrainedPointerPosition);

            if (_this._dropContainer) {
              _this._updateActiveDropContainer(constrainedPointerPosition);
            } else {
              /** @type {?} */
              var activeTransform = _this._activeTransform;
              activeTransform.x = constrainedPointerPosition.x - _this._pickupPositionOnPage.x + _this._passiveTransform.x;
              activeTransform.y = constrainedPointerPosition.y - _this._pickupPositionOnPage.y + _this._passiveTransform.y;

              _this._applyRootElementTransform(activeTransform.x, activeTransform.y); // Apply transform as attribute if dragging and svg element to work for IE


              if (typeof SVGElement !== 'undefined' && _this._rootElement instanceof SVGElement) {
                /** @type {?} */
                var appliedTransform = "translate(".concat(activeTransform.x, " ").concat(activeTransform.y, ")");

                _this._rootElement.setAttribute('transform', appliedTransform);
              }
            } // Since this event gets fired for every pixel while dragging, we only
            // want to fire it if the consumer opted into it. Also we have to
            // re-enter the zone because we run all of the events on the outside.


            if (_this._moveEvents.observers.length) {
              _this._ngZone.run(
              /**
              * @return {?}
              */
              function () {
                _this._moveEvents.next({
                  source: _this,
                  pointerPosition: constrainedPointerPosition,
                  event: event,
                  distance: _this._getDragDistance(constrainedPointerPosition),
                  delta: _this._pointerDirectionDelta
                });
              });
            }
          };
          /**
           * Handler that is invoked when the user lifts their pointer up, after initiating a drag.
           */


          this._pointerUp =
          /**
          * @param {?} event
          * @return {?}
          */
          function (event) {
            _this._endDragSequence(event);
          };

          this.withRootElement(element);

          _dragDropRegistry.registerDragItem(this);
        }
        /**
         * Whether starting to drag this element is disabled.
         * @return {?}
         */


        _createClass(DragRef, [{
          key: "getPlaceholderElement",

          /**
           * Returns the element that is being used as a placeholder
           * while the current element is being dragged.
           * @return {?}
           */
          value: function getPlaceholderElement() {
            return this._placeholder;
          }
          /**
           * Returns the root draggable element.
           * @return {?}
           */

        }, {
          key: "getRootElement",
          value: function getRootElement() {
            return this._rootElement;
          }
          /**
           * Registers the handles that can be used to drag the element.
           * @template THIS
           * @this {THIS}
           * @param {?} handles
           * @return {THIS}
           */

        }, {
          key: "withHandles",
          value: function withHandles(handles) {
            /** @type {?} */
            this._handles = handles.map(
            /**
            * @param {?} handle
            * @return {?}
            */
            function (handle) {
              return Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceElement"])(handle);
            });

            /** @type {?} */
            this._handles.forEach(
            /**
            * @param {?} handle
            * @return {?}
            */
            function (handle) {
              return toggleNativeDragInteractions(handle, false);
            });

            /** @type {?} */
            this._toggleNativeDragInteractions();

            return (
              /** @type {?} */
              this
            );
          }
          /**
           * Registers the template that should be used for the drag preview.
           * @template THIS
           * @this {THIS}
           * @param {?} template Template that from which to stamp out the preview.
           * @return {THIS}
           */

        }, {
          key: "withPreviewTemplate",
          value: function withPreviewTemplate(template) {
            /** @type {?} */
            this._previewTemplate = template;
            return (
              /** @type {?} */
              this
            );
          }
          /**
           * Registers the template that should be used for the drag placeholder.
           * @template THIS
           * @this {THIS}
           * @param {?} template Template that from which to stamp out the placeholder.
           * @return {THIS}
           */

        }, {
          key: "withPlaceholderTemplate",
          value: function withPlaceholderTemplate(template) {
            /** @type {?} */
            this._placeholderTemplate = template;
            return (
              /** @type {?} */
              this
            );
          }
          /**
           * Sets an alternate drag root element. The root element is the element that will be moved as
           * the user is dragging. Passing an alternate root element is useful when trying to enable
           * dragging on an element that you might not have access to.
           * @template THIS
           * @this {THIS}
           * @param {?} rootElement
           * @return {THIS}
           */

        }, {
          key: "withRootElement",
          value: function withRootElement(rootElement) {
            /** @type {?} */
            var element = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceElement"])(rootElement);

            if (element !==
            /** @type {?} */
            this._rootElement) {
              if (
              /** @type {?} */
              this._rootElement) {
                /** @type {?} */
                this._removeRootElementListeners(
                /** @type {?} */
                this._rootElement);
              }

              element.addEventListener('mousedown',
              /** @type {?} */
              this._pointerDown, activeEventListenerOptions);
              element.addEventListener('touchstart',
              /** @type {?} */
              this._pointerDown, passiveEventListenerOptions);

              /** @type {?} */
              this._initialTransform = undefined;

              /** @type {?} */
              this._rootElement = element;
            }

            return (
              /** @type {?} */
              this
            );
          }
          /**
           * Element to which the draggable's position will be constrained.
           * @template THIS
           * @this {THIS}
           * @param {?} boundaryElement
           * @return {THIS}
           */

        }, {
          key: "withBoundaryElement",
          value: function withBoundaryElement(boundaryElement) {
            var _this2 = this;

            /** @type {?} */
            this._boundaryElement = boundaryElement ? Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceElement"])(boundaryElement) : null;

            /** @type {?} */
            this._resizeSubscription.unsubscribe();

            if (boundaryElement) {
              /** @type {?} */
              this._resizeSubscription =
              /** @type {?} */
              this._viewportRuler.change(10).subscribe(
              /**
              * @return {?}
              */
              function () {
                return (
                  /** @type {?} */
                  _this2._containInsideBoundaryOnResize()
                );
              });
            }

            return (
              /** @type {?} */
              this
            );
          }
          /**
           * Removes the dragging functionality from the DOM element.
           * @return {?}
           */

        }, {
          key: "dispose",
          value: function dispose() {
            this._removeRootElementListeners(this._rootElement); // Do this check before removing from the registry since it'll
            // stop being considered as dragged once it is removed.


            if (this.isDragging()) {
              // Since we move out the element to the end of the body while it's being
              // dragged, we have to make sure that it's removed if it gets destroyed.
              removeNode(this._rootElement);
            }

            removeNode(this._anchor);

            this._destroyPreview();

            this._destroyPlaceholder();

            this._dragDropRegistry.removeDragItem(this);

            this._removeSubscriptions();

            this.beforeStarted.complete();
            this.started.complete();
            this.released.complete();
            this.ended.complete();
            this.entered.complete();
            this.exited.complete();
            this.dropped.complete();

            this._moveEvents.complete();

            this._handles = [];

            this._disabledHandles.clear();

            this._dropContainer = undefined;

            this._resizeSubscription.unsubscribe();

            this._boundaryElement = this._rootElement = this._placeholderTemplate = this._previewTemplate = this._anchor =
            /** @type {?} */
            null;
          }
          /**
           * Checks whether the element is currently being dragged.
           * @return {?}
           */

        }, {
          key: "isDragging",
          value: function isDragging() {
            return this._hasStartedDragging && this._dragDropRegistry.isDragging(this);
          }
          /**
           * Resets a standalone drag item to its initial position.
           * @return {?}
           */

        }, {
          key: "reset",
          value: function reset() {
            this._rootElement.style.transform = this._initialTransform || '';
            this._activeTransform = {
              x: 0,
              y: 0
            };
            this._passiveTransform = {
              x: 0,
              y: 0
            };
          }
          /**
           * Sets a handle as disabled. While a handle is disabled, it'll capture and interrupt dragging.
           * @param {?} handle Handle element that should be disabled.
           * @return {?}
           */

        }, {
          key: "disableHandle",
          value: function disableHandle(handle) {
            if (this._handles.indexOf(handle) > -1) {
              this._disabledHandles.add(handle);
            }
          }
          /**
           * Enables a handle, if it has been disabled.
           * @param {?} handle Handle element to be enabled.
           * @return {?}
           */

        }, {
          key: "enableHandle",
          value: function enableHandle(handle) {
            this._disabledHandles["delete"](handle);
          }
          /**
           * Sets the layout direction of the draggable item.
           * @template THIS
           * @this {THIS}
           * @param {?} direction
           * @return {THIS}
           */

        }, {
          key: "withDirection",
          value: function withDirection(direction) {
            /** @type {?} */
            this._direction = direction;
            return (
              /** @type {?} */
              this
            );
          }
          /**
           * Sets the container that the item is part of.
           * @param {?} container
           * @return {?}
           */

        }, {
          key: "_withDropContainer",
          value: function _withDropContainer(container) {
            this._dropContainer = container;
          }
          /**
           * Gets the current position in pixels the draggable outside of a drop container.
           * @return {?}
           */

        }, {
          key: "getFreeDragPosition",
          value: function getFreeDragPosition() {
            /** @type {?} */
            var position = this.isDragging() ? this._activeTransform : this._passiveTransform;
            return {
              x: position.x,
              y: position.y
            };
          }
          /**
           * Sets the current position in pixels the draggable outside of a drop container.
           * @template THIS
           * @this {THIS}
           * @param {?} value New position to be set.
           * @return {THIS}
           */

        }, {
          key: "setFreeDragPosition",
          value: function setFreeDragPosition(value) {
            /** @type {?} */
            this._activeTransform = {
              x: 0,
              y: 0
            };

            /** @type {?} */
            this._passiveTransform.x = value.x;

            /** @type {?} */
            this._passiveTransform.y = value.y;

            if (!
            /** @type {?} */
            this._dropContainer) {
              /** @type {?} */
              this._applyRootElementTransform(value.x, value.y);
            }

            return (
              /** @type {?} */
              this
            );
          }
          /**
           * Updates the item's sort order based on the last-known pointer position.
           * @return {?}
           */

        }, {
          key: "_sortFromLastPointerPosition",
          value: function _sortFromLastPointerPosition() {
            /** @type {?} */
            var position = this._pointerPositionAtLastDirectionChange;

            if (position && this._dropContainer) {
              this._updateActiveDropContainer(position);
            }
          }
          /**
           * Unsubscribes from the global subscriptions.
           * @private
           * @return {?}
           */

        }, {
          key: "_removeSubscriptions",
          value: function _removeSubscriptions() {
            this._pointerMoveSubscription.unsubscribe();

            this._pointerUpSubscription.unsubscribe();

            this._scrollSubscription.unsubscribe();
          }
          /**
           * Destroys the preview element and its ViewRef.
           * @private
           * @return {?}
           */

        }, {
          key: "_destroyPreview",
          value: function _destroyPreview() {
            if (this._preview) {
              removeNode(this._preview);
            }

            if (this._previewRef) {
              this._previewRef.destroy();
            }

            this._preview = this._previewRef =
            /** @type {?} */
            null;
          }
          /**
           * Destroys the placeholder element and its ViewRef.
           * @private
           * @return {?}
           */

        }, {
          key: "_destroyPlaceholder",
          value: function _destroyPlaceholder() {
            if (this._placeholder) {
              removeNode(this._placeholder);
            }

            if (this._placeholderRef) {
              this._placeholderRef.destroy();
            }

            this._placeholder = this._placeholderRef =
            /** @type {?} */
            null;
          }
          /**
           * Clears subscriptions and stops the dragging sequence.
           * @private
           * @param {?} event Browser event object that ended the sequence.
           * @return {?}
           */

        }, {
          key: "_endDragSequence",
          value: function _endDragSequence(event) {
            var _this3 = this;

            // Note that here we use `isDragging` from the service, rather than from `this`.
            // The difference is that the one from the service reflects whether a dragging sequence
            // has been initiated, whereas the one on `this` includes whether the user has passed
            // the minimum dragging threshold.
            if (!this._dragDropRegistry.isDragging(this)) {
              return;
            }

            this._removeSubscriptions();

            this._dragDropRegistry.stopDragging(this);

            this._toggleNativeDragInteractions();

            if (this._handles) {
              this._rootElement.style.webkitTapHighlightColor = this._rootElementTapHighlight;
            }

            if (!this._hasStartedDragging) {
              return;
            }

            this.released.next({
              source: this
            });

            if (this._dropContainer) {
              // Stop scrolling immediately, instead of waiting for the animation to finish.
              this._dropContainer._stopScrolling();

              this._animatePreviewToPlaceholder().then(
              /**
              * @return {?}
              */
              function () {
                _this3._cleanupDragArtifacts(event);

                _this3._cleanupCachedDimensions();

                _this3._dragDropRegistry.stopDragging(_this3);
              });
            } else {
              // Convert the active transform into a passive one. This means that next time
              // the user starts dragging the item, its position will be calculated relatively
              // to the new passive transform.
              this._passiveTransform.x = this._activeTransform.x;
              this._passiveTransform.y = this._activeTransform.y;

              this._ngZone.run(
              /**
              * @return {?}
              */
              function () {
                _this3.ended.next({
                  source: _this3,
                  distance: _this3._getDragDistance(_this3._getPointerPositionOnPage(event))
                });
              });

              this._cleanupCachedDimensions();

              this._dragDropRegistry.stopDragging(this);
            }
          }
          /**
           * Starts the dragging sequence.
           * @private
           * @param {?} event
           * @return {?}
           */

        }, {
          key: "_startDragSequence",
          value: function _startDragSequence(event) {
            // Emit the event on the item before the one on the container.
            this.started.next({
              source: this
            });

            if (isTouchEvent(event)) {
              this._lastTouchEventTime = Date.now();
            }

            this._toggleNativeDragInteractions();

            if (this._dropContainer) {
              /** @type {?} */
              var element = this._rootElement;
              /** @type {?} */

              var parent =
              /** @type {?} */
              element.parentNode;
              /** @type {?} */

              var preview = this._preview = this._createPreviewElement();
              /** @type {?} */


              var placeholder = this._placeholder = this._createPlaceholderElement();
              /** @type {?} */


              var anchor = this._anchor = this._anchor || this._document.createComment(''); // Insert an anchor node so that we can restore the element's position in the DOM.


              parent.insertBefore(anchor, element); // We move the element out at the end of the body and we make it hidden, because keeping it in
              // place will throw off the consumer's `:last-child` selectors. We can't remove the element
              // from the DOM completely, because iOS will stop firing all subsequent events in the chain.

              element.style.display = 'none';

              this._document.body.appendChild(parent.replaceChild(placeholder, element));

              getPreviewInsertionPoint(this._document).appendChild(preview);

              this._dropContainer.start();
            }
          }
          /**
           * Sets up the different variables and subscriptions
           * that will be necessary for the dragging sequence.
           * @private
           * @param {?} referenceElement Element that started the drag sequence.
           * @param {?} event Browser event object that started the sequence.
           * @return {?}
           */

        }, {
          key: "_initializeDragSequence",
          value: function _initializeDragSequence(referenceElement, event) {
            var _this4 = this;

            // Always stop propagation for the event that initializes
            // the dragging sequence, in order to prevent it from potentially
            // starting another sequence for a draggable parent somewhere up the DOM tree.
            event.stopPropagation();
            /** @type {?} */

            var isDragging = this.isDragging();
            /** @type {?} */

            var isTouchSequence = isTouchEvent(event);
            /** @type {?} */

            var isAuxiliaryMouseButton = !isTouchSequence &&
            /** @type {?} */
            event.button !== 0;
            /** @type {?} */

            var rootElement = this._rootElement;
            /** @type {?} */

            var isSyntheticEvent = !isTouchSequence && this._lastTouchEventTime && this._lastTouchEventTime + MOUSE_EVENT_IGNORE_TIME > Date.now(); // If the event started from an element with the native HTML drag&drop, it'll interfere
            // with our own dragging (e.g. `img` tags do it by default). Prevent the default action
            // to stop it from happening. Note that preventing on `dragstart` also seems to work, but
            // it's flaky and it fails if the user drags it away quickly. Also note that we only want
            // to do this for `mousedown` since doing the same for `touchstart` will stop any `click`
            // events from firing on touch devices.

            if (event.target &&
            /** @type {?} */
            event.target.draggable && event.type === 'mousedown') {
              event.preventDefault();
            } // Abort if the user is already dragging or is using a mouse button other than the primary one.


            if (isDragging || isAuxiliaryMouseButton || isSyntheticEvent) {
              return;
            } // If we've got handles, we need to disable the tap highlight on the entire root element,
            // otherwise iOS will still add it, even though all the drag interactions on the handle
            // are disabled.


            if (this._handles.length) {
              this._rootElementTapHighlight = rootElement.style.webkitTapHighlightColor;
              rootElement.style.webkitTapHighlightColor = 'transparent';
            }

            this._hasStartedDragging = this._hasMoved = false;
            this._initialContainer =
            /** @type {?} */
            this._dropContainer; // Avoid multiple subscriptions and memory leaks when multi touch
            // (isDragging check above isn't enough because of possible temporal and/or dimensional delays)

            this._removeSubscriptions();

            this._pointerMoveSubscription = this._dragDropRegistry.pointerMove.subscribe(this._pointerMove);
            this._pointerUpSubscription = this._dragDropRegistry.pointerUp.subscribe(this._pointerUp);
            this._scrollSubscription = this._dragDropRegistry.scroll.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["startWith"])(null)).subscribe(
            /**
            * @return {?}
            */
            function () {
              _this4._scrollPosition = _this4._viewportRuler.getViewportScrollPosition();
            });

            if (this._boundaryElement) {
              this._boundaryRect = this._boundaryElement.getBoundingClientRect();
            } // If we have a custom preview template, the element won't be visible anyway so we avoid the
            // extra `getBoundingClientRect` calls and just move the preview next to the cursor.


            this._pickupPositionInElement = this._previewTemplate && this._previewTemplate.template ? {
              x: 0,
              y: 0
            } : this._getPointerPositionInElement(referenceElement, event);
            /** @type {?} */

            var pointerPosition = this._pickupPositionOnPage = this._getPointerPositionOnPage(event);

            this._pointerDirectionDelta = {
              x: 0,
              y: 0
            };
            this._pointerPositionAtLastDirectionChange = {
              x: pointerPosition.x,
              y: pointerPosition.y
            };
            this._dragStartTime = Date.now();

            this._dragDropRegistry.startDragging(this, event);
          }
          /**
           * Cleans up the DOM artifacts that were added to facilitate the element being dragged.
           * @private
           * @param {?} event
           * @return {?}
           */

        }, {
          key: "_cleanupDragArtifacts",
          value: function _cleanupDragArtifacts(event) {
            var _this5 = this;

            // Restore the element's visibility and insert it at its old position in the DOM.
            // It's important that we maintain the position, because moving the element around in the DOM
            // can throw off `NgFor` which does smart diffing and re-creates elements only when necessary,
            // while moving the existing elements in all other cases.
            this._rootElement.style.display = '';

            /** @type {?} */
            this._anchor.parentNode.replaceChild(this._rootElement, this._anchor);

            this._destroyPreview();

            this._destroyPlaceholder();

            this._boundaryRect = this._previewRect = undefined; // Re-enter the NgZone since we bound `document` events on the outside.

            this._ngZone.run(
            /**
            * @return {?}
            */
            function () {
              /** @type {?} */
              var container =
              /** @type {?} */
              _this5._dropContainer;
              /** @type {?} */

              var currentIndex = container.getItemIndex(_this5);
              /** @type {?} */

              var pointerPosition = _this5._getPointerPositionOnPage(event);
              /** @type {?} */


              var distance = _this5._getDragDistance(_this5._getPointerPositionOnPage(event));
              /** @type {?} */


              var isPointerOverContainer = container._isOverContainer(pointerPosition.x, pointerPosition.y);

              _this5.ended.next({
                source: _this5,
                distance: distance
              });

              _this5.dropped.next({
                item: _this5,
                currentIndex: currentIndex,
                previousIndex: _this5._initialContainer.getItemIndex(_this5),
                container: container,
                previousContainer: _this5._initialContainer,
                isPointerOverContainer: isPointerOverContainer,
                distance: distance
              });

              container.drop(_this5, currentIndex, _this5._initialContainer, isPointerOverContainer, distance);
              _this5._dropContainer = _this5._initialContainer;
            });
          }
          /**
           * Updates the item's position in its drop container, or moves it
           * into a new one, depending on its current drag position.
           * @private
           * @param {?} __0
           * @return {?}
           */

        }, {
          key: "_updateActiveDropContainer",
          value: function _updateActiveDropContainer(_ref) {
            var _this6 = this;

            var x = _ref.x,
                y = _ref.y;

            // Drop container that draggable has been moved into.

            /** @type {?} */
            var newContainer = this._initialContainer._getSiblingContainerFromPosition(this, x, y); // If we couldn't find a new container to move the item into, and the item has left its
            // initial container, check whether the it's over the initial container. This handles the
            // case where two containers are connected one way and the user tries to undo dragging an
            // item into a new container.


            if (!newContainer && this._dropContainer !== this._initialContainer && this._initialContainer._isOverContainer(x, y)) {
              newContainer = this._initialContainer;
            }

            if (newContainer && newContainer !== this._dropContainer) {
              this._ngZone.run(
              /**
              * @return {?}
              */
              function () {
                // Notify the old container that the item has left.
                _this6.exited.next({
                  item: _this6,
                  container:
                  /** @type {?} */
                  _this6._dropContainer
                });

                /** @type {?} */
                _this6._dropContainer.exit(_this6); // Notify the new container that the item has entered.


                _this6._dropContainer =
                /** @type {?} */
                newContainer;

                _this6._dropContainer.enter(_this6, x, y);

                _this6.entered.next({
                  item: _this6,
                  container:
                  /** @type {?} */
                  newContainer,
                  currentIndex:
                  /** @type {?} */
                  newContainer.getItemIndex(_this6)
                });
              });
            }

            /** @type {?} */
            this._dropContainer._startScrollingIfNecessary(x, y);

            /** @type {?} */
            this._dropContainer._sortItem(this, x, y, this._pointerDirectionDelta);

            this._preview.style.transform = getTransform(x - this._pickupPositionInElement.x, y - this._pickupPositionInElement.y);
          }
          /**
           * Creates the element that will be rendered next to the user's pointer
           * and will be used as a preview of the element that is being dragged.
           * @private
           * @return {?}
           */

        }, {
          key: "_createPreviewElement",
          value: function _createPreviewElement() {
            /** @type {?} */
            var previewConfig = this._previewTemplate;
            /** @type {?} */

            var previewClass = this.previewClass;
            /** @type {?} */

            var previewTemplate = previewConfig ? previewConfig.template : null;
            /** @type {?} */

            var preview;

            if (previewTemplate) {
              /** @type {?} */
              var viewRef =
              /** @type {?} */
              previewConfig.viewContainer.createEmbeddedView(previewTemplate,
              /** @type {?} */
              previewConfig.context);
              preview = getRootNode(viewRef, this._document);
              this._previewRef = viewRef;
              preview.style.transform = getTransform(this._pickupPositionOnPage.x, this._pickupPositionOnPage.y);
            } else {
              /** @type {?} */
              var element = this._rootElement;
              /** @type {?} */

              var elementRect = element.getBoundingClientRect();
              preview = deepCloneNode(element);
              preview.style.width = "".concat(elementRect.width, "px");
              preview.style.height = "".concat(elementRect.height, "px");
              preview.style.transform = getTransform(elementRect.left, elementRect.top);
            }

            extendStyles(preview.style, {
              // It's important that we disable the pointer events on the preview, because
              // it can throw off the `document.elementFromPoint` calls in the `CdkDropList`.
              pointerEvents: 'none',
              // We have to reset the margin, because it can throw off positioning relative to the viewport.
              margin: '0',
              position: 'fixed',
              top: '0',
              left: '0',
              zIndex: '1000'
            });
            toggleNativeDragInteractions(preview, false);
            preview.classList.add('cdk-drag-preview');
            preview.setAttribute('dir', this._direction);

            if (previewClass) {
              if (Array.isArray(previewClass)) {
                previewClass.forEach(
                /**
                * @param {?} className
                * @return {?}
                */
                function (className) {
                  return preview.classList.add(className);
                });
              } else {
                preview.classList.add(previewClass);
              }
            }

            return preview;
          }
          /**
           * Animates the preview element from its current position to the location of the drop placeholder.
           * @private
           * @return {?} Promise that resolves when the animation completes.
           */

        }, {
          key: "_animatePreviewToPlaceholder",
          value: function _animatePreviewToPlaceholder() {
            var _this7 = this;

            // If the user hasn't moved yet, the transitionend event won't fire.
            if (!this._hasMoved) {
              return Promise.resolve();
            }
            /** @type {?} */


            var placeholderRect = this._placeholder.getBoundingClientRect(); // Apply the class that adds a transition to the preview.


            this._preview.classList.add('cdk-drag-animating'); // Move the preview to the placeholder position.


            this._preview.style.transform = getTransform(placeholderRect.left, placeholderRect.top); // If the element doesn't have a `transition`, the `transitionend` event won't fire. Since
            // we need to trigger a style recalculation in order for the `cdk-drag-animating` class to
            // apply its style, we take advantage of the available info to figure out whether we need to
            // bind the event in the first place.

            /** @type {?} */

            var duration = getTransformTransitionDurationInMs(this._preview);

            if (duration === 0) {
              return Promise.resolve();
            }

            return this._ngZone.runOutsideAngular(
            /**
            * @return {?}
            */
            function () {
              return new Promise(
              /**
              * @param {?} resolve
              * @return {?}
              */
              function (resolve) {
                /** @type {?} */
                var handler =
                /** @type {?} */

                /**
                * @param {?} event
                * @return {?}
                */
                function handler(event) {
                  if (!event || event.target === _this7._preview && event.propertyName === 'transform') {
                    _this7._preview.removeEventListener('transitionend', handler);

                    resolve();
                    clearTimeout(timeout);
                  }
                }; // If a transition is short enough, the browser might not fire the `transitionend` event.
                // Since we know how long it's supposed to take, add a timeout with a 50% buffer that'll
                // fire if the transition hasn't completed when it was supposed to.

                /** @type {?} */


                var timeout = setTimeout(
                /** @type {?} */
                handler, duration * 1.5);

                _this7._preview.addEventListener('transitionend', handler);
              });
            });
          }
          /**
           * Creates an element that will be shown instead of the current element while dragging.
           * @private
           * @return {?}
           */

        }, {
          key: "_createPlaceholderElement",
          value: function _createPlaceholderElement() {
            /** @type {?} */
            var placeholderConfig = this._placeholderTemplate;
            /** @type {?} */

            var placeholderTemplate = placeholderConfig ? placeholderConfig.template : null;
            /** @type {?} */

            var placeholder;

            if (placeholderTemplate) {
              this._placeholderRef =
              /** @type {?} */
              placeholderConfig.viewContainer.createEmbeddedView(placeholderTemplate,
              /** @type {?} */
              placeholderConfig.context);
              placeholder = getRootNode(this._placeholderRef, this._document);
            } else {
              placeholder = deepCloneNode(this._rootElement);
            }

            placeholder.classList.add('cdk-drag-placeholder');
            return placeholder;
          }
          /**
           * Figures out the coordinates at which an element was picked up.
           * @private
           * @param {?} referenceElement Element that initiated the dragging.
           * @param {?} event Event that initiated the dragging.
           * @return {?}
           */

        }, {
          key: "_getPointerPositionInElement",
          value: function _getPointerPositionInElement(referenceElement, event) {
            /** @type {?} */
            var elementRect = this._rootElement.getBoundingClientRect();
            /** @type {?} */


            var handleElement = referenceElement === this._rootElement ? null : referenceElement;
            /** @type {?} */

            var referenceRect = handleElement ? handleElement.getBoundingClientRect() : elementRect;
            /** @type {?} */

            var point = isTouchEvent(event) ? event.targetTouches[0] : event;
            /** @type {?} */

            var x = point.pageX - referenceRect.left - this._scrollPosition.left;
            /** @type {?} */

            var y = point.pageY - referenceRect.top - this._scrollPosition.top;
            return {
              x: referenceRect.left - elementRect.left + x,
              y: referenceRect.top - elementRect.top + y
            };
          }
          /**
           * Determines the point of the page that was touched by the user.
           * @private
           * @param {?} event
           * @return {?}
           */

        }, {
          key: "_getPointerPositionOnPage",
          value: function _getPointerPositionOnPage(event) {
            // `touches` will be empty for start/end events so we have to fall back to `changedTouches`.

            /** @type {?} */
            var point = isTouchEvent(event) ? event.touches[0] || event.changedTouches[0] : event;
            return {
              x: point.pageX - this._scrollPosition.left,
              y: point.pageY - this._scrollPosition.top
            };
          }
          /**
           * Gets the pointer position on the page, accounting for any position constraints.
           * @private
           * @param {?} event
           * @return {?}
           */

        }, {
          key: "_getConstrainedPointerPosition",
          value: function _getConstrainedPointerPosition(event) {
            /** @type {?} */
            var point = this._getPointerPositionOnPage(event);
            /** @type {?} */


            var constrainedPoint = this.constrainPosition ? this.constrainPosition(point, this) : point;
            /** @type {?} */

            var dropContainerLock = this._dropContainer ? this._dropContainer.lockAxis : null;

            if (this.lockAxis === 'x' || dropContainerLock === 'x') {
              constrainedPoint.y = this._pickupPositionOnPage.y;
            } else if (this.lockAxis === 'y' || dropContainerLock === 'y') {
              constrainedPoint.x = this._pickupPositionOnPage.x;
            }

            if (this._boundaryRect) {
              var _this$_pickupPosition = this._pickupPositionInElement,
                  pickupX = _this$_pickupPosition.x,
                  pickupY = _this$_pickupPosition.y;
              /** @type {?} */

              var boundaryRect = this._boundaryRect;
              /** @type {?} */

              var previewRect =
              /** @type {?} */
              this._previewRect;
              /** @type {?} */

              var minY = boundaryRect.top + pickupY;
              /** @type {?} */

              var maxY = boundaryRect.bottom - (previewRect.height - pickupY);
              /** @type {?} */

              var minX = boundaryRect.left + pickupX;
              /** @type {?} */

              var maxX = boundaryRect.right - (previewRect.width - pickupX);
              constrainedPoint.x = clamp(constrainedPoint.x, minX, maxX);
              constrainedPoint.y = clamp(constrainedPoint.y, minY, maxY);
            }

            return constrainedPoint;
          }
          /**
           * Updates the current drag delta, based on the user's current pointer position on the page.
           * @private
           * @param {?} pointerPositionOnPage
           * @return {?}
           */

        }, {
          key: "_updatePointerDirectionDelta",
          value: function _updatePointerDirectionDelta(pointerPositionOnPage) {
            var x = pointerPositionOnPage.x,
                y = pointerPositionOnPage.y;
            /** @type {?} */

            var delta = this._pointerDirectionDelta;
            /** @type {?} */

            var positionSinceLastChange = this._pointerPositionAtLastDirectionChange; // Amount of pixels the user has dragged since the last time the direction changed.

            /** @type {?} */

            var changeX = Math.abs(x - positionSinceLastChange.x);
            /** @type {?} */

            var changeY = Math.abs(y - positionSinceLastChange.y); // Because we handle pointer events on a per-pixel basis, we don't want the delta
            // to change for every pixel, otherwise anything that depends on it can look erratic.
            // To make the delta more consistent, we track how much the user has moved since the last
            // delta change and we only update it after it has reached a certain threshold.

            if (changeX > this._config.pointerDirectionChangeThreshold) {
              delta.x = x > positionSinceLastChange.x ? 1 : -1;
              positionSinceLastChange.x = x;
            }

            if (changeY > this._config.pointerDirectionChangeThreshold) {
              delta.y = y > positionSinceLastChange.y ? 1 : -1;
              positionSinceLastChange.y = y;
            }

            return delta;
          }
          /**
           * Toggles the native drag interactions, based on how many handles are registered.
           * @private
           * @return {?}
           */

        }, {
          key: "_toggleNativeDragInteractions",
          value: function _toggleNativeDragInteractions() {
            if (!this._rootElement || !this._handles) {
              return;
            }
            /** @type {?} */


            var shouldEnable = this._handles.length > 0 || !this.isDragging();

            if (shouldEnable !== this._nativeInteractionsEnabled) {
              this._nativeInteractionsEnabled = shouldEnable;
              toggleNativeDragInteractions(this._rootElement, shouldEnable);
            }
          }
          /**
           * Removes the manually-added event listeners from the root element.
           * @private
           * @param {?} element
           * @return {?}
           */

        }, {
          key: "_removeRootElementListeners",
          value: function _removeRootElementListeners(element) {
            element.removeEventListener('mousedown', this._pointerDown, activeEventListenerOptions);
            element.removeEventListener('touchstart', this._pointerDown, passiveEventListenerOptions);
          }
          /**
           * Applies a `transform` to the root element, taking into account any existing transforms on it.
           * @private
           * @param {?} x New transform value along the X axis.
           * @param {?} y New transform value along the Y axis.
           * @return {?}
           */

        }, {
          key: "_applyRootElementTransform",
          value: function _applyRootElementTransform(x, y) {
            /** @type {?} */
            var transform = getTransform(x, y); // Cache the previous transform amount only after the first drag sequence, because
            // we don't want our own transforms to stack on top of each other.

            if (this._initialTransform == null) {
              this._initialTransform = this._rootElement.style.transform || '';
            } // Preserve the previous `transform` value, if there was one. Note that we apply our own
            // transform before the user's, because things like rotation can affect which direction
            // the element will be translated towards.


            this._rootElement.style.transform = this._initialTransform ? transform + ' ' + this._initialTransform : transform;
          }
          /**
           * Gets the distance that the user has dragged during the current drag sequence.
           * @private
           * @param {?} currentPosition Current position of the user's pointer.
           * @return {?}
           */

        }, {
          key: "_getDragDistance",
          value: function _getDragDistance(currentPosition) {
            /** @type {?} */
            var pickupPosition = this._pickupPositionOnPage;

            if (pickupPosition) {
              return {
                x: currentPosition.x - pickupPosition.x,
                y: currentPosition.y - pickupPosition.y
              };
            }

            return {
              x: 0,
              y: 0
            };
          }
          /**
           * Cleans up any cached element dimensions that we don't need after dragging has stopped.
           * @private
           * @return {?}
           */

        }, {
          key: "_cleanupCachedDimensions",
          value: function _cleanupCachedDimensions() {
            this._boundaryRect = this._previewRect = undefined;
          }
          /**
           * Checks whether the element is still inside its boundary after the viewport has been resized.
           * If not, the position is adjusted so that the element fits again.
           * @private
           * @return {?}
           */

        }, {
          key: "_containInsideBoundaryOnResize",
          value: function _containInsideBoundaryOnResize() {
            var _this$_passiveTransfo = this._passiveTransform,
                x = _this$_passiveTransfo.x,
                y = _this$_passiveTransfo.y;

            if (x === 0 && y === 0 || this.isDragging() || !this._boundaryElement) {
              return;
            }
            /** @type {?} */


            var boundaryRect = this._boundaryElement.getBoundingClientRect();
            /** @type {?} */


            var elementRect = this._rootElement.getBoundingClientRect(); // It's possible that the element got hidden away after dragging (e.g. by switching to a
            // different tab). Don't do anything in this case so we don't clear the user's position.


            if (boundaryRect.width === 0 && boundaryRect.height === 0 || elementRect.width === 0 && elementRect.height === 0) {
              return;
            }
            /** @type {?} */


            var leftOverflow = boundaryRect.left - elementRect.left;
            /** @type {?} */

            var rightOverflow = elementRect.right - boundaryRect.right;
            /** @type {?} */

            var topOverflow = boundaryRect.top - elementRect.top;
            /** @type {?} */

            var bottomOverflow = elementRect.bottom - boundaryRect.bottom; // If the element has become wider than the boundary, we can't
            // do much to make it fit so we just anchor it to the left.

            if (boundaryRect.width > elementRect.width) {
              if (leftOverflow > 0) {
                x += leftOverflow;
              }

              if (rightOverflow > 0) {
                x -= rightOverflow;
              }
            } else {
              x = 0;
            } // If the element has become taller than the boundary, we can't
            // do much to make it fit so we just anchor it to the top.


            if (boundaryRect.height > elementRect.height) {
              if (topOverflow > 0) {
                y += topOverflow;
              }

              if (bottomOverflow > 0) {
                y -= bottomOverflow;
              }
            } else {
              y = 0;
            }

            if (x !== this._passiveTransform.x || y !== this._passiveTransform.y) {
              this.setFreeDragPosition({
                y: y,
                x: x
              });
            }
          }
          /**
           * Gets the drag start delay, based on the event type.
           * @private
           * @param {?} event
           * @return {?}
           */

        }, {
          key: "_getDragStartDelay",
          value: function _getDragStartDelay(event) {
            /** @type {?} */
            var value = this.dragStartDelay;

            if (typeof value === 'number') {
              return value;
            } else if (isTouchEvent(event)) {
              return value.touch;
            }

            return value ? value.mouse : 0;
          }
        }, {
          key: "disabled",
          get: function get() {
            return this._disabled || !!(this._dropContainer && this._dropContainer.disabled);
          }
          /**
           * @param {?} value
           * @return {?}
           */
          ,
          set: function set(value) {
            /** @type {?} */
            var newValue = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceBooleanProperty"])(value);

            if (newValue !== this._disabled) {
              this._disabled = newValue;

              this._toggleNativeDragInteractions();
            }
          }
        }]);

        return DragRef;
      }();

      if (false) {}
      /**
       * Point on the page or within an element.
       * @record
       */


      function Point() {}

      if (false) {}
      /**
       * Gets a 3d `transform` that can be applied to an element.
       * @param {?} x Desired position of the element along the X axis.
       * @param {?} y Desired position of the element along the Y axis.
       * @return {?}
       */


      function getTransform(x, y) {
        // Round the transforms since some browsers will
        // blur the elements for sub-pixel transforms.
        return "translate3d(".concat(Math.round(x), "px, ").concat(Math.round(y), "px, 0)");
      }
      /**
       * Creates a deep clone of an element.
       * @param {?} node
       * @return {?}
       */


      function deepCloneNode(node) {
        /** @type {?} */
        var clone =
        /** @type {?} */
        node.cloneNode(true);
        /** @type {?} */

        var descendantsWithId = clone.querySelectorAll('[id]');
        /** @type {?} */

        var descendantCanvases = node.querySelectorAll('canvas'); // Remove the `id` to avoid having multiple elements with the same id on the page.

        clone.removeAttribute('id');

        for (var i = 0; i < descendantsWithId.length; i++) {
          descendantsWithId[i].removeAttribute('id');
        } // `cloneNode` won't transfer the content of `canvas` elements so we have to do it ourselves.
        // We match up the cloned canvas to their sources using their index in the DOM.


        if (descendantCanvases.length) {
          /** @type {?} */
          var cloneCanvases = clone.querySelectorAll('canvas');

          for (var _i = 0; _i < descendantCanvases.length; _i++) {
            /** @type {?} */
            var correspondingCloneContext = cloneCanvases[_i].getContext('2d');

            if (correspondingCloneContext) {
              correspondingCloneContext.drawImage(descendantCanvases[_i], 0, 0);
            }
          }
        }

        return clone;
      }
      /**
       * Clamps a value between a minimum and a maximum.
       * @param {?} value
       * @param {?} min
       * @param {?} max
       * @return {?}
       */


      function clamp(value, min, max) {
        return Math.max(min, Math.min(max, value));
      }
      /**
       * Helper to remove a node from the DOM and to do all the necessary null checks.
       * @param {?} node Node to be removed.
       * @return {?}
       */


      function removeNode(node) {
        if (node && node.parentNode) {
          node.parentNode.removeChild(node);
        }
      }
      /**
       * Determines whether an event is a touch event.
       * @param {?} event
       * @return {?}
       */


      function isTouchEvent(event) {
        // This function is called for every pixel that the user has dragged so we need it to be
        // as fast as possible. Since we only bind mouse events and touch events, we can assume
        // that if the event's name starts with `t`, it's a touch event.
        return event.type[0] === 't';
      }
      /**
       * Gets the element into which the drag preview should be inserted.
       * @param {?} documentRef
       * @return {?}
       */


      function getPreviewInsertionPoint(documentRef) {
        // We can't use the body if the user is in fullscreen mode,
        // because the preview will render under the fullscreen element.
        // TODO(crisbeto): dedupe this with the `FullscreenOverlayContainer` eventually.
        return documentRef.fullscreenElement || documentRef.webkitFullscreenElement || documentRef.mozFullScreenElement || documentRef.msFullscreenElement || documentRef.body;
      }
      /**
       * Gets the root HTML element of an embedded view.
       * If the root is not an HTML element it gets wrapped in one.
       * @param {?} viewRef
       * @param {?} _document
       * @return {?}
       */


      function getRootNode(viewRef, _document) {
        /** @type {?} */
        var rootNode = viewRef.rootNodes[0];

        if (rootNode.nodeType !== _document.ELEMENT_NODE) {
          /** @type {?} */
          var wrapper = _document.createElement('div');

          wrapper.appendChild(rootNode);
          return wrapper;
        }

        return (
          /** @type {?} */
          rootNode
        );
      }
      /**
       * @fileoverview added by tsickle
       * Generated from: src/cdk/drag-drop/drag-utils.ts
       * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * @license
       * Copyright Google LLC All Rights Reserved.
       *
       * Use of this source code is governed by an MIT-style license that can be
       * found in the LICENSE file at https://angular.io/license
       */

      /**
       * Moves an item one index in an array to another.
       * @template T
       * @param {?} array Array in which to move the item.
       * @param {?} fromIndex Starting index of the item.
       * @param {?} toIndex Index to which the item should be moved.
       * @return {?}
       */


      function moveItemInArray(array, fromIndex, toIndex) {
        /** @type {?} */
        var from = clamp$1(fromIndex, array.length - 1);
        /** @type {?} */

        var to = clamp$1(toIndex, array.length - 1);

        if (from === to) {
          return;
        }
        /** @type {?} */


        var target = array[from];
        /** @type {?} */

        var delta = to < from ? -1 : 1;

        for (var i = from; i !== to; i += delta) {
          array[i] = array[i + delta];
        }

        array[to] = target;
      }
      /**
       * Moves an item from one array to another.
       * @template T
       * @param {?} currentArray Array from which to transfer the item.
       * @param {?} targetArray Array into which to put the item.
       * @param {?} currentIndex Index of the item in its current array.
       * @param {?} targetIndex Index at which to insert the item.
       * @return {?}
       */


      function transferArrayItem(currentArray, targetArray, currentIndex, targetIndex) {
        /** @type {?} */
        var from = clamp$1(currentIndex, currentArray.length - 1);
        /** @type {?} */

        var to = clamp$1(targetIndex, targetArray.length);

        if (currentArray.length) {
          targetArray.splice(to, 0, currentArray.splice(from, 1)[0]);
        }
      }
      /**
       * Copies an item from one array to another, leaving it in its
       * original position in current array.
       * @template T
       * @param {?} currentArray Array from which to copy the item.
       * @param {?} targetArray Array into which is copy the item.
       * @param {?} currentIndex Index of the item in its current array.
       * @param {?} targetIndex Index at which to insert the item.
       *
       * @return {?}
       */


      function copyArrayItem(currentArray, targetArray, currentIndex, targetIndex) {
        /** @type {?} */
        var to = clamp$1(targetIndex, targetArray.length);

        if (currentArray.length) {
          targetArray.splice(to, 0, currentArray[currentIndex]);
        }
      }
      /**
       * Clamps a number between zero and a maximum.
       * @param {?} value
       * @param {?} max
       * @return {?}
       */


      function clamp$1(value, max) {
        return Math.max(0, Math.min(max, value));
      }
      /**
       * @fileoverview added by tsickle
       * Generated from: src/cdk/drag-drop/drop-list-ref.ts
       * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * Proximity, as a ratio to width/height, at which a
       * dragged item will affect the drop container.
       * @type {?}
       */


      var DROP_PROXIMITY_THRESHOLD = 0.05;
      /**
       * Proximity, as a ratio to width/height at which to start auto-scrolling the drop list or the
       * viewport. The value comes from trying it out manually until it feels right.
       * @type {?}
       */

      var SCROLL_PROXIMITY_THRESHOLD = 0.05;
      /**
       * Number of pixels to scroll for each frame when auto-scrolling an element.
       * The value comes from trying it out manually until it feels right.
       * @type {?}
       */

      var AUTO_SCROLL_STEP = 2;
      /**
       * Entry in the position cache for draggable items.
       * \@docs-private
       * @record
       */

      function CachedItemPosition() {}

      if (false) {}
      /**
       * Object holding the scroll position of something.
       * @record
       */


      function ScrollPosition() {}

      if (false) {}
      /** @enum {number} */


      var AutoScrollVerticalDirection = {
        NONE: 0,
        UP: 1,
        DOWN: 2
      };
      /** @enum {number} */

      var AutoScrollHorizontalDirection = {
        NONE: 0,
        LEFT: 1,
        RIGHT: 2
      };
      /**
       * Internal compile-time-only representation of a `DropListRef`.
       * Used to avoid circular import issues between the `DropListRef` and the `DragRef`.
       * \@docs-private
       * @record
       */

      function DropListRefInternal() {}
      /**
       * Reference to a drop list. Used to manipulate or dispose of the container.
       * @template T
       */


      var DropListRef = /*#__PURE__*/function () {
        /**
         * @param {?} element
         * @param {?} _dragDropRegistry
         * @param {?} _document
         * @param {?} _ngZone
         * @param {?} _viewportRuler
         */
        function DropListRef(element, _dragDropRegistry, _document, _ngZone, _viewportRuler) {
          var _this8 = this;

          _classCallCheck(this, DropListRef);

          this._dragDropRegistry = _dragDropRegistry;
          this._ngZone = _ngZone;
          this._viewportRuler = _viewportRuler;
          /**
           * Whether starting a dragging sequence from this container is disabled.
           */

          this.disabled = false;
          /**
           * Whether sorting items within the list is disabled.
           */

          this.sortingDisabled = false;
          /**
           * Whether auto-scrolling the view when the user
           * moves their pointer close to the edges is disabled.
           */

          this.autoScrollDisabled = false;
          /**
           * Function that is used to determine whether an item
           * is allowed to be moved into a drop container.
           */

          this.enterPredicate =
          /**
          * @return {?}
          */
          function () {
            return true;
          };
          /**
           * Emits right before dragging has started.
           */


          this.beforeStarted = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          /**
           * Emits when the user has moved a new drag item into this container.
           */

          this.entered = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          /**
           * Emits when the user removes an item from the container
           * by dragging it into another container.
           */

          this.exited = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          /**
           * Emits when the user drops an item inside the container.
           */

          this.dropped = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          /**
           * Emits as the user is swapping items while actively dragging.
           */

          this.sorted = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          /**
           * Whether an item in the list is being dragged.
           */

          this._isDragging = false;
          /**
           * Cache of the dimensions of all the items inside the container.
           */

          this._itemPositions = [];
          /**
           * Keeps track of the container's scroll position.
           */

          this._scrollPosition = {
            top: 0,
            left: 0
          };
          /**
           * Keeps track of the scroll position of the viewport.
           */

          this._viewportScrollPosition = {
            top: 0,
            left: 0
          };
          /**
           * Keeps track of the item that was last swapped with the dragged item, as
           * well as what direction the pointer was moving in when the swap occured.
           */

          this._previousSwap = {
            drag:
            /** @type {?} */
            null,
            delta: 0
          };
          /**
           * Drop lists that are connected to the current one.
           */

          this._siblings = [];
          /**
           * Direction in which the list is oriented.
           */

          this._orientation = 'vertical';
          /**
           * Connected siblings that currently have a dragged item.
           */

          this._activeSiblings = new Set();
          /**
           * Layout direction of the drop list.
           */

          this._direction = 'ltr';
          /**
           * Subscription to the window being scrolled.
           */

          this._viewportScrollSubscription = rxjs__WEBPACK_IMPORTED_MODULE_5__["Subscription"].EMPTY;
          /**
           * Vertical direction in which the list is currently scrolling.
           */

          this._verticalScrollDirection = 0
          /* NONE */
          ;
          /**
           * Horizontal direction in which the list is currently scrolling.
           */

          this._horizontalScrollDirection = 0
          /* NONE */
          ;
          /**
           * Used to signal to the current auto-scroll sequence when to stop.
           */

          this._stopScrollTimers = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          /**
           * Shadow root of the current element. Necessary for `elementFromPoint` to resolve correctly.
           */

          this._cachedShadowRoot = null;
          /**
           * Handles the container being scrolled. Has to be an arrow function to preserve the context.
           */

          this._handleScroll =
          /**
          * @return {?}
          */
          function () {
            if (!_this8.isDragging()) {
              return;
            }
            /** @type {?} */


            var element = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceElement"])(_this8.element);

            _this8._updateAfterScroll(_this8._scrollPosition, element.scrollTop, element.scrollLeft);
          };
          /**
           * Starts the interval that'll auto-scroll the element.
           */


          this._startScrollInterval =
          /**
          * @return {?}
          */
          function () {
            _this8._stopScrolling();

            Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["interval"])(0, rxjs__WEBPACK_IMPORTED_MODULE_5__["animationFrameScheduler"]).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["takeUntil"])(_this8._stopScrollTimers)).subscribe(
            /**
            * @return {?}
            */
            function () {
              /** @type {?} */
              var node = _this8._scrollNode;

              if (_this8._verticalScrollDirection === 1
              /* UP */
              ) {
                  incrementVerticalScroll(node, -AUTO_SCROLL_STEP);
                } else if (_this8._verticalScrollDirection === 2
              /* DOWN */
              ) {
                  incrementVerticalScroll(node, AUTO_SCROLL_STEP);
                }

              if (_this8._horizontalScrollDirection === 1
              /* LEFT */
              ) {
                  incrementHorizontalScroll(node, -AUTO_SCROLL_STEP);
                } else if (_this8._horizontalScrollDirection === 2
              /* RIGHT */
              ) {
                  incrementHorizontalScroll(node, AUTO_SCROLL_STEP);
                }
            });
          };

          this.element = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceElement"])(element);
          this._document = _document;

          _dragDropRegistry.registerDropContainer(this);
        }
        /**
         * Removes the drop list functionality from the DOM element.
         * @return {?}
         */


        _createClass(DropListRef, [{
          key: "dispose",
          value: function dispose() {
            this._stopScrolling();

            this._stopScrollTimers.complete();

            this._removeListeners();

            this.beforeStarted.complete();
            this.entered.complete();
            this.exited.complete();
            this.dropped.complete();
            this.sorted.complete();

            this._activeSiblings.clear();

            this._scrollNode =
            /** @type {?} */
            null;

            this._dragDropRegistry.removeDropContainer(this);
          }
          /**
           * Whether an item from this list is currently being dragged.
           * @return {?}
           */

        }, {
          key: "isDragging",
          value: function isDragging() {
            return this._isDragging;
          }
          /**
           * Starts dragging an item.
           * @return {?}
           */

        }, {
          key: "start",
          value: function start() {
            var _this9 = this;

            /** @type {?} */
            var element = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceElement"])(this.element);
            this.beforeStarted.next();
            this._isDragging = true;

            this._cacheItems();

            this._siblings.forEach(
            /**
            * @param {?} sibling
            * @return {?}
            */
            function (sibling) {
              return sibling._startReceiving(_this9);
            });

            this._removeListeners();

            this._ngZone.runOutsideAngular(
            /**
            * @return {?}
            */
            function () {
              return element.addEventListener('scroll', _this9._handleScroll);
            });

            this._listenToScrollEvents();
          }
          /**
           * Emits an event to indicate that the user moved an item into the container.
           * @param {?} item Item that was moved into the container.
           * @param {?} pointerX Position of the item along the X axis.
           * @param {?} pointerY Position of the item along the Y axis.
           * @return {?}
           */

        }, {
          key: "enter",
          value: function enter(item, pointerX, pointerY) {
            this.start(); // If sorting is disabled, we want the item to return to its starting
            // position if the user is returning it to its initial container.

            /** @type {?} */

            var newIndex = this.sortingDisabled ? this._draggables.indexOf(item) : -1;

            if (newIndex === -1) {
              // We use the coordinates of where the item entered the drop
              // zone to figure out at which index it should be inserted.
              newIndex = this._getItemIndexFromPointerPosition(item, pointerX, pointerY);
            }
            /** @type {?} */


            var activeDraggables = this._activeDraggables;
            /** @type {?} */

            var currentIndex = activeDraggables.indexOf(item);
            /** @type {?} */

            var placeholder = item.getPlaceholderElement();
            /** @type {?} */

            var newPositionReference = activeDraggables[newIndex]; // If the item at the new position is the same as the item that is being dragged,
            // it means that we're trying to restore the item to its initial position. In this
            // case we should use the next item from the list as the reference.

            if (newPositionReference === item) {
              newPositionReference = activeDraggables[newIndex + 1];
            } // Since the item may be in the `activeDraggables` already (e.g. if the user dragged it
            // into another container and back again), we have to ensure that it isn't duplicated.


            if (currentIndex > -1) {
              activeDraggables.splice(currentIndex, 1);
            } // Don't use items that are being dragged as a reference, because
            // their element has been moved down to the bottom of the body.


            if (newPositionReference && !this._dragDropRegistry.isDragging(newPositionReference)) {
              /** @type {?} */
              var element = newPositionReference.getRootElement();

              /** @type {?} */
              element.parentElement.insertBefore(placeholder, element);
              activeDraggables.splice(newIndex, 0, item);
            } else {
              Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceElement"])(this.element).appendChild(placeholder);
              activeDraggables.push(item);
            } // The transform needs to be cleared so it doesn't throw off the measurements.


            placeholder.style.transform = ''; // Note that the positions were already cached when we called `start` above,
            // but we need to refresh them since the amount of items has changed.

            this._cacheItemPositions();

            this.entered.next({
              item: item,
              container: this,
              currentIndex: this.getItemIndex(item)
            });
          }
          /**
           * Removes an item from the container after it was dragged into another container by the user.
           * @param {?} item Item that was dragged out.
           * @return {?}
           */

        }, {
          key: "exit",
          value: function exit(item) {
            this._reset();

            this.exited.next({
              item: item,
              container: this
            });
          }
          /**
           * Drops an item into this container.
           * @param {?} item Item being dropped into the container.
           * @param {?} currentIndex Index at which the item should be inserted.
           * @param {?} previousContainer Container from which the item got dragged in.
           * @param {?} isPointerOverContainer Whether the user's pointer was over the
           *    container when the item was dropped.
           * @param {?} distance Distance the user has dragged since the start of the dragging sequence.
           * @return {?}
           */

        }, {
          key: "drop",
          value: function drop(item, currentIndex, previousContainer, isPointerOverContainer, distance) {
            this._reset();

            this.dropped.next({
              item: item,
              currentIndex: currentIndex,
              previousIndex: previousContainer.getItemIndex(item),
              container: this,
              previousContainer: previousContainer,
              isPointerOverContainer: isPointerOverContainer,
              distance: distance
            });
          }
          /**
           * Sets the draggable items that are a part of this list.
           * @template THIS
           * @this {THIS}
           * @param {?} items Items that are a part of this list.
           * @return {THIS}
           */

        }, {
          key: "withItems",
          value: function withItems(items) {
            var _this10 = this;

            /** @type {?} */
            this._draggables = items;
            items.forEach(
            /**
            * @param {?} item
            * @return {?}
            */
            function (item) {
              return item._withDropContainer(
              /** @type {?} */
              _this10);
            });

            if (
            /** @type {?} */
            this.isDragging()) {
              /** @type {?} */
              this._cacheItems();
            }

            return (
              /** @type {?} */
              this
            );
          }
          /**
           * Sets the layout direction of the drop list.
           * @template THIS
           * @this {THIS}
           * @param {?} direction
           * @return {THIS}
           */

        }, {
          key: "withDirection",
          value: function withDirection(direction) {
            /** @type {?} */
            this._direction = direction;
            return (
              /** @type {?} */
              this
            );
          }
          /**
           * Sets the containers that are connected to this one. When two or more containers are
           * connected, the user will be allowed to transfer items between them.
           * @template THIS
           * @this {THIS}
           * @param {?} connectedTo Other containers that the current containers should be connected to.
           * @return {THIS}
           */

        }, {
          key: "connectedTo",
          value: function connectedTo(_connectedTo) {
            /** @type {?} */
            this._siblings = _connectedTo.slice();
            return (
              /** @type {?} */
              this
            );
          }
          /**
           * Sets the orientation of the container.
           * @template THIS
           * @this {THIS}
           * @param {?} orientation New orientation for the container.
           * @return {THIS}
           */

        }, {
          key: "withOrientation",
          value: function withOrientation(orientation) {
            /** @type {?} */
            this._orientation = orientation;
            return (
              /** @type {?} */
              this
            );
          }
          /**
           * Figures out the index of an item in the container.
           * @param {?} item Item whose index should be determined.
           * @return {?}
           */

        }, {
          key: "getItemIndex",
          value: function getItemIndex(item) {
            if (!this._isDragging) {
              return this._draggables.indexOf(item);
            } // Items are sorted always by top/left in the cache, however they flow differently in RTL.
            // The rest of the logic still stands no matter what orientation we're in, however
            // we need to invert the array when determining the index.

            /** @type {?} */


            var items = this._orientation === 'horizontal' && this._direction === 'rtl' ? this._itemPositions.slice().reverse() : this._itemPositions;
            return findIndex(items,
            /**
            * @param {?} currentItem
            * @return {?}
            */
            function (currentItem) {
              return currentItem.drag === item;
            });
          }
          /**
           * Whether the list is able to receive the item that
           * is currently being dragged inside a connected drop list.
           * @return {?}
           */

        }, {
          key: "isReceiving",
          value: function isReceiving() {
            return this._activeSiblings.size > 0;
          }
          /**
           * Sorts an item inside the container based on its position.
           * @param {?} item Item to be sorted.
           * @param {?} pointerX Position of the item along the X axis.
           * @param {?} pointerY Position of the item along the Y axis.
           * @param {?} pointerDelta Direction in which the pointer is moving along each axis.
           * @return {?}
           */

        }, {
          key: "_sortItem",
          value: function _sortItem(item, pointerX, pointerY, pointerDelta) {
            // Don't sort the item if sorting is disabled or it's out of range.
            if (this.sortingDisabled || !this._isPointerNearDropContainer(pointerX, pointerY)) {
              return;
            }
            /** @type {?} */


            var siblings = this._itemPositions;
            /** @type {?} */

            var newIndex = this._getItemIndexFromPointerPosition(item, pointerX, pointerY, pointerDelta);

            if (newIndex === -1 && siblings.length > 0) {
              return;
            }
            /** @type {?} */


            var isHorizontal = this._orientation === 'horizontal';
            /** @type {?} */

            var currentIndex = findIndex(siblings,
            /**
            * @param {?} currentItem
            * @return {?}
            */
            function (currentItem) {
              return currentItem.drag === item;
            });
            /** @type {?} */

            var siblingAtNewPosition = siblings[newIndex];
            /** @type {?} */

            var currentPosition = siblings[currentIndex].clientRect;
            /** @type {?} */

            var newPosition = siblingAtNewPosition.clientRect;
            /** @type {?} */

            var delta = currentIndex > newIndex ? 1 : -1;
            this._previousSwap.drag = siblingAtNewPosition.drag;
            this._previousSwap.delta = isHorizontal ? pointerDelta.x : pointerDelta.y; // How many pixels the item's placeholder should be offset.

            /** @type {?} */

            var itemOffset = this._getItemOffsetPx(currentPosition, newPosition, delta); // How many pixels all the other items should be offset.

            /** @type {?} */


            var siblingOffset = this._getSiblingOffsetPx(currentIndex, siblings, delta); // Save the previous order of the items before moving the item to its new index.
            // We use this to check whether an item has been moved as a result of the sorting.

            /** @type {?} */


            var oldOrder = siblings.slice(); // Shuffle the array in place.

            moveItemInArray(siblings, currentIndex, newIndex);
            this.sorted.next({
              previousIndex: currentIndex,
              currentIndex: newIndex,
              container: this,
              item: item
            });
            siblings.forEach(
            /**
            * @param {?} sibling
            * @param {?} index
            * @return {?}
            */
            function (sibling, index) {
              // Don't do anything if the position hasn't changed.
              if (oldOrder[index] === sibling) {
                return;
              }
              /** @type {?} */


              var isDraggedItem = sibling.drag === item;
              /** @type {?} */

              var offset = isDraggedItem ? itemOffset : siblingOffset;
              /** @type {?} */

              var elementToOffset = isDraggedItem ? item.getPlaceholderElement() : sibling.drag.getRootElement(); // Update the offset to reflect the new position.

              sibling.offset += offset; // Since we're moving the items with a `transform`, we need to adjust their cached
              // client rects to reflect their new position, as well as swap their positions in the cache.
              // Note that we shouldn't use `getBoundingClientRect` here to update the cache, because the
              // elements may be mid-animation which will give us a wrong result.

              if (isHorizontal) {
                // Round the transforms since some browsers will
                // blur the elements, for sub-pixel transforms.
                elementToOffset.style.transform = "translate3d(".concat(Math.round(sibling.offset), "px, 0, 0)");
                adjustClientRect(sibling.clientRect, 0, offset);
              } else {
                elementToOffset.style.transform = "translate3d(0, ".concat(Math.round(sibling.offset), "px, 0)");
                adjustClientRect(sibling.clientRect, offset, 0);
              }
            });
          }
          /**
           * Checks whether the user's pointer is close to the edges of either the
           * viewport or the drop list and starts the auto-scroll sequence.
           * @param {?} pointerX User's pointer position along the x axis.
           * @param {?} pointerY User's pointer position along the y axis.
           * @return {?}
           */

        }, {
          key: "_startScrollingIfNecessary",
          value: function _startScrollingIfNecessary(pointerX, pointerY) {
            if (this.autoScrollDisabled) {
              return;
            }
            /** @type {?} */


            var scrollNode;
            /** @type {?} */

            var verticalScrollDirection = 0
            /* NONE */
            ;
            /** @type {?} */

            var horizontalScrollDirection = 0
            /* NONE */
            ; // Check whether we should start scrolling the container.

            if (this._isPointerNearDropContainer(pointerX, pointerY)) {
              /** @type {?} */
              var element = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceElement"])(this.element);

              var _getElementScrollDire = getElementScrollDirections(element, this._clientRect, pointerX, pointerY);

              var _getElementScrollDire2 = _slicedToArray(_getElementScrollDire, 2);

              verticalScrollDirection = _getElementScrollDire2[0];
              horizontalScrollDirection = _getElementScrollDire2[1];

              if (verticalScrollDirection || horizontalScrollDirection) {
                scrollNode = element;
              }
            } // Otherwise check if we can start scrolling the viewport.


            if (!verticalScrollDirection && !horizontalScrollDirection) {
              var _this$_viewportRuler$ = this._viewportRuler.getViewportSize(),
                  width = _this$_viewportRuler$.width,
                  height = _this$_viewportRuler$.height;
              /** @type {?} */


              var clientRect = {
                width: width,
                height: height,
                top: 0,
                right: width,
                bottom: height,
                left: 0
              };
              verticalScrollDirection = getVerticalScrollDirection(clientRect, pointerY);
              horizontalScrollDirection = getHorizontalScrollDirection(clientRect, pointerX);
              scrollNode = window;
            }

            if (scrollNode && (verticalScrollDirection !== this._verticalScrollDirection || horizontalScrollDirection !== this._horizontalScrollDirection || scrollNode !== this._scrollNode)) {
              this._verticalScrollDirection = verticalScrollDirection;
              this._horizontalScrollDirection = horizontalScrollDirection;
              this._scrollNode = scrollNode;

              if ((verticalScrollDirection || horizontalScrollDirection) && scrollNode) {
                this._ngZone.runOutsideAngular(this._startScrollInterval);
              } else {
                this._stopScrolling();
              }
            }
          }
          /**
           * Stops any currently-running auto-scroll sequences.
           * @return {?}
           */

        }, {
          key: "_stopScrolling",
          value: function _stopScrolling() {
            this._stopScrollTimers.next();
          }
          /**
           * Caches the position of the drop list.
           * @private
           * @return {?}
           */

        }, {
          key: "_cacheOwnPosition",
          value: function _cacheOwnPosition() {
            /** @type {?} */
            var element = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceElement"])(this.element);
            this._clientRect = getMutableClientRect(element);
            this._scrollPosition = {
              top: element.scrollTop,
              left: element.scrollLeft
            };
          }
          /**
           * Refreshes the position cache of the items and sibling containers.
           * @private
           * @return {?}
           */

        }, {
          key: "_cacheItemPositions",
          value: function _cacheItemPositions() {
            var _this11 = this;

            /** @type {?} */
            var isHorizontal = this._orientation === 'horizontal';
            this._itemPositions = this._activeDraggables.map(
            /**
            * @param {?} drag
            * @return {?}
            */
            function (drag) {
              /** @type {?} */
              var elementToMeasure = _this11._dragDropRegistry.isDragging(drag) ? // If the element is being dragged, we have to measure the
              // placeholder, because the element is hidden.
              drag.getPlaceholderElement() : drag.getRootElement();
              return {
                drag: drag,
                offset: 0,
                clientRect: getMutableClientRect(elementToMeasure)
              };
            }).sort(
            /**
            * @param {?} a
            * @param {?} b
            * @return {?}
            */
            function (a, b) {
              return isHorizontal ? a.clientRect.left - b.clientRect.left : a.clientRect.top - b.clientRect.top;
            });
          }
          /**
           * Resets the container to its initial state.
           * @private
           * @return {?}
           */

        }, {
          key: "_reset",
          value: function _reset() {
            var _this12 = this;

            this._isDragging = false; // TODO(crisbeto): may have to wait for the animations to finish.

            this._activeDraggables.forEach(
            /**
            * @param {?} item
            * @return {?}
            */
            function (item) {
              return item.getRootElement().style.transform = '';
            });

            this._siblings.forEach(
            /**
            * @param {?} sibling
            * @return {?}
            */
            function (sibling) {
              return sibling._stopReceiving(_this12);
            });

            this._activeDraggables = [];
            this._itemPositions = [];
            this._previousSwap.drag = null;
            this._previousSwap.delta = 0;

            this._stopScrolling();

            this._removeListeners();
          }
          /**
           * Gets the offset in pixels by which the items that aren't being dragged should be moved.
           * @private
           * @param {?} currentIndex Index of the item currently being dragged.
           * @param {?} siblings All of the items in the list.
           * @param {?} delta Direction in which the user is moving.
           * @return {?}
           */

        }, {
          key: "_getSiblingOffsetPx",
          value: function _getSiblingOffsetPx(currentIndex, siblings, delta) {
            /** @type {?} */
            var isHorizontal = this._orientation === 'horizontal';
            /** @type {?} */

            var currentPosition = siblings[currentIndex].clientRect;
            /** @type {?} */

            var immediateSibling = siblings[currentIndex + delta * -1];
            /** @type {?} */

            var siblingOffset = currentPosition[isHorizontal ? 'width' : 'height'] * delta;

            if (immediateSibling) {
              /** @type {?} */
              var start = isHorizontal ? 'left' : 'top';
              /** @type {?} */

              var end = isHorizontal ? 'right' : 'bottom'; // Get the spacing between the start of the current item and the end of the one immediately
              // after it in the direction in which the user is dragging, or vice versa. We add it to the
              // offset in order to push the element to where it will be when it's inline and is influenced
              // by the `margin` of its siblings.

              if (delta === -1) {
                siblingOffset -= immediateSibling.clientRect[start] - currentPosition[end];
              } else {
                siblingOffset += currentPosition[start] - immediateSibling.clientRect[end];
              }
            }

            return siblingOffset;
          }
          /**
           * Checks whether the pointer coordinates are close to the drop container.
           * @private
           * @param {?} pointerX Coordinates along the X axis.
           * @param {?} pointerY Coordinates along the Y axis.
           * @return {?}
           */

        }, {
          key: "_isPointerNearDropContainer",
          value: function _isPointerNearDropContainer(pointerX, pointerY) {
            var _this$_clientRect = this._clientRect,
                top = _this$_clientRect.top,
                right = _this$_clientRect.right,
                bottom = _this$_clientRect.bottom,
                left = _this$_clientRect.left,
                width = _this$_clientRect.width,
                height = _this$_clientRect.height;
            /** @type {?} */

            var xThreshold = width * DROP_PROXIMITY_THRESHOLD;
            /** @type {?} */

            var yThreshold = height * DROP_PROXIMITY_THRESHOLD;
            return pointerY > top - yThreshold && pointerY < bottom + yThreshold && pointerX > left - xThreshold && pointerX < right + xThreshold;
          }
          /**
           * Gets the offset in pixels by which the item that is being dragged should be moved.
           * @private
           * @param {?} currentPosition Current position of the item.
           * @param {?} newPosition Position of the item where the current item should be moved.
           * @param {?} delta Direction in which the user is moving.
           * @return {?}
           */

        }, {
          key: "_getItemOffsetPx",
          value: function _getItemOffsetPx(currentPosition, newPosition, delta) {
            /** @type {?} */
            var isHorizontal = this._orientation === 'horizontal';
            /** @type {?} */

            var itemOffset = isHorizontal ? newPosition.left - currentPosition.left : newPosition.top - currentPosition.top; // Account for differences in the item width/height.

            if (delta === -1) {
              itemOffset += isHorizontal ? newPosition.width - currentPosition.width : newPosition.height - currentPosition.height;
            }

            return itemOffset;
          }
          /**
           * Gets the index of an item in the drop container, based on the position of the user's pointer.
           * @private
           * @param {?} item Item that is being sorted.
           * @param {?} pointerX Position of the user's pointer along the X axis.
           * @param {?} pointerY Position of the user's pointer along the Y axis.
           * @param {?=} delta Direction in which the user is moving their pointer.
           * @return {?}
           */

        }, {
          key: "_getItemIndexFromPointerPosition",
          value: function _getItemIndexFromPointerPosition(item, pointerX, pointerY, delta) {
            var _this13 = this;

            /** @type {?} */
            var isHorizontal = this._orientation === 'horizontal';
            return findIndex(this._itemPositions,
            /**
            * @param {?} __0
            * @param {?} _
            * @param {?} array
            * @return {?}
            */
            function (_ref2, _, array) {
              var drag = _ref2.drag,
                  clientRect = _ref2.clientRect;

              if (drag === item) {
                // If there's only one item left in the container, it must be
                // the dragged item itself so we use it as a reference.
                return array.length < 2;
              }

              if (delta) {
                /** @type {?} */
                var direction = isHorizontal ? delta.x : delta.y; // If the user is still hovering over the same item as last time, and they didn't change
                // the direction in which they're dragging, we don't consider it a direction swap.

                if (drag === _this13._previousSwap.drag && direction === _this13._previousSwap.delta) {
                  return false;
                }
              }

              return isHorizontal ? // Round these down since most browsers report client rects with
              // sub-pixel precision, whereas the pointer coordinates are rounded to pixels.
              pointerX >= Math.floor(clientRect.left) && pointerX <= Math.floor(clientRect.right) : pointerY >= Math.floor(clientRect.top) && pointerY <= Math.floor(clientRect.bottom);
            });
          }
          /**
           * Caches the current items in the list and their positions.
           * @private
           * @return {?}
           */

        }, {
          key: "_cacheItems",
          value: function _cacheItems() {
            this._activeDraggables = this._draggables.slice();

            this._cacheItemPositions();

            this._cacheOwnPosition();
          }
          /**
           * Updates the internal state of the container after a scroll event has happened.
           * @private
           * @param {?} scrollPosition Object that is keeping track of the scroll position.
           * @param {?} newTop New top scroll position.
           * @param {?} newLeft New left scroll position.
           * @param {?=} extraClientRect Extra `ClientRect` object that should be updated, in addition to the
           *  ones of the drag items. Useful when the viewport has been scrolled and we also need to update
           *  the `ClientRect` of the list.
           * @return {?}
           */

        }, {
          key: "_updateAfterScroll",
          value: function _updateAfterScroll(scrollPosition, newTop, newLeft, extraClientRect) {
            var _this14 = this;

            /** @type {?} */
            var topDifference = scrollPosition.top - newTop;
            /** @type {?} */

            var leftDifference = scrollPosition.left - newLeft;

            if (extraClientRect) {
              adjustClientRect(extraClientRect, topDifference, leftDifference);
            } // Since we know the amount that the user has scrolled we can shift all of the client rectangles
            // ourselves. This is cheaper than re-measuring everything and we can avoid inconsistent
            // behavior where we might be measuring the element before its position has changed.


            this._itemPositions.forEach(
            /**
            * @param {?} __0
            * @return {?}
            */
            function (_ref3) {
              var clientRect = _ref3.clientRect;
              adjustClientRect(clientRect, topDifference, leftDifference);
            }); // We need two loops for this, because we want all of the cached
            // positions to be up-to-date before we re-sort the item.


            this._itemPositions.forEach(
            /**
            * @param {?} __0
            * @return {?}
            */
            function (_ref4) {
              var drag = _ref4.drag;

              if (_this14._dragDropRegistry.isDragging(drag)) {
                // We need to re-sort the item manually, because the pointer move
                // events won't be dispatched while the user is scrolling.
                drag._sortFromLastPointerPosition();
              }
            });

            scrollPosition.top = newTop;
            scrollPosition.left = newLeft;
          }
          /**
           * Removes the event listeners associated with this drop list.
           * @private
           * @return {?}
           */

        }, {
          key: "_removeListeners",
          value: function _removeListeners() {
            Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceElement"])(this.element).removeEventListener('scroll', this._handleScroll);

            this._viewportScrollSubscription.unsubscribe();
          }
          /**
           * Checks whether the user's pointer is positioned over the container.
           * @param {?} x Pointer position along the X axis.
           * @param {?} y Pointer position along the Y axis.
           * @return {?}
           */

        }, {
          key: "_isOverContainer",
          value: function _isOverContainer(x, y) {
            return isInsideClientRect(this._clientRect, x, y);
          }
          /**
           * Figures out whether an item should be moved into a sibling
           * drop container, based on its current position.
           * @param {?} item Drag item that is being moved.
           * @param {?} x Position of the item along the X axis.
           * @param {?} y Position of the item along the Y axis.
           * @return {?}
           */

        }, {
          key: "_getSiblingContainerFromPosition",
          value: function _getSiblingContainerFromPosition(item, x, y) {
            return this._siblings.find(
            /**
            * @param {?} sibling
            * @return {?}
            */
            function (sibling) {
              return sibling._canReceive(item, x, y);
            });
          }
          /**
           * Checks whether the drop list can receive the passed-in item.
           * @param {?} item Item that is being dragged into the list.
           * @param {?} x Position of the item along the X axis.
           * @param {?} y Position of the item along the Y axis.
           * @return {?}
           */

        }, {
          key: "_canReceive",
          value: function _canReceive(item, x, y) {
            if (!isInsideClientRect(this._clientRect, x, y) || !this.enterPredicate(item, this)) {
              return false;
            }
            /** @type {?} */


            var elementFromPoint =
            /** @type {?} */
            this._getShadowRoot().elementFromPoint(x, y); // If there's no element at the pointer position, then
            // the client rect is probably scrolled out of the view.


            if (!elementFromPoint) {
              return false;
            }
            /** @type {?} */


            var nativeElement = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceElement"])(this.element); // The `ClientRect`, that we're using to find the container over which the user is
            // hovering, doesn't give us any information on whether the element has been scrolled
            // out of the view or whether it's overlapping with other containers. This means that
            // we could end up transferring the item into a container that's invisible or is positioned
            // below another one. We use the result from `elementFromPoint` to get the top-most element
            // at the pointer position and to find whether it's one of the intersecting drop containers.

            return elementFromPoint === nativeElement || nativeElement.contains(elementFromPoint);
          }
          /**
           * Called by one of the connected drop lists when a dragging sequence has started.
           * @param {?} sibling Sibling in which dragging has started.
           * @return {?}
           */

        }, {
          key: "_startReceiving",
          value: function _startReceiving(sibling) {
            /** @type {?} */
            var activeSiblings = this._activeSiblings;

            if (!activeSiblings.has(sibling)) {
              activeSiblings.add(sibling);

              this._cacheOwnPosition();

              this._listenToScrollEvents();
            }
          }
          /**
           * Called by a connected drop list when dragging has stopped.
           * @param {?} sibling Sibling whose dragging has stopped.
           * @return {?}
           */

        }, {
          key: "_stopReceiving",
          value: function _stopReceiving(sibling) {
            this._activeSiblings["delete"](sibling);

            this._viewportScrollSubscription.unsubscribe();
          }
          /**
           * Starts listening to scroll events on the viewport.
           * Used for updating the internal state of the list.
           * @private
           * @return {?}
           */

        }, {
          key: "_listenToScrollEvents",
          value: function _listenToScrollEvents() {
            var _this15 = this;

            this._viewportScrollPosition =
            /** @type {?} */
            this._viewportRuler.getViewportScrollPosition();
            this._viewportScrollSubscription = this._dragDropRegistry.scroll.subscribe(
            /**
            * @return {?}
            */
            function () {
              if (_this15.isDragging()) {
                /** @type {?} */
                var newPosition =
                /** @type {?} */
                _this15._viewportRuler.getViewportScrollPosition();

                _this15._updateAfterScroll(_this15._viewportScrollPosition, newPosition.top, newPosition.left, _this15._clientRect);
              } else if (_this15.isReceiving()) {
                _this15._cacheOwnPosition();
              }
            });
          }
          /**
           * Lazily resolves and returns the shadow root of the element. We do this in a function, rather
           * than saving it in property directly on init, because we want to resolve it as late as possible
           * in order to ensure that the element has been moved into the shadow DOM. Doing it inside the
           * constructor might be too early if the element is inside of something like `ngFor` or `ngIf`.
           * @private
           * @return {?}
           */

        }, {
          key: "_getShadowRoot",
          value: function _getShadowRoot() {
            if (!this._cachedShadowRoot) {
              this._cachedShadowRoot = getShadowRoot(Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceElement"])(this.element)) || this._document;
            }

            return this._cachedShadowRoot;
          }
        }]);

        return DropListRef;
      }();

      if (false) {}
      /**
       * Updates the top/left positions of a `ClientRect`, as well as their bottom/right counterparts.
       * @param {?} clientRect `ClientRect` that should be updated.
       * @param {?} top Amount to add to the `top` position.
       * @param {?} left Amount to add to the `left` position.
       * @return {?}
       */


      function adjustClientRect(clientRect, top, left) {
        clientRect.top += top;
        clientRect.bottom = clientRect.top + clientRect.height;
        clientRect.left += left;
        clientRect.right = clientRect.left + clientRect.width;
      }
      /**
       * Finds the index of an item that matches a predicate function. Used as an equivalent
       * of `Array.prototype.findIndex` which isn't part of the standard Google typings.
       * @template T
       * @param {?} array Array in which to look for matches.
       * @param {?} predicate Function used to determine whether an item is a match.
       * @return {?}
       */


      function findIndex(array, predicate) {
        for (var i = 0; i < array.length; i++) {
          if (predicate(array[i], i, array)) {
            return i;
          }
        }

        return -1;
      }
      /**
       * Checks whether some coordinates are within a `ClientRect`.
       * @param {?} clientRect ClientRect that is being checked.
       * @param {?} x Coordinates along the X axis.
       * @param {?} y Coordinates along the Y axis.
       * @return {?}
       */


      function isInsideClientRect(clientRect, x, y) {
        var top = clientRect.top,
            bottom = clientRect.bottom,
            left = clientRect.left,
            right = clientRect.right;
        return y >= top && y <= bottom && x >= left && x <= right;
      }
      /**
       * Gets a mutable version of an element's bounding `ClientRect`.
       * @param {?} element
       * @return {?}
       */


      function getMutableClientRect(element) {
        /** @type {?} */
        var clientRect = element.getBoundingClientRect(); // We need to clone the `clientRect` here, because all the values on it are readonly
        // and we need to be able to update them. Also we can't use a spread here, because
        // the values on a `ClientRect` aren't own properties. See:
        // https://developer.mozilla.org/en-US/docs/Web/API/Element/getBoundingClientRect#Notes

        return {
          top: clientRect.top,
          right: clientRect.right,
          bottom: clientRect.bottom,
          left: clientRect.left,
          width: clientRect.width,
          height: clientRect.height
        };
      }
      /**
       * Increments the vertical scroll position of a node.
       * @param {?} node Node whose scroll position should change.
       * @param {?} amount Amount of pixels that the `node` should be scrolled.
       * @return {?}
       */


      function incrementVerticalScroll(node, amount) {
        if (node === window) {
          /** @type {?} */
          node.scrollBy(0, amount);
        } else {
          // Ideally we could use `Element.scrollBy` here as well, but IE and Edge don't support it.

          /** @type {?} */
          node.scrollTop += amount;
        }
      }
      /**
       * Increments the horizontal scroll position of a node.
       * @param {?} node Node whose scroll position should change.
       * @param {?} amount Amount of pixels that the `node` should be scrolled.
       * @return {?}
       */


      function incrementHorizontalScroll(node, amount) {
        if (node === window) {
          /** @type {?} */
          node.scrollBy(amount, 0);
        } else {
          // Ideally we could use `Element.scrollBy` here as well, but IE and Edge don't support it.

          /** @type {?} */
          node.scrollLeft += amount;
        }
      }
      /**
       * Gets whether the vertical auto-scroll direction of a node.
       * @param {?} clientRect Dimensions of the node.
       * @param {?} pointerY Position of the user's pointer along the y axis.
       * @return {?}
       */


      function getVerticalScrollDirection(clientRect, pointerY) {
        var top = clientRect.top,
            bottom = clientRect.bottom,
            height = clientRect.height;
        /** @type {?} */

        var yThreshold = height * SCROLL_PROXIMITY_THRESHOLD;

        if (pointerY >= top - yThreshold && pointerY <= top + yThreshold) {
          return 1
          /* UP */
          ;
        } else if (pointerY >= bottom - yThreshold && pointerY <= bottom + yThreshold) {
          return 2
          /* DOWN */
          ;
        }

        return 0
        /* NONE */
        ;
      }
      /**
       * Gets whether the horizontal auto-scroll direction of a node.
       * @param {?} clientRect Dimensions of the node.
       * @param {?} pointerX Position of the user's pointer along the x axis.
       * @return {?}
       */


      function getHorizontalScrollDirection(clientRect, pointerX) {
        var left = clientRect.left,
            right = clientRect.right,
            width = clientRect.width;
        /** @type {?} */

        var xThreshold = width * SCROLL_PROXIMITY_THRESHOLD;

        if (pointerX >= left - xThreshold && pointerX <= left + xThreshold) {
          return 1
          /* LEFT */
          ;
        } else if (pointerX >= right - xThreshold && pointerX <= right + xThreshold) {
          return 2
          /* RIGHT */
          ;
        }

        return 0
        /* NONE */
        ;
      }
      /**
       * Gets the directions in which an element node should be scrolled,
       * assuming that the user's pointer is already within it scrollable region.
       * @param {?} element Element for which we should calculate the scroll direction.
       * @param {?} clientRect Bounding client rectangle of the element.
       * @param {?} pointerX Position of the user's pointer along the x axis.
       * @param {?} pointerY Position of the user's pointer along the y axis.
       * @return {?}
       */


      function getElementScrollDirections(element, clientRect, pointerX, pointerY) {
        /** @type {?} */
        var computedVertical = getVerticalScrollDirection(clientRect, pointerY);
        /** @type {?} */

        var computedHorizontal = getHorizontalScrollDirection(clientRect, pointerX);
        /** @type {?} */

        var verticalScrollDirection = 0
        /* NONE */
        ;
        /** @type {?} */

        var horizontalScrollDirection = 0
        /* NONE */
        ; // Note that we here we do some extra checks for whether the element is actually scrollable in
        // a certain direction and we only assign the scroll direction if it is. We do this so that we
        // can allow other elements to be scrolled, if the current element can't be scrolled anymore.
        // This allows us to handle cases where the scroll regions of two scrollable elements overlap.

        if (computedVertical) {
          /** @type {?} */
          var scrollTop = element.scrollTop;

          if (computedVertical === 1
          /* UP */
          ) {
              if (scrollTop > 0) {
                verticalScrollDirection = 1
                /* UP */
                ;
              }
            } else if (element.scrollHeight - scrollTop > element.clientHeight) {
            verticalScrollDirection = 2
            /* DOWN */
            ;
          }
        }

        if (computedHorizontal) {
          /** @type {?} */
          var scrollLeft = element.scrollLeft;

          if (computedHorizontal === 1
          /* LEFT */
          ) {
              if (scrollLeft > 0) {
                horizontalScrollDirection = 1
                /* LEFT */
                ;
              }
            } else if (element.scrollWidth - scrollLeft > element.clientWidth) {
            horizontalScrollDirection = 2
            /* RIGHT */
            ;
          }
        }

        return [verticalScrollDirection, horizontalScrollDirection];
      }
      /**
       * Gets the shadow root of an element, if any.
       * @param {?} element
       * @return {?}
       */


      function getShadowRoot(element) {
        if (Object(_angular_cdk_platform__WEBPACK_IMPORTED_MODULE_3__["_supportsShadowDom"])()) {
          /** @type {?} */
          var rootNode = element.getRootNode ? element.getRootNode() : null;

          if (rootNode instanceof ShadowRoot) {
            return rootNode;
          }
        }

        return null;
      }
      /**
       * @fileoverview added by tsickle
       * Generated from: src/cdk/drag-drop/drag-drop-registry.ts
       * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * Event options that can be used to bind an active, capturing event.
       * @type {?}
       */


      var activeCapturingEventOptions = Object(_angular_cdk_platform__WEBPACK_IMPORTED_MODULE_3__["normalizePassiveListenerOptions"])({
        passive: false,
        capture: true
      });
      /**
       * Service that keeps track of all the drag item and drop container
       * instances, and manages global event listeners on the `document`.
       * \@docs-private
       * @template I, C
       */
      // Note: this class is generic, rather than referencing CdkDrag and CdkDropList directly, in order
      // to avoid circular imports. If we were to reference them here, importing the registry into the
      // classes that are registering themselves will introduce a circular import.

      var DragDropRegistry = /*#__PURE__*/function () {
        /**
         * @param {?} _ngZone
         * @param {?} _document
         */
        function DragDropRegistry(_ngZone, _document) {
          var _this16 = this;

          _classCallCheck(this, DragDropRegistry);

          this._ngZone = _ngZone;
          /**
           * Registered drop container instances.
           */

          this._dropInstances = new Set();
          /**
           * Registered drag item instances.
           */

          this._dragInstances = new Set();
          /**
           * Drag item instances that are currently being dragged.
           */

          this._activeDragInstances = new Set();
          /**
           * Keeps track of the event listeners that we've bound to the `document`.
           */

          this._globalListeners = new Map();
          /**
           * Emits the `touchmove` or `mousemove` events that are dispatched
           * while the user is dragging a drag item instance.
           */

          this.pointerMove = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          /**
           * Emits the `touchend` or `mouseup` events that are dispatched
           * while the user is dragging a drag item instance.
           */

          this.pointerUp = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          /**
           * Emits when the viewport has been scrolled while the user is dragging an item.
           */

          this.scroll = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          /**
           * Event listener that will prevent the default browser action while the user is dragging.
           * @param event Event whose default action should be prevented.
           */

          this._preventDefaultWhileDragging =
          /**
          * @param {?} event
          * @return {?}
          */
          function (event) {
            if (_this16._activeDragInstances.size) {
              event.preventDefault();
            }
          };

          this._document = _document;
        }
        /**
         * Adds a drop container to the registry.
         * @param {?} drop
         * @return {?}
         */


        _createClass(DragDropRegistry, [{
          key: "registerDropContainer",
          value: function registerDropContainer(drop) {
            if (!this._dropInstances.has(drop)) {
              this._dropInstances.add(drop);
            }
          }
          /**
           * Adds a drag item instance to the registry.
           * @param {?} drag
           * @return {?}
           */

        }, {
          key: "registerDragItem",
          value: function registerDragItem(drag) {
            var _this17 = this;

            this._dragInstances.add(drag); // The `touchmove` event gets bound once, ahead of time, because WebKit
            // won't preventDefault on a dynamically-added `touchmove` listener.
            // See https://bugs.webkit.org/show_bug.cgi?id=184250.


            if (this._dragInstances.size === 1) {
              this._ngZone.runOutsideAngular(
              /**
              * @return {?}
              */
              function () {
                // The event handler has to be explicitly active,
                // because newer browsers make it passive by default.
                _this17._document.addEventListener('touchmove', _this17._preventDefaultWhileDragging, activeCapturingEventOptions);
              });
            }
          }
          /**
           * Removes a drop container from the registry.
           * @param {?} drop
           * @return {?}
           */

        }, {
          key: "removeDropContainer",
          value: function removeDropContainer(drop) {
            this._dropInstances["delete"](drop);
          }
          /**
           * Removes a drag item instance from the registry.
           * @param {?} drag
           * @return {?}
           */

        }, {
          key: "removeDragItem",
          value: function removeDragItem(drag) {
            this._dragInstances["delete"](drag);

            this.stopDragging(drag);

            if (this._dragInstances.size === 0) {
              this._document.removeEventListener('touchmove', this._preventDefaultWhileDragging, activeCapturingEventOptions);
            }
          }
          /**
           * Starts the dragging sequence for a drag instance.
           * @param {?} drag Drag instance which is being dragged.
           * @param {?} event Event that initiated the dragging.
           * @return {?}
           */

        }, {
          key: "startDragging",
          value: function startDragging(drag, event) {
            var _this18 = this;

            // Do not process the same drag twice to avoid memory leaks and redundant listeners
            if (this._activeDragInstances.has(drag)) {
              return;
            }

            this._activeDragInstances.add(drag);

            if (this._activeDragInstances.size === 1) {
              /** @type {?} */
              var _isTouchEvent = event.type.startsWith('touch');
              /** @type {?} */


              var moveEvent = _isTouchEvent ? 'touchmove' : 'mousemove';
              /** @type {?} */

              var upEvent = _isTouchEvent ? 'touchend' : 'mouseup'; // We explicitly bind __active__ listeners here, because newer browsers will default to
              // passive ones for `mousemove` and `touchmove`. The events need to be active, because we
              // use `preventDefault` to prevent the page from scrolling while the user is dragging.

              this._globalListeners.set(moveEvent, {
                handler:
                /**
                * @param {?} e
                * @return {?}
                */
                function handler(e) {
                  return _this18.pointerMove.next(
                  /** @type {?} */
                  e);
                },
                options: activeCapturingEventOptions
              }).set(upEvent, {
                handler:
                /**
                * @param {?} e
                * @return {?}
                */
                function handler(e) {
                  return _this18.pointerUp.next(
                  /** @type {?} */
                  e);
                },
                options: true
              }).set('scroll', {
                handler:
                /**
                * @param {?} e
                * @return {?}
                */
                function handler(e) {
                  return _this18.scroll.next(e);
                },
                // Use capturing so that we pick up scroll changes in any scrollable nodes that aren't
                // the document. See https://github.com/angular/components/issues/17144.
                options: true
              }) // Preventing the default action on `mousemove` isn't enough to disable text selection
              // on Safari so we need to prevent the selection event as well. Alternatively this can
              // be done by setting `user-select: none` on the `body`, however it has causes a style
              // recalculation which can be expensive on pages with a lot of elements.
              .set('selectstart', {
                handler: this._preventDefaultWhileDragging,
                options: activeCapturingEventOptions
              });

              this._ngZone.runOutsideAngular(
              /**
              * @return {?}
              */
              function () {
                _this18._globalListeners.forEach(
                /**
                * @param {?} config
                * @param {?} name
                * @return {?}
                */
                function (config, name) {
                  _this18._document.addEventListener(name, config.handler, config.options);
                });
              });
            }
          }
          /**
           * Stops dragging a drag item instance.
           * @param {?} drag
           * @return {?}
           */

        }, {
          key: "stopDragging",
          value: function stopDragging(drag) {
            this._activeDragInstances["delete"](drag);

            if (this._activeDragInstances.size === 0) {
              this._clearGlobalListeners();
            }
          }
          /**
           * Gets whether a drag item instance is currently being dragged.
           * @param {?} drag
           * @return {?}
           */

        }, {
          key: "isDragging",
          value: function isDragging(drag) {
            return this._activeDragInstances.has(drag);
          }
          /**
           * @return {?}
           */

        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            var _this19 = this;

            this._dragInstances.forEach(
            /**
            * @param {?} instance
            * @return {?}
            */
            function (instance) {
              return _this19.removeDragItem(instance);
            });

            this._dropInstances.forEach(
            /**
            * @param {?} instance
            * @return {?}
            */
            function (instance) {
              return _this19.removeDropContainer(instance);
            });

            this._clearGlobalListeners();

            this.pointerMove.complete();
            this.pointerUp.complete();
          }
          /**
           * Clears out the global event listeners from the `document`.
           * @private
           * @return {?}
           */

        }, {
          key: "_clearGlobalListeners",
          value: function _clearGlobalListeners() {
            var _this20 = this;

            this._globalListeners.forEach(
            /**
            * @param {?} config
            * @param {?} name
            * @return {?}
            */
            function (config, name) {
              _this20._document.removeEventListener(name, config.handler, config.options);
            });

            this._globalListeners.clear();
          }
        }]);

        return DragDropRegistry;
      }();

      DragDropRegistry.ɵfac = function DragDropRegistry_Factory(t) {
        return new (t || DragDropRegistry)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"]));
      };
      /** @nocollapse */


      DragDropRegistry.ctorParameters = function () {
        return [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]
        }, {
          type: undefined,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
            args: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"]]
          }]
        }];
      };
      /** @nocollapse */


      DragDropRegistry.ɵprov = Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"])({
        factory: function DragDropRegistry_Factory() {
          return new DragDropRegistry(Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"])(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]), Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"])(_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"]));
        },
        token: DragDropRegistry,
        providedIn: "root"
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DragDropRegistry, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]
          }, {
            type: undefined,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
              args: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"]]
            }]
          }];
        }, null);
      })();

      if (false) {}
      /**
       * @fileoverview added by tsickle
       * Generated from: src/cdk/drag-drop/drag-drop.ts
       * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * Default configuration to be used when creating a `DragRef`.
       * @type {?}
       */


      var DEFAULT_CONFIG = {
        dragStartThreshold: 5,
        pointerDirectionChangeThreshold: 5
      };
      /**
       * Service that allows for drag-and-drop functionality to be attached to DOM elements.
       */

      var DragDrop = /*#__PURE__*/function () {
        /**
         * @param {?} _document
         * @param {?} _ngZone
         * @param {?} _viewportRuler
         * @param {?} _dragDropRegistry
         */
        function DragDrop(_document, _ngZone, _viewportRuler, _dragDropRegistry) {
          _classCallCheck(this, DragDrop);

          this._document = _document;
          this._ngZone = _ngZone;
          this._viewportRuler = _viewportRuler;
          this._dragDropRegistry = _dragDropRegistry;
        }
        /**
         * Turns an element into a draggable item.
         * @template T
         * @param {?} element Element to which to attach the dragging functionality.
         * @param {?=} config Object used to configure the dragging behavior.
         * @return {?}
         */


        _createClass(DragDrop, [{
          key: "createDrag",
          value: function createDrag(element) {
            var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : DEFAULT_CONFIG;
            return new DragRef(element, config, this._document, this._ngZone, this._viewportRuler, this._dragDropRegistry);
          }
          /**
           * Turns an element into a drop list.
           * @template T
           * @param {?} element Element to which to attach the drop list functionality.
           * @return {?}
           */

        }, {
          key: "createDropList",
          value: function createDropList(element) {
            return new DropListRef(element, this._dragDropRegistry, this._document, this._ngZone, this._viewportRuler);
          }
        }]);

        return DragDrop;
      }();

      DragDrop.ɵfac = function DragDrop_Factory(t) {
        return new (t || DragDrop)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_2__["ViewportRuler"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](DragDropRegistry));
      };
      /** @nocollapse */


      DragDrop.ctorParameters = function () {
        return [{
          type: undefined,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
            args: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"]]
          }]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]
        }, {
          type: _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_2__["ViewportRuler"]
        }, {
          type: DragDropRegistry
        }];
      };
      /** @nocollapse */


      DragDrop.ɵprov = Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"])({
        factory: function DragDrop_Factory() {
          return new DragDrop(Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"])(_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"]), Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"])(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]), Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"])(_angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_2__["ViewportRuler"]), Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"])(DragDropRegistry));
        },
        token: DragDrop,
        providedIn: "root"
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DragDrop, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: undefined,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
              args: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"]]
            }]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]
          }, {
            type: _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_2__["ViewportRuler"]
          }, {
            type: DragDropRegistry
          }];
        }, null);
      })();

      if (false) {}
      /**
       * @fileoverview added by tsickle
       * Generated from: src/cdk/drag-drop/drag-events.ts
       * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * @license
       * Copyright Google LLC All Rights Reserved.
       *
       * Use of this source code is governed by an MIT-style license that can be
       * found in the LICENSE file at https://angular.io/license
       */

      /**
       * Event emitted when the user starts dragging a draggable.
       * @record
       * @template T
       */


      function CdkDragStart() {}

      if (false) {}
      /**
       * Event emitted when the user releases an item, before any animations have started.
       * @record
       * @template T
       */


      function CdkDragRelease() {}

      if (false) {}
      /**
       * Event emitted when the user stops dragging a draggable.
       * @record
       * @template T
       */


      function CdkDragEnd() {}

      if (false) {}
      /**
       * Event emitted when the user moves an item into a new drop container.
       * @record
       * @template T, I
       */


      function CdkDragEnter() {}

      if (false) {}
      /**
       * Event emitted when the user removes an item from a
       * drop container by moving it into another one.
       * @record
       * @template T, I
       */


      function CdkDragExit() {}

      if (false) {}
      /**
       * Event emitted when the user drops a draggable item inside a drop container.
       * @record
       * @template T, O
       */


      function CdkDragDrop() {}

      if (false) {}
      /**
       * Event emitted as the user is dragging a draggable item.
       * @record
       * @template T
       */


      function CdkDragMove() {}

      if (false) {}
      /**
       * Event emitted when the user swaps the position of two drag items.
       * @record
       * @template T, I
       */


      function CdkDragSortEvent() {}

      if (false) {}
      /**
       * @fileoverview added by tsickle
       * Generated from: src/cdk/drag-drop/drag-parent.ts
       * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * Injection token that can be used for a `CdkDrag` to provide itself as a parent to the
       * drag-specific child directive (`CdkDragHandle`, `CdkDragPreview` etc.). Used primarily
       * to avoid circular imports.
       * \@docs-private
       * @type {?}
       */


      var CDK_DRAG_PARENT = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('CDK_DRAG_PARENT');
      /**
       * @fileoverview added by tsickle
       * Generated from: src/cdk/drag-drop/directives/drag-handle.ts
       * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * Handle that can be used to drag and CdkDrag instance.
       */

      var CdkDragHandle = /*#__PURE__*/function () {
        /**
         * @param {?} element
         * @param {?=} parentDrag
         */
        function CdkDragHandle(element, parentDrag) {
          _classCallCheck(this, CdkDragHandle);

          this.element = element;
          /**
           * Emits when the state of the handle has changed.
           */

          this._stateChanges = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          this._disabled = false;
          this._parentDrag = parentDrag;
          toggleNativeDragInteractions(element.nativeElement, false);
        }
        /**
         * Whether starting to drag through this handle is disabled.
         * @return {?}
         */


        _createClass(CdkDragHandle, [{
          key: "ngOnDestroy",

          /**
           * @return {?}
           */
          value: function ngOnDestroy() {
            this._stateChanges.complete();
          }
        }, {
          key: "disabled",
          get: function get() {
            return this._disabled;
          }
          /**
           * @param {?} value
           * @return {?}
           */
          ,
          set: function set(value) {
            this._disabled = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceBooleanProperty"])(value);

            this._stateChanges.next(this);
          }
        }]);

        return CdkDragHandle;
      }();

      CdkDragHandle.ɵfac = function CdkDragHandle_Factory(t) {
        return new (t || CdkDragHandle)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](CDK_DRAG_PARENT, 8));
      };

      CdkDragHandle.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineDirective"]({
        type: CdkDragHandle,
        selectors: [["", "cdkDragHandle", ""]],
        hostAttrs: [1, "cdk-drag-handle"],
        inputs: {
          disabled: ["cdkDragHandleDisabled", "disabled"]
        }
      });
      /** @nocollapse */

      CdkDragHandle.ctorParameters = function () {
        return [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
        }, {
          type: undefined,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
            args: [CDK_DRAG_PARENT]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
          }]
        }];
      };

      CdkDragHandle.propDecorators = {
        disabled: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['cdkDragHandleDisabled']
        }]
      };
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CdkDragHandle, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
          args: [{
            selector: '[cdkDragHandle]',
            host: {
              'class': 'cdk-drag-handle'
            }
          }]
        }], function () {
          return [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
          }, {
            type: undefined,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
              args: [CDK_DRAG_PARENT]
            }, {
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
            }]
          }];
        }, {
          disabled: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['cdkDragHandleDisabled']
          }]
        });
      })();

      if (false) {}
      /**
       * @fileoverview added by tsickle
       * Generated from: src/cdk/drag-drop/directives/drag-placeholder.ts
       * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * Element that will be used as a template for the placeholder of a CdkDrag when
       * it is being dragged. The placeholder is displayed in place of the element being dragged.
       * @template T
       */


      var CdkDragPlaceholder =
      /**
       * @param {?} templateRef
       */
      function CdkDragPlaceholder(templateRef) {
        _classCallCheck(this, CdkDragPlaceholder);

        this.templateRef = templateRef;
      };

      CdkDragPlaceholder.ɵfac = function CdkDragPlaceholder_Factory(t) {
        return new (t || CdkDragPlaceholder)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]));
      };

      CdkDragPlaceholder.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineDirective"]({
        type: CdkDragPlaceholder,
        selectors: [["ng-template", "cdkDragPlaceholder", ""]],
        inputs: {
          data: "data"
        }
      });
      /** @nocollapse */

      CdkDragPlaceholder.ctorParameters = function () {
        return [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]
        }];
      };

      CdkDragPlaceholder.propDecorators = {
        data: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }]
      };
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CdkDragPlaceholder, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
          args: [{
            selector: 'ng-template[cdkDragPlaceholder]'
          }]
        }], function () {
          return [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]
          }];
        }, {
          data: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }]
        });
      })();

      if (false) {}
      /**
       * @fileoverview added by tsickle
       * Generated from: src/cdk/drag-drop/directives/drag-preview.ts
       * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * Element that will be used as a template for the preview
       * of a CdkDrag when it is being dragged.
       * @template T
       */


      var CdkDragPreview =
      /**
       * @param {?} templateRef
       */
      function CdkDragPreview(templateRef) {
        _classCallCheck(this, CdkDragPreview);

        this.templateRef = templateRef;
      };

      CdkDragPreview.ɵfac = function CdkDragPreview_Factory(t) {
        return new (t || CdkDragPreview)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]));
      };

      CdkDragPreview.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineDirective"]({
        type: CdkDragPreview,
        selectors: [["ng-template", "cdkDragPreview", ""]],
        inputs: {
          data: "data"
        }
      });
      /** @nocollapse */

      CdkDragPreview.ctorParameters = function () {
        return [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]
        }];
      };

      CdkDragPreview.propDecorators = {
        data: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }]
      };
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CdkDragPreview, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
          args: [{
            selector: 'ng-template[cdkDragPreview]'
          }]
        }], function () {
          return [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]
          }];
        }, {
          data: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }]
        });
      })();

      if (false) {}
      /**
       * @fileoverview added by tsickle
       * Generated from: src/cdk/drag-drop/directives/drag.ts
       * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * Injection token that is used to provide a CdkDropList instance to CdkDrag.
       * Used for avoiding circular imports.
       * @type {?}
       */


      var CDK_DROP_LIST = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('CDK_DROP_LIST');
      /**
       * Injection token that can be used to configure the behavior of `CdkDrag`.
       * @type {?}
       */

      var CDK_DRAG_CONFIG = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('CDK_DRAG_CONFIG', {
        providedIn: 'root',
        factory: CDK_DRAG_CONFIG_FACTORY
      });
      /**
       * \@docs-private
       * @return {?}
       */

      function CDK_DRAG_CONFIG_FACTORY() {
        return {
          dragStartThreshold: 5,
          pointerDirectionChangeThreshold: 5
        };
      }
      /**
       * Element that can be moved inside a CdkDropList container.
       * @template T
       */


      var CdkDrag = /*#__PURE__*/function () {
        /**
         * @param {?} element
         * @param {?} dropContainer
         * @param {?} _document
         * @param {?} _ngZone
         * @param {?} _viewContainerRef
         * @param {?} config
         * @param {?} _dir
         * @param {?} dragDrop
         * @param {?} _changeDetectorRef
         */
        function CdkDrag(element, dropContainer, _document, _ngZone, _viewContainerRef, config, _dir, dragDrop, _changeDetectorRef) {
          var _this21 = this;

          _classCallCheck(this, CdkDrag);

          this.element = element;
          this.dropContainer = dropContainer;
          this._document = _document;
          this._ngZone = _ngZone;
          this._viewContainerRef = _viewContainerRef;
          this._dir = _dir;
          this._changeDetectorRef = _changeDetectorRef;
          this._destroyed = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          /**
           * Amount of milliseconds to wait after the user has put their
           * pointer down before starting to drag the element.
           */

          this.dragStartDelay = 0;
          this._disabled = false;
          /**
           * Emits when the user starts dragging the item.
           */

          this.started = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          /**
           * Emits when the user has released a drag item, before any animations have started.
           */

          this.released = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          /**
           * Emits when the user stops dragging an item in the container.
           */

          this.ended = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          /**
           * Emits when the user has moved the item into a new container.
           */

          this.entered = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          /**
           * Emits when the user removes the item its container by dragging it into another container.
           */

          this.exited = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          /**
           * Emits when the user drops the item inside a container.
           */

          this.dropped = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          /**
           * Emits as the user is dragging the item. Use with caution,
           * because this event will fire for every pixel that the user has dragged.
           */

          this.moved = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Observable"](
          /**
          * @param {?} observer
          * @return {?}
          */
          function (observer) {
            /** @type {?} */
            var subscription = _this21._dragRef.moved.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(
            /**
            * @param {?} movedEvent
            * @return {?}
            */
            function (movedEvent) {
              return {
                source: _this21,
                pointerPosition: movedEvent.pointerPosition,
                event: movedEvent.event,
                delta: movedEvent.delta,
                distance: movedEvent.distance
              };
            })).subscribe(observer);

            return (
              /**
              * @return {?}
              */
              function () {
                subscription.unsubscribe();
              }
            );
          });
          this._dragRef = dragDrop.createDrag(element, config);
          this._dragRef.data = this;

          this._syncInputs(this._dragRef);

          this._handleEvents(this._dragRef);
        }
        /**
         * Whether starting to drag this element is disabled.
         * @return {?}
         */


        _createClass(CdkDrag, [{
          key: "getPlaceholderElement",

          /**
           * Returns the element that is being used as a placeholder
           * while the current element is being dragged.
           * @return {?}
           */
          value: function getPlaceholderElement() {
            return this._dragRef.getPlaceholderElement();
          }
          /**
           * Returns the root draggable element.
           * @return {?}
           */

        }, {
          key: "getRootElement",
          value: function getRootElement() {
            return this._dragRef.getRootElement();
          }
          /**
           * Resets a standalone drag item to its initial position.
           * @return {?}
           */

        }, {
          key: "reset",
          value: function reset() {
            this._dragRef.reset();
          }
          /**
           * Gets the pixel coordinates of the draggable outside of a drop container.
           * @return {?}
           */

        }, {
          key: "getFreeDragPosition",
          value: function getFreeDragPosition() {
            return this._dragRef.getFreeDragPosition();
          }
          /**
           * @return {?}
           */

        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            var _this22 = this;

            // We need to wait for the zone to stabilize, in order for the reference
            // element to be in the proper place in the DOM. This is mostly relevant
            // for draggable elements inside portals since they get stamped out in
            // their original DOM position and then they get transferred to the portal.
            this._ngZone.onStable.asObservable().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["takeUntil"])(this._destroyed)).subscribe(
            /**
            * @return {?}
            */
            function () {
              _this22._updateRootElement(); // Listen for any newly-added handles.


              _this22._handles.changes.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["startWith"])(_this22._handles), // Sync the new handles with the DragRef.
              Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["tap"])(
              /**
              * @param {?} handles
              * @return {?}
              */
              function (handles) {
                /** @type {?} */
                var childHandleElements = handles.filter(
                /**
                * @param {?} handle
                * @return {?}
                */
                function (handle) {
                  return handle._parentDrag === _this22;
                }).map(
                /**
                * @param {?} handle
                * @return {?}
                */
                function (handle) {
                  return handle.element;
                });

                _this22._dragRef.withHandles(childHandleElements);
              }), // Listen if the state of any of the handles changes.
              Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])(
              /**
              * @param {?} handles
              * @return {?}
              */
              function (handles) {
                return (
                  /** @type {?} */
                  Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["merge"]).apply(void 0, _toConsumableArray(handles.map(
                  /**
                  * @param {?} item
                  * @return {?}
                  */
                  function (item) {
                    return item._stateChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["startWith"])(item));
                  })))
                );
              }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["takeUntil"])(_this22._destroyed)).subscribe(
              /**
              * @param {?} handleInstance
              * @return {?}
              */
              function (handleInstance) {
                // Enabled/disable the handle that changed in the DragRef.

                /** @type {?} */
                var dragRef = _this22._dragRef;
                /** @type {?} */

                var handle = handleInstance.element.nativeElement;
                handleInstance.disabled ? dragRef.disableHandle(handle) : dragRef.enableHandle(handle);
              });

              if (_this22.freeDragPosition) {
                _this22._dragRef.setFreeDragPosition(_this22.freeDragPosition);
              }
            });
          }
          /**
           * @param {?} changes
           * @return {?}
           */

        }, {
          key: "ngOnChanges",
          value: function ngOnChanges(changes) {
            /** @type {?} */
            var rootSelectorChange = changes['rootElementSelector'];
            /** @type {?} */

            var positionChange = changes['freeDragPosition']; // We don't have to react to the first change since it's being
            // handled in `ngAfterViewInit` where it needs to be deferred.

            if (rootSelectorChange && !rootSelectorChange.firstChange) {
              this._updateRootElement();
            } // Skip the first change since it's being handled in `ngAfterViewInit`.


            if (positionChange && !positionChange.firstChange && this.freeDragPosition) {
              this._dragRef.setFreeDragPosition(this.freeDragPosition);
            }
          }
          /**
           * @return {?}
           */

        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this._destroyed.next();

            this._destroyed.complete();

            this._dragRef.dispose();
          }
          /**
           * Syncs the root element with the `DragRef`.
           * @private
           * @return {?}
           */

        }, {
          key: "_updateRootElement",
          value: function _updateRootElement() {
            /** @type {?} */
            var element = this.element.nativeElement;
            /** @type {?} */

            var rootElement = this.rootElementSelector ? getClosestMatchingAncestor(element, this.rootElementSelector) : element;

            if (rootElement && rootElement.nodeType !== this._document.ELEMENT_NODE) {
              throw Error("cdkDrag must be attached to an element node. " + "Currently attached to \"".concat(rootElement.nodeName, "\"."));
            }

            this._dragRef.withRootElement(rootElement || element);
          }
          /**
           * Gets the boundary element, based on the `boundaryElement` value.
           * @private
           * @return {?}
           */

        }, {
          key: "_getBoundaryElement",
          value: function _getBoundaryElement() {
            /** @type {?} */
            var boundary = this.boundaryElement;

            if (!boundary) {
              return null;
            }

            if (typeof boundary === 'string') {
              return getClosestMatchingAncestor(this.element.nativeElement, boundary);
            }
            /** @type {?} */


            var element = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceElement"])(boundary);

            if (Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["isDevMode"])() && !element.contains(this.element.nativeElement)) {
              throw Error('Draggable element is not inside of the node passed into cdkDragBoundary.');
            }

            return element;
          }
          /**
           * Syncs the inputs of the CdkDrag with the options of the underlying DragRef.
           * @private
           * @param {?} ref
           * @return {?}
           */

        }, {
          key: "_syncInputs",
          value: function _syncInputs(ref) {
            var _this23 = this;

            ref.beforeStarted.subscribe(
            /**
            * @return {?}
            */
            function () {
              if (!ref.isDragging()) {
                /** @type {?} */
                var dir = _this23._dir;
                /** @type {?} */

                var dragStartDelay = _this23.dragStartDelay;
                /** @type {?} */

                var placeholder = _this23._placeholderTemplate ? {
                  template: _this23._placeholderTemplate.templateRef,
                  context: _this23._placeholderTemplate.data,
                  viewContainer: _this23._viewContainerRef
                } : null;
                /** @type {?} */

                var preview = _this23._previewTemplate ? {
                  template: _this23._previewTemplate.templateRef,
                  context: _this23._previewTemplate.data,
                  viewContainer: _this23._viewContainerRef
                } : null;
                ref.disabled = _this23.disabled;
                ref.lockAxis = _this23.lockAxis;
                ref.dragStartDelay = typeof dragStartDelay === 'object' && dragStartDelay ? dragStartDelay : Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceNumberProperty"])(dragStartDelay);
                ref.constrainPosition = _this23.constrainPosition;
                ref.previewClass = _this23.previewClass;
                ref.withBoundaryElement(_this23._getBoundaryElement()).withPlaceholderTemplate(placeholder).withPreviewTemplate(preview);

                if (dir) {
                  ref.withDirection(dir.value);
                }
              }
            });
          }
          /**
           * Handles the events from the underlying `DragRef`.
           * @private
           * @param {?} ref
           * @return {?}
           */

        }, {
          key: "_handleEvents",
          value: function _handleEvents(ref) {
            var _this24 = this;

            ref.started.subscribe(
            /**
            * @return {?}
            */
            function () {
              _this24.started.emit({
                source: _this24
              }); // Since all of these events run outside of change detection,
              // we need to ensure that everything is marked correctly.


              _this24._changeDetectorRef.markForCheck();
            });
            ref.released.subscribe(
            /**
            * @return {?}
            */
            function () {
              _this24.released.emit({
                source: _this24
              });
            });
            ref.ended.subscribe(
            /**
            * @param {?} event
            * @return {?}
            */
            function (event) {
              _this24.ended.emit({
                source: _this24,
                distance: event.distance
              }); // Since all of these events run outside of change detection,
              // we need to ensure that everything is marked correctly.


              _this24._changeDetectorRef.markForCheck();
            });
            ref.entered.subscribe(
            /**
            * @param {?} event
            * @return {?}
            */
            function (event) {
              _this24.entered.emit({
                container: event.container.data,
                item: _this24,
                currentIndex: event.currentIndex
              });
            });
            ref.exited.subscribe(
            /**
            * @param {?} event
            * @return {?}
            */
            function (event) {
              _this24.exited.emit({
                container: event.container.data,
                item: _this24
              });
            });
            ref.dropped.subscribe(
            /**
            * @param {?} event
            * @return {?}
            */
            function (event) {
              _this24.dropped.emit({
                previousIndex: event.previousIndex,
                currentIndex: event.currentIndex,
                previousContainer: event.previousContainer.data,
                container: event.container.data,
                isPointerOverContainer: event.isPointerOverContainer,
                item: _this24,
                distance: event.distance
              });
            });
          }
        }, {
          key: "disabled",
          get: function get() {
            return this._disabled || this.dropContainer && this.dropContainer.disabled;
          }
          /**
           * @param {?} value
           * @return {?}
           */
          ,
          set: function set(value) {
            this._disabled = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceBooleanProperty"])(value);
            this._dragRef.disabled = this._disabled;
          }
        }]);

        return CdkDrag;
      }();

      CdkDrag.ɵfac = function CdkDrag_Factory(t) {
        return new (t || CdkDrag)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](CDK_DROP_LIST, 12), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](CDK_DRAG_CONFIG), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_7__["Directionality"], 8), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](DragDrop), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]));
      };

      CdkDrag.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineDirective"]({
        type: CdkDrag,
        selectors: [["", "cdkDrag", ""]],
        contentQueries: function CdkDrag_ContentQueries(rf, ctx, dirIndex) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵcontentQuery"](dirIndex, CdkDragPreview, true);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵcontentQuery"](dirIndex, CdkDragPlaceholder, true);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵcontentQuery"](dirIndex, CdkDragHandle, true);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx._previewTemplate = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx._placeholderTemplate = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx._handles = _t);
          }
        },
        hostAttrs: [1, "cdk-drag"],
        hostVars: 4,
        hostBindings: function CdkDrag_HostBindings(rf, ctx) {
          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("cdk-drag-disabled", ctx.disabled)("cdk-drag-dragging", ctx._dragRef.isDragging());
          }
        },
        inputs: {
          dragStartDelay: ["cdkDragStartDelay", "dragStartDelay"],
          disabled: ["cdkDragDisabled", "disabled"],
          data: ["cdkDragData", "data"],
          lockAxis: ["cdkDragLockAxis", "lockAxis"],
          rootElementSelector: ["cdkDragRootElement", "rootElementSelector"],
          boundaryElement: ["cdkDragBoundary", "boundaryElement"],
          freeDragPosition: ["cdkDragFreeDragPosition", "freeDragPosition"],
          constrainPosition: ["cdkDragConstrainPosition", "constrainPosition"],
          previewClass: ["cdkDragPreviewClass", "previewClass"]
        },
        outputs: {
          started: "cdkDragStarted",
          released: "cdkDragReleased",
          ended: "cdkDragEnded",
          entered: "cdkDragEntered",
          exited: "cdkDragExited",
          dropped: "cdkDragDropped",
          moved: "cdkDragMoved"
        },
        exportAs: ["cdkDrag"],
        features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵProvidersFeature"]([{
          provide: CDK_DRAG_PARENT,
          useExisting: CdkDrag
        }]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵNgOnChangesFeature"]]
      });
      /** @nocollapse */

      CdkDrag.ctorParameters = function () {
        return [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
        }, {
          type: undefined,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
            args: [CDK_DROP_LIST]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["SkipSelf"]
          }]
        }, {
          type: undefined,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
            args: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"]]
          }]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"]
        }, {
          type: undefined,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
            args: [CDK_DRAG_CONFIG]
          }]
        }, {
          type: _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_7__["Directionality"],
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
          }]
        }, {
          type: DragDrop
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
        }];
      };

      CdkDrag.propDecorators = {
        _handles: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChildren"],
          args: [CdkDragHandle, {
            descendants: true
          }]
        }],
        _previewTemplate: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"],
          args: [CdkDragPreview]
        }],
        _placeholderTemplate: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"],
          args: [CdkDragPlaceholder]
        }],
        data: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['cdkDragData']
        }],
        lockAxis: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['cdkDragLockAxis']
        }],
        rootElementSelector: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['cdkDragRootElement']
        }],
        boundaryElement: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['cdkDragBoundary']
        }],
        dragStartDelay: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['cdkDragStartDelay']
        }],
        freeDragPosition: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['cdkDragFreeDragPosition']
        }],
        disabled: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['cdkDragDisabled']
        }],
        constrainPosition: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['cdkDragConstrainPosition']
        }],
        previewClass: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['cdkDragPreviewClass']
        }],
        started: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
          args: ['cdkDragStarted']
        }],
        released: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
          args: ['cdkDragReleased']
        }],
        ended: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
          args: ['cdkDragEnded']
        }],
        entered: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
          args: ['cdkDragEntered']
        }],
        exited: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
          args: ['cdkDragExited']
        }],
        dropped: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
          args: ['cdkDragDropped']
        }],
        moved: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
          args: ['cdkDragMoved']
        }]
      };
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CdkDrag, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
          args: [{
            selector: '[cdkDrag]',
            exportAs: 'cdkDrag',
            host: {
              'class': 'cdk-drag',
              '[class.cdk-drag-disabled]': 'disabled',
              '[class.cdk-drag-dragging]': '_dragRef.isDragging()'
            },
            providers: [{
              provide: CDK_DRAG_PARENT,
              useExisting: CdkDrag
            }]
          }]
        }], function () {
          return [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
          }, {
            type: undefined,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
              args: [CDK_DROP_LIST]
            }, {
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
            }, {
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["SkipSelf"]
            }]
          }, {
            type: undefined,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
              args: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"]]
            }]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"]
          }, {
            type: undefined,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
              args: [CDK_DRAG_CONFIG]
            }]
          }, {
            type: _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_7__["Directionality"],
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
            }]
          }, {
            type: DragDrop
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
          }];
        }, {
          dragStartDelay: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['cdkDragStartDelay']
          }],
          started: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
            args: ['cdkDragStarted']
          }],
          released: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
            args: ['cdkDragReleased']
          }],
          ended: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
            args: ['cdkDragEnded']
          }],
          entered: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
            args: ['cdkDragEntered']
          }],
          exited: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
            args: ['cdkDragExited']
          }],
          dropped: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
            args: ['cdkDragDropped']
          }],
          moved: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
            args: ['cdkDragMoved']
          }],
          disabled: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['cdkDragDisabled']
          }],
          _handles: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChildren"],
            args: [CdkDragHandle, {
              descendants: true
            }]
          }],
          _previewTemplate: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"],
            args: [CdkDragPreview]
          }],
          _placeholderTemplate: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"],
            args: [CdkDragPlaceholder]
          }],
          data: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['cdkDragData']
          }],
          lockAxis: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['cdkDragLockAxis']
          }],
          rootElementSelector: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['cdkDragRootElement']
          }],
          boundaryElement: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['cdkDragBoundary']
          }],
          freeDragPosition: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['cdkDragFreeDragPosition']
          }],
          constrainPosition: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['cdkDragConstrainPosition']
          }],
          previewClass: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['cdkDragPreviewClass']
          }]
        });
      })();

      if (false) {}
      /**
       * Gets the closest ancestor of an element that matches a selector.
       * @param {?} element
       * @param {?} selector
       * @return {?}
       */


      function getClosestMatchingAncestor(element, selector) {
        /** @type {?} */
        var currentElement =
        /** @type {?} */
        element.parentElement;

        while (currentElement) {
          // IE doesn't support `matches` so we have to fall back to `msMatchesSelector`.
          if (currentElement.matches ? currentElement.matches(selector) :
          /** @type {?} */
          currentElement.msMatchesSelector(selector)) {
            return currentElement;
          }

          currentElement = currentElement.parentElement;
        }

        return null;
      }
      /**
       * @fileoverview added by tsickle
       * Generated from: src/cdk/drag-drop/directives/drop-list-group.ts
       * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * Declaratively connects sibling `cdkDropList` instances together. All of the `cdkDropList`
       * elements that are placed inside a `cdkDropListGroup` will be connected to each other
       * automatically. Can be used as an alternative to the `cdkDropListConnectedTo` input
       * from `cdkDropList`.
       * @template T
       */


      var CdkDropListGroup = /*#__PURE__*/function () {
        function CdkDropListGroup() {
          _classCallCheck(this, CdkDropListGroup);

          /**
           * Drop lists registered inside the group.
           */
          this._items = new Set();
          this._disabled = false;
        }
        /**
         * Whether starting a dragging sequence from inside this group is disabled.
         * @return {?}
         */


        _createClass(CdkDropListGroup, [{
          key: "ngOnDestroy",

          /**
           * @return {?}
           */
          value: function ngOnDestroy() {
            this._items.clear();
          }
        }, {
          key: "disabled",
          get: function get() {
            return this._disabled;
          }
          /**
           * @param {?} value
           * @return {?}
           */
          ,
          set: function set(value) {
            this._disabled = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceBooleanProperty"])(value);
          }
        }]);

        return CdkDropListGroup;
      }();

      CdkDropListGroup.ɵfac = function CdkDropListGroup_Factory(t) {
        return new (t || CdkDropListGroup)();
      };

      CdkDropListGroup.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineDirective"]({
        type: CdkDropListGroup,
        selectors: [["", "cdkDropListGroup", ""]],
        inputs: {
          disabled: ["cdkDropListGroupDisabled", "disabled"]
        },
        exportAs: ["cdkDropListGroup"]
      });
      CdkDropListGroup.propDecorators = {
        disabled: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['cdkDropListGroupDisabled']
        }]
      };
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CdkDropListGroup, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
          args: [{
            selector: '[cdkDropListGroup]',
            exportAs: 'cdkDropListGroup'
          }]
        }], function () {
          return [];
        }, {
          disabled: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['cdkDropListGroupDisabled']
          }]
        });
      })();

      if (false) {}
      /**
       * @fileoverview added by tsickle
       * Generated from: src/cdk/drag-drop/directives/drop-list.ts
       * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * Counter used to generate unique ids for drop zones.
       * @type {?}
       */


      var _uniqueIdCounter = 0;
      /**
       * Internal compile-time-only representation of a `CdkDropList`.
       * Used to avoid circular import issues between the `CdkDropList` and the `CdkDrag`.
       * \@docs-private
       * @record
       */

      function CdkDropListInternal() {}

      var ɵ0 = undefined;
      /**
       * Container that wraps a set of draggable items.
       * @template T
       */

      var CdkDropList = /*#__PURE__*/function () {
        /**
         * @param {?} element
         * @param {?} dragDrop
         * @param {?} _changeDetectorRef
         * @param {?=} _dir
         * @param {?=} _group
         */
        function CdkDropList(element, dragDrop, _changeDetectorRef, _dir, _group) {
          var _this25 = this;

          _classCallCheck(this, CdkDropList);

          this.element = element;
          this._changeDetectorRef = _changeDetectorRef;
          this._dir = _dir;
          this._group = _group;
          /**
           * Emits when the list has been destroyed.
           */

          this._destroyed = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
          /**
           * Other draggable containers that this container is connected to and into which the
           * container's items can be transferred. Can either be references to other drop containers,
           * or their unique IDs.
           */

          this.connectedTo = [];
          /**
           * Direction in which the list is oriented.
           */

          this.orientation = 'vertical';
          /**
           * Unique ID for the drop zone. Can be used as a reference
           * in the `connectedTo` of another `CdkDropList`.
           */

          this.id = "cdk-drop-list-".concat(_uniqueIdCounter++);
          this._disabled = false;
          /**
           * Whether sorting within this drop list is disabled.
           */

          this.sortingDisabled = false;
          /**
           * Function that is used to determine whether an item
           * is allowed to be moved into a drop container.
           */

          this.enterPredicate =
          /**
          * @return {?}
          */
          function () {
            return true;
          };
          /**
           * Whether to auto-scroll the view when the user moves their pointer close to the edges.
           */


          this.autoScrollDisabled = false;
          /**
           * Emits when the user drops an item inside the container.
           */

          this.dropped = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          /**
           * Emits when the user has moved a new drag item into this container.
           */

          this.entered = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          /**
           * Emits when the user removes an item from the container
           * by dragging it into another container.
           */

          this.exited = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          /**
           * Emits as the user is swapping items while actively dragging.
           */

          this.sorted = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this._dropListRef = dragDrop.createDropList(element);
          this._dropListRef.data = this;

          this._dropListRef.enterPredicate =
          /**
          * @param {?} drag
          * @param {?} drop
          * @return {?}
          */
          function (drag, drop) {
            return _this25.enterPredicate(drag.data, drop.data);
          };

          this._setupInputSyncSubscription(this._dropListRef);

          this._handleEvents(this._dropListRef);

          CdkDropList._dropLists.push(this);

          if (_group) {
            _group._items.add(this);
          }
        }
        /**
         * Whether starting a dragging sequence from this container is disabled.
         * @return {?}
         */


        _createClass(CdkDropList, [{
          key: "ngAfterContentInit",

          /**
           * @return {?}
           */
          value: function ngAfterContentInit() {
            var _this26 = this;

            this._draggables.changes.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["startWith"])(this._draggables), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["takeUntil"])(this._destroyed)).subscribe(
            /**
            * @param {?} items
            * @return {?}
            */
            function (items) {
              _this26._dropListRef.withItems(items.reduce(
              /**
              * @param {?} filteredItems
              * @param {?} drag
              * @return {?}
              */
              function (filteredItems, drag) {
                if (drag.dropContainer === _this26) {
                  filteredItems.push(drag._dragRef);
                }

                return filteredItems;
              },
              /** @type {?} */
              []));
            });
          }
          /**
           * @return {?}
           */

        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            /** @type {?} */
            var index = CdkDropList._dropLists.indexOf(this);

            if (index > -1) {
              CdkDropList._dropLists.splice(index, 1);
            }

            if (this._group) {
              this._group._items["delete"](this);
            }

            this._dropListRef.dispose();

            this._destroyed.next();

            this._destroyed.complete();
          }
          /**
           * Starts dragging an item.
           * @deprecated No longer being used. To be removed.
           * \@breaking-change 10.0.0
           * @return {?}
           */

        }, {
          key: "start",
          value: function start() {
            this._dropListRef.start();
          }
          /**
           * Drops an item into this container.
           * @deprecated No longer being used. To be removed.
           * \@breaking-change 10.0.0
           * @param {?} item Item being dropped into the container.
           * @param {?} currentIndex Index at which the item should be inserted.
           * @param {?} previousContainer Container from which the item got dragged in.
           * @param {?} isPointerOverContainer Whether the user's pointer was over the
           *    container when the item was dropped.
           *
           * @return {?}
           */

        }, {
          key: "drop",
          value: function drop(item, currentIndex, previousContainer, isPointerOverContainer) {
            this._dropListRef.drop(item._dragRef, currentIndex, previousContainer._dropListRef, isPointerOverContainer, {
              x: 0,
              y: 0
            });
          }
          /**
           * Emits an event to indicate that the user moved an item into the container.
           * @deprecated No longer being used. To be removed.
           * \@breaking-change 10.0.0
           * @param {?} item Item that was moved into the container.
           * @param {?} pointerX Position of the item along the X axis.
           * @param {?} pointerY Position of the item along the Y axis.
           * @return {?}
           */

        }, {
          key: "enter",
          value: function enter(item, pointerX, pointerY) {
            this._dropListRef.enter(item._dragRef, pointerX, pointerY);
          }
          /**
           * Removes an item from the container after it was dragged into another container by the user.
           * @deprecated No longer being used. To be removed.
           * \@breaking-change 10.0.0
           * @param {?} item Item that was dragged out.
           * @return {?}
           */

        }, {
          key: "exit",
          value: function exit(item) {
            this._dropListRef.exit(item._dragRef);
          }
          /**
           * Figures out the index of an item in the container.
           * @deprecated No longer being used. To be removed.
           * \@breaking-change 10.0.0
           * @param {?} item Item whose index should be determined.
           * @return {?}
           */

        }, {
          key: "getItemIndex",
          value: function getItemIndex(item) {
            return this._dropListRef.getItemIndex(item._dragRef);
          }
          /**
           * Syncs the inputs of the CdkDropList with the options of the underlying DropListRef.
           * @private
           * @param {?} ref
           * @return {?}
           */

        }, {
          key: "_setupInputSyncSubscription",
          value: function _setupInputSyncSubscription(ref) {
            var _this27 = this;

            if (this._dir) {
              this._dir.change.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["startWith"])(this._dir.value), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["takeUntil"])(this._destroyed)).subscribe(
              /**
              * @param {?} value
              * @return {?}
              */
              function (value) {
                return ref.withDirection(value);
              });
            }

            ref.beforeStarted.subscribe(
            /**
            * @return {?}
            */
            function () {
              /** @type {?} */
              var siblings = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceArray"])(_this27.connectedTo).map(
              /**
              * @param {?} drop
              * @return {?}
              */
              function (drop) {
                return typeof drop === 'string' ?
                /** @type {?} */
                CdkDropList._dropLists.find(
                /**
                * @param {?} list
                * @return {?}
                */
                function (list) {
                  return list.id === drop;
                }) : drop;
              });

              if (_this27._group) {
                _this27._group._items.forEach(
                /**
                * @param {?} drop
                * @return {?}
                */
                function (drop) {
                  if (siblings.indexOf(drop) === -1) {
                    siblings.push(drop);
                  }
                });
              }

              ref.disabled = _this27.disabled;
              ref.lockAxis = _this27.lockAxis;
              ref.sortingDisabled = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceBooleanProperty"])(_this27.sortingDisabled);
              ref.autoScrollDisabled = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceBooleanProperty"])(_this27.autoScrollDisabled);
              ref.connectedTo(siblings.filter(
              /**
              * @param {?} drop
              * @return {?}
              */
              function (drop) {
                return drop && drop !== _this27;
              }).map(
              /**
              * @param {?} list
              * @return {?}
              */
              function (list) {
                return list._dropListRef;
              })).withOrientation(_this27.orientation);
            });
          }
          /**
           * Handles events from the underlying DropListRef.
           * @private
           * @param {?} ref
           * @return {?}
           */

        }, {
          key: "_handleEvents",
          value: function _handleEvents(ref) {
            var _this28 = this;

            ref.beforeStarted.subscribe(
            /**
            * @return {?}
            */
            function () {
              _this28._changeDetectorRef.markForCheck();
            });
            ref.entered.subscribe(
            /**
            * @param {?} event
            * @return {?}
            */
            function (event) {
              _this28.entered.emit({
                container: _this28,
                item: event.item.data,
                currentIndex: event.currentIndex
              });
            });
            ref.exited.subscribe(
            /**
            * @param {?} event
            * @return {?}
            */
            function (event) {
              _this28.exited.emit({
                container: _this28,
                item: event.item.data
              });

              _this28._changeDetectorRef.markForCheck();
            });
            ref.sorted.subscribe(
            /**
            * @param {?} event
            * @return {?}
            */
            function (event) {
              _this28.sorted.emit({
                previousIndex: event.previousIndex,
                currentIndex: event.currentIndex,
                container: _this28,
                item: event.item.data
              });
            });
            ref.dropped.subscribe(
            /**
            * @param {?} event
            * @return {?}
            */
            function (event) {
              _this28.dropped.emit({
                previousIndex: event.previousIndex,
                currentIndex: event.currentIndex,
                previousContainer: event.previousContainer.data,
                container: event.container.data,
                item: event.item.data,
                isPointerOverContainer: event.isPointerOverContainer,
                distance: event.distance
              }); // Mark for check since all of these events run outside of change
              // detection and we're not guaranteed for something else to have triggered it.


              _this28._changeDetectorRef.markForCheck();
            });
          }
        }, {
          key: "disabled",
          get: function get() {
            return this._disabled || !!this._group && this._group.disabled;
          }
          /**
           * @param {?} value
           * @return {?}
           */
          ,
          set: function set(value) {
            // Usually we sync the directive and ref state right before dragging starts, in order to have
            // a single point of failure and to avoid having to use setters for everything. `disabled` is
            // a special case, because it can prevent the `beforeStarted` event from firing, which can lock
            // the user in a disabled state, so we also need to sync it as it's being set.
            this._dropListRef.disabled = this._disabled = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_4__["coerceBooleanProperty"])(value);
          }
        }]);

        return CdkDropList;
      }();

      CdkDropList.ɵfac = function CdkDropList_Factory(t) {
        return new (t || CdkDropList)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](DragDrop), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_7__["Directionality"], 8), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](CdkDropListGroup, 12));
      };

      CdkDropList.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineDirective"]({
        type: CdkDropList,
        selectors: [["", "cdkDropList", ""], ["cdk-drop-list"]],
        contentQueries: function CdkDropList_ContentQueries(rf, ctx, dirIndex) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵcontentQuery"](dirIndex, CdkDrag, true);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx._draggables = _t);
          }
        },
        hostAttrs: [1, "cdk-drop-list"],
        hostVars: 7,
        hostBindings: function CdkDropList_HostBindings(rf, ctx) {
          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵhostProperty"]("id", ctx.id);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("cdk-drop-list-disabled", ctx.disabled)("cdk-drop-list-dragging", ctx._dropListRef.isDragging())("cdk-drop-list-receiving", ctx._dropListRef.isReceiving());
          }
        },
        inputs: {
          connectedTo: ["cdkDropListConnectedTo", "connectedTo"],
          orientation: ["cdkDropListOrientation", "orientation"],
          id: "id",
          sortingDisabled: ["cdkDropListSortingDisabled", "sortingDisabled"],
          enterPredicate: ["cdkDropListEnterPredicate", "enterPredicate"],
          autoScrollDisabled: ["cdkDropListAutoScrollDisabled", "autoScrollDisabled"],
          disabled: ["cdkDropListDisabled", "disabled"],
          data: ["cdkDropListData", "data"],
          lockAxis: ["cdkDropListLockAxis", "lockAxis"]
        },
        outputs: {
          dropped: "cdkDropListDropped",
          entered: "cdkDropListEntered",
          exited: "cdkDropListExited",
          sorted: "cdkDropListSorted"
        },
        exportAs: ["cdkDropList"],
        features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵProvidersFeature"]([// Prevent child drop lists from picking up the same group as their parent.
        {
          provide: CdkDropListGroup,
          useValue: ɵ0
        }, {
          provide: CDK_DROP_LIST,
          useExisting: CdkDropList
        }])]
      });
      /**
       * Keeps track of the drop lists that are currently on the page.
       */

      CdkDropList._dropLists = [];
      /** @nocollapse */

      CdkDropList.ctorParameters = function () {
        return [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
        }, {
          type: DragDrop
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
        }, {
          type: _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_7__["Directionality"],
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
          }]
        }, {
          type: CdkDropListGroup,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["SkipSelf"]
          }]
        }];
      };

      CdkDropList.propDecorators = {
        _draggables: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChildren"],
          args: [CdkDrag, {
            descendants: true
          }]
        }],
        connectedTo: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['cdkDropListConnectedTo']
        }],
        data: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['cdkDropListData']
        }],
        orientation: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['cdkDropListOrientation']
        }],
        id: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        lockAxis: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['cdkDropListLockAxis']
        }],
        disabled: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['cdkDropListDisabled']
        }],
        sortingDisabled: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['cdkDropListSortingDisabled']
        }],
        enterPredicate: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['cdkDropListEnterPredicate']
        }],
        autoScrollDisabled: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['cdkDropListAutoScrollDisabled']
        }],
        dropped: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
          args: ['cdkDropListDropped']
        }],
        entered: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
          args: ['cdkDropListEntered']
        }],
        exited: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
          args: ['cdkDropListExited']
        }],
        sorted: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
          args: ['cdkDropListSorted']
        }]
      };
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CdkDropList, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
          args: [{
            selector: '[cdkDropList], cdk-drop-list',
            exportAs: 'cdkDropList',
            providers: [// Prevent child drop lists from picking up the same group as their parent.
            {
              provide: CdkDropListGroup,
              useValue: ɵ0
            }, {
              provide: CDK_DROP_LIST,
              useExisting: CdkDropList
            }],
            host: {
              'class': 'cdk-drop-list',
              '[id]': 'id',
              '[class.cdk-drop-list-disabled]': 'disabled',
              '[class.cdk-drop-list-dragging]': '_dropListRef.isDragging()',
              '[class.cdk-drop-list-receiving]': '_dropListRef.isReceiving()'
            }
          }]
        }], function () {
          return [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
          }, {
            type: DragDrop
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
          }, {
            type: _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_7__["Directionality"],
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
            }]
          }, {
            type: CdkDropListGroup,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
            }, {
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["SkipSelf"]
            }]
          }];
        }, {
          connectedTo: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['cdkDropListConnectedTo']
          }],
          orientation: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['cdkDropListOrientation']
          }],
          id: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          sortingDisabled: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['cdkDropListSortingDisabled']
          }],
          enterPredicate: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['cdkDropListEnterPredicate']
          }],
          autoScrollDisabled: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['cdkDropListAutoScrollDisabled']
          }],
          dropped: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
            args: ['cdkDropListDropped']
          }],
          entered: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
            args: ['cdkDropListEntered']
          }],
          exited: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
            args: ['cdkDropListExited']
          }],
          sorted: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
            args: ['cdkDropListSorted']
          }],
          disabled: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['cdkDropListDisabled']
          }],
          _draggables: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChildren"],
            args: [CdkDrag, {
              descendants: true
            }]
          }],
          data: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['cdkDropListData']
          }],
          lockAxis: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['cdkDropListLockAxis']
          }]
        });
      })();

      if (false) {}
      /**
       * @fileoverview added by tsickle
       * Generated from: src/cdk/drag-drop/drag-drop-module.ts
       * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */


      var DragDropModule = function DragDropModule() {
        _classCallCheck(this, DragDropModule);
      };

      DragDropModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: DragDropModule
      });
      DragDropModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function DragDropModule_Factory(t) {
          return new (t || DragDropModule)();
        },
        providers: [DragDrop]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](DragDropModule, {
          declarations: [CdkDropList, CdkDropListGroup, CdkDrag, CdkDragHandle, CdkDragPreview, CdkDragPlaceholder],
          exports: [CdkDropList, CdkDropListGroup, CdkDrag, CdkDragHandle, CdkDragPreview, CdkDragPlaceholder]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DragDropModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            declarations: [CdkDropList, CdkDropListGroup, CdkDrag, CdkDragHandle, CdkDragPreview, CdkDragPlaceholder],
            exports: [CdkDropList, CdkDropListGroup, CdkDrag, CdkDragHandle, CdkDragPreview, CdkDragPlaceholder],
            providers: [DragDrop]
          }]
        }], null, null);
      })();
      /**
       * @fileoverview added by tsickle
       * Generated from: src/cdk/drag-drop/public-api.ts
       * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * Generated bundle index. Do not edit.
       */
      //# sourceMappingURL=drag-drop.js.map

      /***/

    },

    /***/
    "./src/app/views/todo/tag-dialogue/tag-dialogue.component.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/views/todo/tag-dialogue/tag-dialogue.component.ts ***!
      \*******************************************************************/

    /*! exports provided: TagDialogueComponent */

    /***/
    function srcAppViewsTodoTagDialogueTagDialogueComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TagDialogueComponent", function () {
        return TagDialogueComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/fesm2015/core.js");
      /* harmony import */


      var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/material/dialog */
      "./node_modules/@angular/material/fesm2015/dialog.js");
      /* harmony import */


      var _todo_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../todo.service */
      "./src/app/views/todo/todo.service.ts");
      /* harmony import */


      var _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/flex-layout/flex */
      "./node_modules/@angular/flex-layout/esm2015/flex.js");
      /* harmony import */


      var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/material/form-field */
      "./node_modules/@angular/material/fesm2015/form-field.js");
      /* harmony import */


      var _angular_material_input__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/material/input */
      "./node_modules/@angular/material/fesm2015/input.js");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/material/button */
      "./node_modules/@angular/material/fesm2015/button.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/fesm2015/common.js");

      function TagDialogueComponent_div_8_Template(rf, ctx) {
        if (rf & 1) {
          var _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TagDialogueComponent_div_8_Template_button_click_5_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5);

            var i_r3 = ctx.index;
            var tag_r2 = ctx.$implicit;

            var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            ctx_r4.tagList.splice(i_r3, 1);
            return ctx_r4.deleteTag(tag_r2);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Delete");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var tag_r2 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](tag_r2.id);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](tag_r2.name);
        }
      }

      var TagDialogueComponent = /*#__PURE__*/function () {
        function TagDialogueComponent(cdr, todoService, dialogRef, data) {
          _classCallCheck(this, TagDialogueComponent);

          this.cdr = cdr;
          this.todoService = todoService;
          this.dialogRef = dialogRef;
          this.data = data;
        }

        _createClass(TagDialogueComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this29 = this;

            this.todoService.getTagList().subscribe(function (tagList) {
              _this29.tagList = tagList;

              _this29.cdr.markForCheck();
            });
          }
        }, {
          key: "addTag",
          value: function addTag(tagName) {
            if (tagName) {
              var tag = {
                id: this.tagList.length + 1,
                name: tagName
              };
              this.tagList.push(tag);
              this.todoService.saveTag(tag).subscribe(function (res) {});
              this.cdr.markForCheck();
            }
          }
        }, {
          key: "deleteTag",
          value: function deleteTag(tag) {
            this.todoService.deleteTag(tag);
          }
        }]);

        return TagDialogueComponent;
      }();

      TagDialogueComponent.ɵfac = function TagDialogueComponent_Factory(t) {
        return new (t || TagDialogueComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_todo_service__WEBPACK_IMPORTED_MODULE_2__["TodoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"]));
      };

      TagDialogueComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: TagDialogueComponent,
        selectors: [["app-tag-dialogue"]],
        decls: 12,
        vars: 1,
        consts: [["fxLayout", "row", "fxLayoutAlign", "space-between center"], ["matInput", "", "placeholder", "New tag", "required", "", 3, "keyup.enter"], ["tagInput", ""], ["color", "primary", "mat-raised-button", "", 3, "click"], ["fxLayout", "column", "fxLayoutGap", "16px", 2, "margin-top", "20px"], ["fxLayout", "row", "fxLayoutAlign", "space-between center", 4, "ngFor", "ngForOf"], ["align", "end"], ["mat-button", "", "mat-dialog-close", ""], ["color", "warn", "mat-raised-button", "", 3, "click"]],
        template: function TagDialogueComponent_Template(rf, ctx) {
          if (rf & 1) {
            var _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-dialog-content");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-form-field");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "input", 1, 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.enter", function TagDialogueComponent_Template_input_keyup_enter_3_listener() {
              _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6);

              var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](4);

              ctx.addTag(_r0.value);
              return _r0.value = "";
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TagDialogueComponent_Template_button_click_5_listener() {
              _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6);

              var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](4);

              ctx.addTag(_r0.value);
              return _r0.value = "";
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Add");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, TagDialogueComponent_div_8_Template, 7, 2, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-dialog-actions", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Close");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.tagList);
          }
        },
        directives: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogContent"], _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_3__["DefaultLayoutDirective"], _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_3__["DefaultLayoutAlignDirective"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatFormField"], _angular_material_input__WEBPACK_IMPORTED_MODULE_5__["MatInput"], _angular_material_button__WEBPACK_IMPORTED_MODULE_6__["MatButton"], _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_3__["DefaultLayoutGapDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgForOf"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogActions"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogClose"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3RvZG8vdGFnLWRpYWxvZ3VlL3RhZy1kaWFsb2d1ZS5jb21wb25lbnQuc2NzcyJ9 */"],
        changeDetection: 0
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TagDialogueComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-tag-dialogue',
            templateUrl: './tag-dialogue.component.html',
            styleUrls: ['./tag-dialogue.component.scss'],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush
          }]
        }], function () {
          return [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
          }, {
            type: _todo_service__WEBPACK_IMPORTED_MODULE_2__["TodoService"]
          }, {
            type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"]
          }, {
            type: undefined,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
              args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"]]
            }]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "./src/app/views/todo/todo-details/todo-details.component.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/views/todo/todo-details/todo-details.component.ts ***!
      \*******************************************************************/

    /*! exports provided: TodoDetailsComponent */

    /***/
    function srcAppViewsTodoTodoDetailsTodoDetailsComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TodoDetailsComponent", function () {
        return TodoDetailsComponent;
      });
      /* harmony import */


      var _tag_dialogue_tag_dialogue_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./../tag-dialogue/tag-dialogue.component */
      "./src/app/views/todo/tag-dialogue/tag-dialogue.component.ts");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/fesm2015/forms.js");
      /* harmony import */


      var _todo_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./../todo.service */
      "./src/app/views/todo/todo.service.ts");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/fesm2015/http.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/fesm2015/router.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/fesm2015/core.js");
      /* harmony import */


      var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/material/dialog */
      "./node_modules/@angular/material/fesm2015/dialog.js");
      /* harmony import */


      var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/material/toolbar */
      "./node_modules/@angular/material/fesm2015/toolbar.js");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/material/button */
      "./node_modules/@angular/material/fesm2015/button.js");
      /* harmony import */


      var _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @angular/material/icon */
      "./node_modules/@angular/material/fesm2015/icon.js");
      /* harmony import */


      var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/material/checkbox */
      "./node_modules/@angular/material/fesm2015/checkbox.js");
      /* harmony import */


      var _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @angular/flex-layout/flex */
      "./node_modules/@angular/flex-layout/esm2015/flex.js");
      /* harmony import */


      var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @angular/material/tooltip */
      "./node_modules/@angular/material/fesm2015/tooltip.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/fesm2015/common.js");
      /* harmony import */


      var _angular_flex_layout_extended__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! @angular/flex-layout/extended */
      "./node_modules/@angular/flex-layout/esm2015/extended.js");
      /* harmony import */


      var _angular_material_menu__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
      /*! @angular/material/menu */
      "./node_modules/@angular/material/fesm2015/menu.js");
      /* harmony import */


      var _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
      /*! @angular/material/chips */
      "./node_modules/@angular/material/fesm2015/chips.js");
      /* harmony import */


      var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
      /*! @angular/material/form-field */
      "./node_modules/@angular/material/fesm2015/form-field.js");
      /* harmony import */


      var _angular_material_input__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
      /*! @angular/material/input */
      "./node_modules/@angular/material/fesm2015/input.js");
      /* harmony import */


      var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
      /*! @angular/material/datepicker */
      "./node_modules/@angular/material/fesm2015/datepicker.js");

      function TodoDetailsComponent_button_25_Template(rf, ctx) {
        if (rf & 1) {
          var _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "button", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function TodoDetailsComponent_button_25_Template_button_click_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r7);

            var tag_r5 = ctx.$implicit;

            var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();

            return ctx_r6.updateTodoTag(tag_r5.id);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var tag_r5 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtextInterpolate1"](" ", tag_r5.name, " ");
        }
      }

      function TodoDetailsComponent_div_32_mat_chip_1_Template(rf, ctx) {
        if (rf & 1) {
          var _r12 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "mat-chip", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("removed", function TodoDetailsComponent_div_32_mat_chip_1_Template_mat_chip_removed_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r12);

            var tagId_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]().$implicit;

            var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();

            return ctx_r10.removeTagFromTodo(tagId_r8);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](1, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](3, "mat-icon", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](4, "cancel");

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var tagId_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]().$implicit;

          var ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("selectable", true)("removable", true);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtextInterpolate"](ctx_r9.tagMap.get(tagId_r8));
        }
      }

      function TodoDetailsComponent_div_32_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](1, TodoDetailsComponent_div_32_mat_chip_1_Template, 5, 3, "mat-chip", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var tagId_r8 = ctx.$implicit;

          var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", ctx_r3.tagMap.get(tagId_r8));
        }
      }

      function TodoDetailsComponent_form_33_Template(rf, ctx) {
        if (rf & 1) {
          var _r17 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "form", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngSubmit", function TodoDetailsComponent_form_33_Template_form_ngSubmit_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r17);

            var ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();

            return ctx_r16.saveTodo();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](1, "mat-form-field");

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](2, "input", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](3, "mat-form-field");

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](4, "textarea", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](5, "div", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](6, "mat-form-field");

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](7, "input", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](8, "mat-datepicker-toggle", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](9, "mat-datepicker", null, 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](11, "mat-form-field");

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](12, "input", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](13, "mat-datepicker-toggle", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](14, "mat-datepicker", null, 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](16, "div", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](17, "button", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](18, "Cancel");

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](19, "button", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](20, "Save");

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var _r14 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵreference"](10);

          var _r15 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵreference"](15);

          var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("formGroup", ctx_r4.todoForm);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("matDatepicker", _r14);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("for", _r14);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("matDatepicker", _r15);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("for", _r15);
        }
      }

      var _c0 = function _c0(a0) {
        return {
          "fill-color-warn": a0
        };
      };

      var _c1 = function _c1(a0) {
        return {
          "fill-color-accent": a0
        };
      };

      var TodoDetailsComponent = /*#__PURE__*/function () {
        function TodoDetailsComponent(tagDialogue, fb, router, route, http, todoService, cdr) {
          _classCallCheck(this, TodoDetailsComponent);

          this.tagDialogue = tagDialogue;
          this.fb = fb;
          this.router = router;
          this.route = route;
          this.http = http;
          this.todoService = todoService;
          this.cdr = cdr;
          this.todo = {
            title: '',
            note: '',
            startDate: '',
            dueDate: '',
            tag: []
          };
          this.tagMap = new Map();
        }

        _createClass(TodoDetailsComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this30 = this;

            var id = this.route.snapshot.params['id'];

            if (id) {
              this.todoService.getTodoById(id).subscribe(function (todo) {
                _this30.todo = todo;

                _this30.buildTodoForm(_this30.todo);
              });
            } else this.buildTodoForm();

            this.getTagList();
          }
        }, {
          key: "getTagList",
          value: function getTagList() {
            var _this31 = this;

            this.tagMap.clear();
            this.todoService.getTagList().subscribe(function (tagList) {
              _this31.tagList = tagList;
              tagList.forEach(function (tag) {
                _this31.tagMap.set(tag.id, tag.name);
              });

              _this31.cdr.markForCheck();
            });
          }
        }, {
          key: "buildTodoForm",
          value: function buildTodoForm(todo) {
            this.todoForm = this.fb.group({
              title: [todo ? todo.title : ''],
              note: [todo ? todo.note : ''],
              startDate: [todo ? todo.startDate : ''],
              dueDate: [todo ? todo.dueDate : '']
            });
          }
        }, {
          key: "saveTodo",
          value: function saveTodo() {
            var _this32 = this;

            if (this.todoForm.invalid) return;
            this.todo.title = this.todoForm.get("title").value;
            this.todo.note = this.todoForm.get("note").value;
            this.todo.startDate = this.todoForm.get("startDate").value;
            this.todo.dueDate = this.todoForm.get("dueDate").value;
            this.todoService.updateTodo(this.todo).subscribe(function (res) {
              _this32.router.navigateByUrl("/todo/list");
            });
          }
        }, {
          key: "updateTodoTag",
          value: function updateTodoTag(id) {
            if (!this.todo.tag.includes(id)) {
              this.todo.tag.push(id);
              this.cdr.markForCheck();
            }
          }
        }, {
          key: "removeTagFromTodo",
          value: function removeTagFromTodo(tagId) {
            this.todo.tag.splice(this.todo.tag.indexOf(tagId), 1);
            this.cdr.markForCheck();
          }
        }, {
          key: "deleteTodo",
          value: function deleteTodo() {
            var _this33 = this;

            if (this.todo.id) {
              this.todoService.deleteTodo(this.todo).subscribe(function (e) {
                _this33.router.navigateByUrl("/todo/list");
              });
            }
          }
        }, {
          key: "openTagManaginDialogue",
          value: function openTagManaginDialogue() {
            var _this34 = this;

            var dialogRef = this.tagDialogue.open(_tag_dialogue_tag_dialogue_component__WEBPACK_IMPORTED_MODULE_0__["TagDialogueComponent"], {// width: '250px',
              // data: {name: "", animal: ""}
            });
            dialogRef.afterClosed().subscribe(function (result) {
              _this34.getTagList();
            });
          }
        }]);

        return TodoDetailsComponent;
      }();

      TodoDetailsComponent.ɵfac = function TodoDetailsComponent_Factory(t) {
        return new (t || TodoDetailsComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialog"]), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_todo_service__WEBPACK_IMPORTED_MODULE_2__["TodoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__["ChangeDetectorRef"]));
      };

      TodoDetailsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineComponent"]({
        type: TodoDetailsComponent,
        selectors: [["app-todo-details"]],
        decls: 34,
        vars: 19,
        consts: [["mat-icon-button", "", "routerLink", "/todo/list"], [2, "margin-left", "16px", "font-size", "14px", 3, "checked", "change"], ["fxLayout", "row", "fxFlex", "1 1 0%", "fxLayoutAlign", "end center"], ["mat-icon-button", "", 3, "matTooltip", "click"], [3, "ngClass"], ["mat-icon-button", "", "matTooltip", "Manage tags", 3, "click"], ["mat-icon-button", "", "matTooltip", "Add tags", 3, "matMenuTriggerFor"], [3, "xPosition"], ["menu", "matMenu"], ["style", "text-transform: capitalize;", "mat-menu-item", "", 3, "click", 4, "ngFor", "ngForOf"], ["mat-icon-button", "", "matTooltip", "Delete", 3, "click"], [2, "margin", "20px 0px 0px 16px"], ["chipList", ""], [4, "ngFor", "ngForOf"], ["fxLayout", "column", "fxLayoutGap", "8px", "style", "padding: 0px 16px;margin-top: 20px;", 3, "formGroup", "ngSubmit", 4, "ngIf"], ["mat-menu-item", "", 2, "text-transform", "capitalize", 3, "click"], ["fxLayout", "row", "fxLayoutAlign", "center center", 3, "selectable", "removable", "removed", 4, "ngIf"], ["fxLayout", "row", "fxLayoutAlign", "center center", 3, "selectable", "removable", "removed"], ["matChipRemove", ""], ["fxLayout", "column", "fxLayoutGap", "8px", 2, "padding", "0px 16px", "margin-top", "20px", 3, "formGroup", "ngSubmit"], ["formControlName", "title", "matInput", "", "placeholder", "Title", "required", ""], ["formControlName", "note", "matInput", "", "placeholder", "Put your notes", "matTextareaAutosize", "", "required", ""], ["fxLayout", "row", "fxLayoutAlign", "start center", "fxLayoutGap", "50px"], ["formControlName", "startDate", "matInput", "", "placeholder", "Start Date", "required", "", 3, "matDatepicker"], ["matSuffix", "", 3, "for"], ["picker", ""], ["formControlName", "dueDate", "matInput", "", "placeholder", "Due Date", "required", "", 3, "matDatepicker"], ["picker2", ""], ["fxLayout", "row", "fxLayoutAlign", "start center", "fxLayoutGap", "16px"], ["type", "button", "mat-button", "", "routerLink", "/todo/list"], ["color", "primary", "mat-raised-button", ""]],
        template: function TodoDetailsComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "mat-toolbar");

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](1, "button", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](2, "mat-icon");

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](3, "arrow_back");

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](4, "mat-checkbox", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("change", function TodoDetailsComponent_Template_mat_checkbox_change_4_listener() {
              return ctx.todo.done = !ctx.todo.done;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](5, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](7, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](8, "button", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function TodoDetailsComponent_Template_button_click_8_listener() {
              return ctx.todo.read = !ctx.todo.read;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](9, "mat-icon");

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](10);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](11, "button", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function TodoDetailsComponent_Template_button_click_11_listener() {
              return ctx.todo.important = !ctx.todo.important;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](12, "mat-icon", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](13);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](14, "button", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function TodoDetailsComponent_Template_button_click_14_listener() {
              return ctx.todo.starred = !ctx.todo.starred;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](15, "mat-icon", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](16);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](17, "button", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function TodoDetailsComponent_Template_button_click_17_listener() {
              return ctx.openTagManaginDialogue();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](18, "mat-icon");

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](19, "library_add");

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](20, "button", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](21, "mat-icon");

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](22, "label");

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](23, "mat-menu", 7, 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](25, TodoDetailsComponent_button_25_Template, 2, 1, "button", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](26, "button", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function TodoDetailsComponent_Template_button_click_26_listener() {
              return ctx.deleteTodo();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](27, "mat-icon");

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](28, "delete_outline");

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](29, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](30, "mat-chip-list", null, 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](32, TodoDetailsComponent_div_32_Template, 2, 1, "div", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](33, TodoDetailsComponent_form_33_Template, 21, 5, "form", 14);
          }

          if (rf & 2) {
            var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵreference"](24);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("checked", ctx.todo == null ? null : ctx.todo.done);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtextInterpolate1"](" ", (ctx.todo == null ? null : ctx.todo.done) ? "Mark as undone" : "Mark as done", " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵpropertyInterpolate"]("matTooltip", (ctx.todo == null ? null : ctx.todo.read) ? "Mark as unread" : "Mark as read");

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtextInterpolate"]((ctx.todo == null ? null : ctx.todo.read) ? "drafts" : "markunread");

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵpropertyInterpolate"]("matTooltip", (ctx.todo == null ? null : ctx.todo.important) ? "Mark as unimportant" : "Mark as important");

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵpureFunction1"](15, _c0, ctx.todo == null ? null : ctx.todo.important));

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtextInterpolate"]((ctx.todo == null ? null : ctx.todo.important) ? "error" : "error_outline");

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵpropertyInterpolate"]("matTooltip", (ctx.todo == null ? null : ctx.todo.starred) ? "Remove star" : "Mark star");

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵpureFunction1"](17, _c1, ctx.todo == null ? null : ctx.todo.starred));

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtextInterpolate"]((ctx.todo == null ? null : ctx.todo.starred) ? "star" : "star_outline");

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("matMenuTriggerFor", _r0);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("xPosition", "before");

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngForOf", ctx.tagList);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngForOf", ctx.todo == null ? null : ctx.todo.tag);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", ctx.todoForm);
          }
        },
        directives: [_angular_material_toolbar__WEBPACK_IMPORTED_MODULE_7__["MatToolbar"], _angular_material_button__WEBPACK_IMPORTED_MODULE_8__["MatButton"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLink"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__["MatIcon"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_10__["MatCheckbox"], _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_11__["DefaultLayoutDirective"], _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_11__["DefaultFlexDirective"], _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_11__["DefaultLayoutAlignDirective"], _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_12__["MatTooltip"], _angular_common__WEBPACK_IMPORTED_MODULE_13__["NgClass"], _angular_flex_layout_extended__WEBPACK_IMPORTED_MODULE_14__["DefaultClassDirective"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_15__["MatMenuTrigger"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_15__["_MatMenu"], _angular_common__WEBPACK_IMPORTED_MODULE_13__["NgForOf"], _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__["MatChipList"], _angular_common__WEBPACK_IMPORTED_MODULE_13__["NgIf"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_15__["MatMenuItem"], _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__["MatChip"], _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__["MatChipRemove"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_11__["DefaultLayoutGapDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_17__["MatFormField"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_material_input__WEBPACK_IMPORTED_MODULE_18__["MatInput"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RequiredValidator"], _angular_material_input__WEBPACK_IMPORTED_MODULE_18__["MatTextareaAutosize"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_19__["MatDatepickerInput"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_19__["MatDatepickerToggle"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_17__["MatSuffix"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_19__["MatDatepicker"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3RvZG8vdG9kby1kZXRhaWxzL3RvZG8tZGV0YWlscy5jb21wb25lbnQuc2NzcyJ9 */"],
        changeDetection: 0
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵsetClassMetadata"](TodoDetailsComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_5__["Component"],
          args: [{
            selector: 'app-todo-details',
            templateUrl: './todo-details.component.html',
            styleUrls: ['./todo-details.component.scss'],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_5__["ChangeDetectionStrategy"].OnPush
          }]
        }], function () {
          return [{
            type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialog"]
          }, {
            type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
          }, {
            type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
          }, {
            type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
          }, {
            type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
          }, {
            type: _todo_service__WEBPACK_IMPORTED_MODULE_2__["TodoService"]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_5__["ChangeDetectorRef"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "./src/app/views/todo/todo-list/todo-list.component.ts":
    /*!*************************************************************!*\
      !*** ./src/app/views/todo/todo-list/todo-list.component.ts ***!
      \*************************************************************/

    /*! exports provided: TodoListComponent */

    /***/
    function srcAppViewsTodoTodoListTodoListComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TodoListComponent", function () {
        return TodoListComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/fesm2015/core.js");
      /* harmony import */


      var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/cdk/drag-drop */
      "./node_modules/@angular/cdk/fesm2015/drag-drop.js");
      /* harmony import */


      var _todo_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../todo.service */
      "./src/app/views/todo/todo.service.ts");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/material/toolbar */
      "./node_modules/@angular/material/fesm2015/toolbar.js");
      /* harmony import */


      var _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/flex-layout/flex */
      "./node_modules/@angular/flex-layout/esm2015/flex.js");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/material/button */
      "./node_modules/@angular/material/fesm2015/button.js");
      /* harmony import */


      var _angular_material_menu__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/material/menu */
      "./node_modules/@angular/material/fesm2015/menu.js");
      /* harmony import */


      var _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @angular/material/icon */
      "./node_modules/@angular/material/fesm2015/icon.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/fesm2015/common.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/fesm2015/router.js");
      /* harmony import */


      var _angular_material_chips__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @angular/material/chips */
      "./node_modules/@angular/material/fesm2015/chips.js");
      /* harmony import */


      var _angular_flex_layout_extended__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! @angular/flex-layout/extended */
      "./node_modules/@angular/flex-layout/esm2015/extended.js");
      /* harmony import */


      var _todo_search_pipe__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! ../todo-search.pipe */
      "./src/app/views/todo/todo-search.pipe.ts");

      function TodoListComponent_button_31_Template(rf, ctx) {
        if (rf & 1) {
          var _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TodoListComponent_button_31_Template_button_click_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7);

            var tag_r5 = ctx.$implicit;

            var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r6.filterTodoListOnTag(tag_r5);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var tag_r5 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", tag_r5.name, " ");
        }
      }

      function TodoListComponent_div_36_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Loading... ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function TodoListComponent_div_37_div_11_mat_chip_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-chip");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var tag_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r13.tagMap.get(tag_r12), " ");
        }
      }

      function TodoListComponent_div_37_div_11_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TodoListComponent_div_37_div_11_mat_chip_1_Template, 2, 1, "mat-chip", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var tag_r12 = ctx.$implicit;

          var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r10.tagMap.get(tag_r12));
        }
      }

      var _c0 = function _c0(a0) {
        return {
          "fill-color-warn": a0
        };
      };

      var _c1 = function _c1(a0) {
        return {
          "fill-color-accent": a0
        };
      };

      function TodoListComponent_div_37_Template(rf, ctx) {
        if (rf & 1) {
          var _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "open_with");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-chip-list", null, 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, TodoListComponent_div_37_div_11_Template, 2, 1, "div", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "button", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TodoListComponent_div_37_Template_button_click_13_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r16);

            var todo_r8 = ctx.$implicit;

            var ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r15.toggleImportant(todo_r8);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "mat-icon", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "button", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TodoListComponent_div_37_Template_button_click_16_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r16);

            var todo_r8 = ctx.$implicit;

            var ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r17.toggleStar(todo_r8);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "mat-icon", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "button", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "mat-icon");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "more_vert");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "mat-menu", 21, 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "button", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TodoListComponent_div_37_Template_button_click_25_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r16);

            var todo_r8 = ctx.$implicit;

            var ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r18.toggleRead(todo_r8);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "mat-icon");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "span", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "button", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TodoListComponent_div_37_Template_button_click_30_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r16);

            var todo_r8 = ctx.$implicit;

            var ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r19.toggleDone(todo_r8);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "mat-icon");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "span", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var todo_r8 = ctx.$implicit;

          var _r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("routerLink", "/todo/list/", todo_r8 == null ? null : todo_r8.id, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](todo_r8 == null ? null : todo_r8.title);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](todo_r8 == null ? null : todo_r8.note);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", todo_r8 == null ? null : todo_r8.tag);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](14, _c0, todo_r8 == null ? null : todo_r8.important));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"]((todo_r8 == null ? null : todo_r8.important) ? "error" : "error_outline");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](16, _c1, todo_r8 == null ? null : todo_r8.starred));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"]((todo_r8 == null ? null : todo_r8.starred) ? "star" : "star_outline");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("matMenuTriggerFor", _r11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("xPosition", "before");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](todo_r8.read ? "check_box" : "check_box_outline_blank");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](todo_r8.read ? "Mark as unread" : "Mark as read");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](todo_r8.done ? "check_box" : "check_box_outline_blank");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](todo_r8.done ? "Mark as undone" : "Mark as done");
        }
      }

      var TodoListComponent = /*#__PURE__*/function () {
        function TodoListComponent(todoService, cdr) {
          _classCallCheck(this, TodoListComponent);

          this.todoService = todoService;
          this.cdr = cdr;
          this.tagMap = new Map();
          this.isMasterToggled = false;
          this.toggledItemNumber = 0;
          this.unsubscribeAll = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subject"]();
        }

        _createClass(TodoListComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this35 = this;

            this.tagMap.clear();
            this.todoService.getTodoList().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this.unsubscribeAll)).subscribe(function (res) {
              _this35.todoList = res;
              _this35.tempList = res; //used for filtering data

              _this35.todoService.getTagList().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(_this35.unsubscribeAll)).subscribe(function (res) {
                _this35.tagList = res;
                res.forEach(function (tag) {
                  _this35.tagMap.set(tag.id, tag.name);

                  _this35.cdr.markForCheck();
                });
              });
            });
            this.todoService.getSearchTerm().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["debounceTime"])(250)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this.unsubscribeAll)).subscribe(function (term) {
              _this35.searchTerm = term;

              _this35.cdr.markForCheck();
            });
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.unsubscribeAll.next();
            this.unsubscribeAll.complete();
          }
        }, {
          key: "drop",
          value: function drop(event) {
            Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_1__["moveItemInArray"])(this.todoList, event.previousIndex, event.currentIndex); // Do what you need with the re-arranged array "todoList"

            this.cdr.markForCheck();
          }
        }, {
          key: "filterTodoListOnTag",
          value: function filterTodoListOnTag(tag) {
            this.todoList = this.tempList.filter(function (todo) {
              return todo.tag.includes(tag.id);
            });
            this.cdr.detectChanges();
          }
        }, {
          key: "onTodoOptionSelected",
          value: function onTodoOptionSelected(event) {
            switch (event.target.innerText) {
              case "All":
                this.todoList = this.tempList;
                break;

              case "Read":
                this.todoList = this.tempList.filter(function (todo) {
                  return !todo.read;
                });
                break;

              case "Unread":
                this.todoList = this.tempList.filter(function (todo) {
                  return !todo.read;
                });
                break;

              case "Important":
                this.todoList = this.tempList.filter(function (todo) {
                  return todo.important;
                });
                break;

              case "Unimportant":
                this.todoList = this.tempList.filter(function (todo) {
                  return !todo.important;
                });
                break;

              case "Done":
                this.todoList = this.tempList.filter(function (todo) {
                  return todo.done;
                });
                break;

              case "Undone":
                this.todoList = this.tempList.filter(function (todo) {
                  return !todo.done;
                });
                break;

              case "Starred":
                this.todoList = this.tempList.filter(function (todo) {
                  return todo.starred;
                });
                break;

              case "Unstarred":
                this.todoList = this.tempList.filter(function (todo) {
                  return !todo.starred;
                });
                break;

              default:
                break;
            }

            this.cdr.detectChanges();
          }
        }, {
          key: "masterToggle",
          value: function masterToggle() {
            var _this36 = this;

            this.toggledItemNumber = 0;
            this.isMasterToggled = !this.isMasterToggled;

            if (this.isMasterToggled) {
              this.todoList.forEach(function (todo) {
                todo.selected = true;
                _this36.toggledItemNumber++;
              });
            } else {
              this.todoList.forEach(function (todo) {
                todo.selected = false;
                _this36.toggledItemNumber--;
              });
            }

            this.cdr.detectChanges();
          }
        }, {
          key: "toggleCheckBox",
          value: function toggleCheckBox(todo) {
            todo.selected = !todo.selected;
            if (todo.selected) this.toggledItemNumber++;else this.toggledItemNumber--;
            if (this.toggledItemNumber == this.todoList.length) this.isMasterToggled = true;else if (this.toggledItemNumber == 0) this.isMasterToggled = false;
          }
        }, {
          key: "toggleImportant",
          value: function toggleImportant(todo) {
            todo.important = !todo.important;
            this.todoService.updateTodo(todo);
          }
        }, {
          key: "toggleStar",
          value: function toggleStar(todo) {
            todo.starred = !todo.starred;
            this.todoService.updateTodo(todo);
          }
        }, {
          key: "toggleRead",
          value: function toggleRead(todo) {
            todo.read = !todo.read;
            this.todoService.updateTodo(todo);
          }
        }, {
          key: "toggleDone",
          value: function toggleDone(todo) {
            todo.done = !todo.done;
            this.todoService.updateTodo(todo);
          }
        }]);

        return TodoListComponent;
      }();

      TodoListComponent.ɵfac = function TodoListComponent_Factory(t) {
        return new (t || TodoListComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_todo_service__WEBPACK_IMPORTED_MODULE_2__["TodoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]));
      };

      TodoListComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: TodoListComponent,
        selectors: [["app-todo-list"]],
        decls: 39,
        vars: 8,
        consts: [["fxLayoutGap", "5px"], ["mat-icon-button", "", 3, "matMenuTriggerFor"], ["menu", "matMenu"], ["mat-menu-item", "", 3, "click"], ["tagMenu", "matMenu"], ["mat-menu-item", "", 3, "click", 4, "ngFor", "ngForOf"], ["fxLayout", "row", "fxFlex", "1 1 0%", "fxLayoutAlign", "end center"], ["mat-raised-button", "", "color", "primary", "routerLink", "/todo/add"], ["cdkDropList", "", 1, "todo-list", 3, "cdkDropListDropped"], ["class", "todo-box", "fxLayout", "row", "fxLayoutAlign", "center center", 4, "ngIf"], ["class", "todo-box", "fxLayout", "row", "fxLayoutAlign", "start center", "fxLayoutGap", "20px", "cdkDrag", "", "cdkDragHandle", "", 4, "ngFor", "ngForOf"], ["fxLayout", "row", "fxLayoutAlign", "center center", 1, "todo-box"], ["fxLayout", "row", "fxLayoutAlign", "start center", "fxLayoutGap", "20px", "cdkDrag", "", "cdkDragHandle", "", 1, "todo-box"], [1, "move-handler"], ["fxLayout", "row", "fxLayoutAlign", "space-between center", "fxFlex", "1 1 0%"], ["fxLayout", "column", "fxLayoutGap", "8px", "fxFlex", "1 1 0%", 2, "cursor", "pointer", 3, "routerLink"], ["chipList", ""], [4, "ngFor", "ngForOf"], ["fxFlexAlign", "end center", "fxLayout", "row"], ["mat-icon-button", "", 3, "click"], [3, "ngClass"], [3, "xPosition"], ["menu2", "matMenu"], [2, "vertical-align", "middle"], [4, "ngIf"]],
        template: function TodoListComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-toolbar", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "button", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-icon");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "arrow_drop_down");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "mat-menu", null, 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TodoListComponent_Template_button_click_6_listener($event) {
              return ctx.onTodoOptionSelected($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "All");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "button", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TodoListComponent_Template_button_click_8_listener($event) {
              return ctx.onTodoOptionSelected($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Done");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TodoListComponent_Template_button_click_10_listener($event) {
              return ctx.onTodoOptionSelected($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Undone");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "button", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TodoListComponent_Template_button_click_12_listener($event) {
              return ctx.onTodoOptionSelected($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Read");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "button", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TodoListComponent_Template_button_click_14_listener($event) {
              return ctx.onTodoOptionSelected($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Unread");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "button", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TodoListComponent_Template_button_click_16_listener($event) {
              return ctx.onTodoOptionSelected($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, " Starred ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "button", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TodoListComponent_Template_button_click_18_listener($event) {
              return ctx.onTodoOptionSelected($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, " Unstarred ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "button", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TodoListComponent_Template_button_click_20_listener($event) {
              return ctx.onTodoOptionSelected($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, " Important ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "button", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TodoListComponent_Template_button_click_22_listener($event) {
              return ctx.onTodoOptionSelected($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, " Unimportant ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "button", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "mat-icon");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "label");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "mat-menu", null, 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "button", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TodoListComponent_Template_button_click_29_listener() {
              return ctx.onTodoOptionSelected({
                target: {
                  innerText: "All"
                }
              });
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, " all ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](31, TodoListComponent_button_31_Template, 2, 1, "button", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "button", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, " Create Todo ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("cdkDropListDropped", function TodoListComponent_Template_div_cdkDropListDropped_35_listener($event) {
              return ctx.drop($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](36, TodoListComponent_div_36_Template, 2, 0, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](37, TodoListComponent_div_37_Template, 35, 18, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](38, "todoSearch");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](5);

            var _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("matMenuTriggerFor", _r0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("matMenuTriggerFor", _r1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.tagList);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.todoList == null ? null : ctx.todoList.length) < 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](38, 5, ctx.todoList, ctx.searchTerm));
          }
        },
        directives: [_angular_material_toolbar__WEBPACK_IMPORTED_MODULE_5__["MatToolbar"], _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_6__["DefaultLayoutGapDirective"], _angular_material_button__WEBPACK_IMPORTED_MODULE_7__["MatButton"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_8__["MatMenuTrigger"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__["MatIcon"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_8__["_MatMenu"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_8__["MatMenuItem"], _angular_common__WEBPACK_IMPORTED_MODULE_10__["NgForOf"], _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_6__["DefaultLayoutDirective"], _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_6__["DefaultFlexDirective"], _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_6__["DefaultLayoutAlignDirective"], _angular_router__WEBPACK_IMPORTED_MODULE_11__["RouterLink"], _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_1__["CdkDropList"], _angular_common__WEBPACK_IMPORTED_MODULE_10__["NgIf"], _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_1__["CdkDrag"], _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_1__["CdkDragHandle"], _angular_material_chips__WEBPACK_IMPORTED_MODULE_12__["MatChipList"], _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_6__["DefaultFlexAlignDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_10__["NgClass"], _angular_flex_layout_extended__WEBPACK_IMPORTED_MODULE_13__["DefaultClassDirective"], _angular_material_chips__WEBPACK_IMPORTED_MODULE_12__["MatChip"]],
        pipes: [_todo_search_pipe__WEBPACK_IMPORTED_MODULE_14__["TodoSearchPipe"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3RvZG8vdG9kby1saXN0L3RvZG8tbGlzdC5jb21wb25lbnQuc2NzcyJ9 */"],
        changeDetection: 0
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TodoListComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: "app-todo-list",
            templateUrl: "./todo-list.component.html",
            styleUrls: ["./todo-list.component.scss"],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush
          }]
        }], function () {
          return [{
            type: _todo_service__WEBPACK_IMPORTED_MODULE_2__["TodoService"]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "./src/app/views/todo/todo-routing.module.ts":
    /*!***************************************************!*\
      !*** ./src/app/views/todo/todo-routing.module.ts ***!
      \***************************************************/

    /*! exports provided: TodoRoutingModule */

    /***/
    function srcAppViewsTodoTodoRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TodoRoutingModule", function () {
        return TodoRoutingModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/fesm2015/router.js");
      /* harmony import */


      var _todo_todo_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./todo/todo.component */
      "./src/app/views/todo/todo/todo.component.ts");
      /* harmony import */


      var _todo_list_todo_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./todo-list/todo-list.component */
      "./src/app/views/todo/todo-list/todo-list.component.ts");
      /* harmony import */


      var _todo_details_todo_details_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./todo-details/todo-details.component */
      "./src/app/views/todo/todo-details/todo-details.component.ts");

      var routes = [{
        path: '',
        component: _todo_todo_component__WEBPACK_IMPORTED_MODULE_2__["TodoComponent"],
        children: [{
          path: '',
          redirectTo: 'list',
          pathMatch: 'full'
        }, {
          path: 'list',
          component: _todo_list_todo_list_component__WEBPACK_IMPORTED_MODULE_3__["TodoListComponent"]
        }, {
          path: 'add',
          component: _todo_details_todo_details_component__WEBPACK_IMPORTED_MODULE_4__["TodoDetailsComponent"]
        }, {
          path: 'list/:id',
          component: _todo_details_todo_details_component__WEBPACK_IMPORTED_MODULE_4__["TodoDetailsComponent"]
        }]
      }];

      var TodoRoutingModule = function TodoRoutingModule() {
        _classCallCheck(this, TodoRoutingModule);
      };

      TodoRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: TodoRoutingModule
      });
      TodoRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function TodoRoutingModule_Factory(t) {
          return new (t || TodoRoutingModule)();
        },
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](TodoRoutingModule, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TodoRoutingModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "./src/app/views/todo/todo-search.pipe.ts":
    /*!************************************************!*\
      !*** ./src/app/views/todo/todo-search.pipe.ts ***!
      \************************************************/

    /*! exports provided: TodoSearchPipe */

    /***/
    function srcAppViewsTodoTodoSearchPipeTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TodoSearchPipe", function () {
        return TodoSearchPipe;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/fesm2015/core.js");

      var TodoSearchPipe = /*#__PURE__*/function () {
        function TodoSearchPipe() {
          _classCallCheck(this, TodoSearchPipe);
        }

        _createClass(TodoSearchPipe, [{
          key: "transform",
          value: function transform(value, term) {
            if (value !== undefined && term) {
              return value.filter(function (todo) {
                return todo.title.toLowerCase().match(term.toLowerCase()) || todo.note.toLowerCase().match(term.toLowerCase());
              });
            }

            return value;
          }
        }]);

        return TodoSearchPipe;
      }();

      TodoSearchPipe.ɵfac = function TodoSearchPipe_Factory(t) {
        return new (t || TodoSearchPipe)();
      };

      TodoSearchPipe.ɵpipe = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefinePipe"]({
        name: "todoSearch",
        type: TodoSearchPipe,
        pure: true
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TodoSearchPipe, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"],
          args: [{
            name: 'todoSearch'
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "./src/app/views/todo/todo.module.ts":
    /*!*******************************************!*\
      !*** ./src/app/views/todo/todo.module.ts ***!
      \*******************************************/

    /*! exports provided: TodoModule */

    /***/
    function srcAppViewsTodoTodoModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TodoModule", function () {
        return TodoModule;
      });
      /* harmony import */


      var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ngx-perfect-scrollbar */
      "./node_modules/ngx-perfect-scrollbar/fesm2015/ngx-perfect-scrollbar.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/fesm2015/forms.js");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/material/button */
      "./node_modules/@angular/material/fesm2015/button.js");
      /* harmony import */


      var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/material/checkbox */
      "./node_modules/@angular/material/fesm2015/checkbox.js");
      /* harmony import */


      var _angular_material_chips__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/material/chips */
      "./node_modules/@angular/material/fesm2015/chips.js");
      /* harmony import */


      var _angular_material_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/material/core */
      "./node_modules/@angular/material/fesm2015/core.js");
      /* harmony import */


      var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/material/datepicker */
      "./node_modules/@angular/material/fesm2015/datepicker.js");
      /* harmony import */


      var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/material/dialog */
      "./node_modules/@angular/material/fesm2015/dialog.js");
      /* harmony import */


      var _angular_material_divider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/material/divider */
      "./node_modules/@angular/material/fesm2015/divider.js");
      /* harmony import */


      var _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @angular/material/icon */
      "./node_modules/@angular/material/fesm2015/icon.js");
      /* harmony import */


      var _angular_material_input__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/material/input */
      "./node_modules/@angular/material/fesm2015/input.js");
      /* harmony import */


      var _angular_material_menu__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @angular/material/menu */
      "./node_modules/@angular/material/fesm2015/menu.js");
      /* harmony import */


      var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @angular/material/toolbar */
      "./node_modules/@angular/material/fesm2015/toolbar.js");
      /* harmony import */


      var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! @angular/material/tooltip */
      "./node_modules/@angular/material/fesm2015/tooltip.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/fesm2015/common.js");
      /* harmony import */


      var _todo_routing_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
      /*! ./todo-routing.module */
      "./src/app/views/todo/todo-routing.module.ts");
      /* harmony import */


      var _todo_todo_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
      /*! ./todo/todo.component */
      "./src/app/views/todo/todo/todo.component.ts");
      /* harmony import */


      var _todo_list_todo_list_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
      /*! ./todo-list/todo-list.component */
      "./src/app/views/todo/todo-list/todo-list.component.ts");
      /* harmony import */


      var _todo_details_todo_details_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
      /*! ./todo-details/todo-details.component */
      "./src/app/views/todo/todo-details/todo-details.component.ts");
      /* harmony import */


      var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
      /*! @angular/cdk/drag-drop */
      "./node_modules/@angular/cdk/fesm2015/drag-drop.js");
      /* harmony import */


      var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
      /*! @angular/flex-layout */
      "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
      /* harmony import */


      var _todo_search_pipe__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(
      /*! ./todo-search.pipe */
      "./src/app/views/todo/todo-search.pipe.ts");
      /* harmony import */


      var _tag_dialogue_tag_dialogue_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(
      /*! ./tag-dialogue/tag-dialogue.component */
      "./src/app/views/todo/tag-dialogue/tag-dialogue.component.ts");

      var TodoModule = function TodoModule() {
        _classCallCheck(this, TodoModule);
      };

      TodoModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_14__["ɵɵdefineNgModule"]({
        type: TodoModule
      });
      TodoModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_14__["ɵɵdefineInjector"]({
        factory: function TodoModule_Factory(t) {
          return new (t || TodoModule)();
        },
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_15__["CommonModule"], _todo_routing_module__WEBPACK_IMPORTED_MODULE_16__["TodoRoutingModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__["MatIconModule"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_3__["MatCheckboxModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_11__["MatMenuModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"], _angular_material_divider__WEBPACK_IMPORTED_MODULE_8__["MatDividerModule"], _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_20__["DragDropModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_21__["FlexLayoutModule"], _angular_material_core__WEBPACK_IMPORTED_MODULE_5__["MatRippleModule"], _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_12__["MatToolbarModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_10__["MatInputModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_6__["MatDatepickerModule"], _angular_material_core__WEBPACK_IMPORTED_MODULE_5__["MatNativeDateModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_10__["MatInputModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"], _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_13__["MatTooltipModule"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MatDialogModule"], _angular_material_chips__WEBPACK_IMPORTED_MODULE_4__["MatChipsModule"], ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_0__["PerfectScrollbarModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_14__["ɵɵsetNgModuleScope"](TodoModule, {
          declarations: [_todo_todo_component__WEBPACK_IMPORTED_MODULE_17__["TodoComponent"], _todo_list_todo_list_component__WEBPACK_IMPORTED_MODULE_18__["TodoListComponent"], _todo_details_todo_details_component__WEBPACK_IMPORTED_MODULE_19__["TodoDetailsComponent"], _todo_search_pipe__WEBPACK_IMPORTED_MODULE_22__["TodoSearchPipe"], _tag_dialogue_tag_dialogue_component__WEBPACK_IMPORTED_MODULE_23__["TagDialogueComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_15__["CommonModule"], _todo_routing_module__WEBPACK_IMPORTED_MODULE_16__["TodoRoutingModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__["MatIconModule"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_3__["MatCheckboxModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_11__["MatMenuModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"], _angular_material_divider__WEBPACK_IMPORTED_MODULE_8__["MatDividerModule"], _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_20__["DragDropModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_21__["FlexLayoutModule"], _angular_material_core__WEBPACK_IMPORTED_MODULE_5__["MatRippleModule"], _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_12__["MatToolbarModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_10__["MatInputModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_6__["MatDatepickerModule"], _angular_material_core__WEBPACK_IMPORTED_MODULE_5__["MatNativeDateModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_10__["MatInputModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"], _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_13__["MatTooltipModule"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MatDialogModule"], _angular_material_chips__WEBPACK_IMPORTED_MODULE_4__["MatChipsModule"], ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_0__["PerfectScrollbarModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_14__["ɵsetClassMetadata"](TodoModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_14__["NgModule"],
          args: [{
            declarations: [_todo_todo_component__WEBPACK_IMPORTED_MODULE_17__["TodoComponent"], _todo_list_todo_list_component__WEBPACK_IMPORTED_MODULE_18__["TodoListComponent"], _todo_details_todo_details_component__WEBPACK_IMPORTED_MODULE_19__["TodoDetailsComponent"], _todo_search_pipe__WEBPACK_IMPORTED_MODULE_22__["TodoSearchPipe"], _tag_dialogue_tag_dialogue_component__WEBPACK_IMPORTED_MODULE_23__["TagDialogueComponent"]],
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_15__["CommonModule"], _todo_routing_module__WEBPACK_IMPORTED_MODULE_16__["TodoRoutingModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__["MatIconModule"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_3__["MatCheckboxModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_11__["MatMenuModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"], _angular_material_divider__WEBPACK_IMPORTED_MODULE_8__["MatDividerModule"], _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_20__["DragDropModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_21__["FlexLayoutModule"], _angular_material_core__WEBPACK_IMPORTED_MODULE_5__["MatRippleModule"], _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_12__["MatToolbarModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_10__["MatInputModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_6__["MatDatepickerModule"], _angular_material_core__WEBPACK_IMPORTED_MODULE_5__["MatNativeDateModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_10__["MatInputModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"], _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_13__["MatTooltipModule"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MatDialogModule"], _angular_material_chips__WEBPACK_IMPORTED_MODULE_4__["MatChipsModule"], ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_0__["PerfectScrollbarModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "./src/app/views/todo/todo.service.ts":
    /*!********************************************!*\
      !*** ./src/app/views/todo/todo.service.ts ***!
      \********************************************/

    /*! exports provided: TodoService */

    /***/
    function srcAppViewsTodoTodoServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TodoService", function () {
        return TodoService;
      });
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/fesm2015/http.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/fesm2015/core.js");

      var TodoService = /*#__PURE__*/function () {
        function TodoService(http) {
          _classCallCheck(this, TodoService);

          this.http = http;
          this.searchTerm = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]('');
        }

        _createClass(TodoService, [{
          key: "getTodoList",
          value: function getTodoList() {
            return this.http.get("/api/todoList");
          }
        }, {
          key: "getTodoById",
          value: function getTodoById(id) {
            return this.http.get("/api/todoList/" + id);
          }
        }, {
          key: "getTagList",
          value: function getTagList() {
            return this.http.get("/api/todoTag");
          }
        }, {
          key: "updateSearchTerm",
          value: function updateSearchTerm(term) {
            this.searchTerm.next(term);
          }
        }, {
          key: "getSearchTerm",
          value: function getSearchTerm() {
            return this.searchTerm;
          }
        }, {
          key: "saveTag",
          value: function saveTag(tag) {
            return this.http.post("/api/todoTag/", tag);
          }
        }, {
          key: "deleteTag",
          value: function deleteTag(tag) {
            this.http["delete"]("/api/todoTag/" + tag.id).subscribe(function (e) {});
          }
        }, {
          key: "deleteTodo",
          value: function deleteTodo(todo) {
            return this.http["delete"]("/api/todoList/" + todo.id);
          }
        }, {
          key: "updateTodo",
          value: function updateTodo(todo) {
            var returnTodo;
            todo.selected = false;

            if (!todo.id) {
              todo.id = Math.random() * 1000000;
              returnTodo = this.http.post("/api/todoList/", todo);
            } else {
              returnTodo = this.http.put("/api/todoList/" + todo.id, todo);
            }

            if (this.sub) {
              this.sub.unsubscribe();
              this.sub = returnTodo.subscribe(function (res) {});
            } else {
              this.sub = returnTodo.subscribe(function (res) {});
            }

            return returnTodo;
          }
        }]);

        return TodoService;
      }();

      TodoService.ɵfac = function TodoService_Factory(t) {
        return new (t || TodoService)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]));
      };

      TodoService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjectable"]({
        token: TodoService,
        factory: TodoService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵsetClassMetadata"](TodoService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "./src/app/views/todo/todo/todo.component.ts":
    /*!***************************************************!*\
      !*** ./src/app/views/todo/todo/todo.component.ts ***!
      \***************************************************/

    /*! exports provided: TodoComponent */

    /***/
    function srcAppViewsTodoTodoTodoComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TodoComponent", function () {
        return TodoComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/fesm2015/core.js");
      /* harmony import */


      var _todo_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../todo.service */
      "./src/app/views/todo/todo.service.ts");
      /* harmony import */


      var _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/flex-layout/flex */
      "./node_modules/@angular/flex-layout/esm2015/flex.js");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/material/button */
      "./node_modules/@angular/material/fesm2015/button.js");
      /* harmony import */


      var _angular_material_icon__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/material/icon */
      "./node_modules/@angular/material/fesm2015/icon.js");
      /* harmony import */


      var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ngx-perfect-scrollbar */
      "./node_modules/ngx-perfect-scrollbar/fesm2015/ngx-perfect-scrollbar.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/fesm2015/router.js");

      var TodoComponent = /*#__PURE__*/function () {
        function TodoComponent(todoService) {
          _classCallCheck(this, TodoComponent);

          this.todoService = todoService;
        }

        _createClass(TodoComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.updateSearchTerm("");
          }
        }, {
          key: "updateSearchTerm",
          value: function updateSearchTerm(term) {
            this.todoService.updateSearchTerm(term);
          }
        }]);

        return TodoComponent;
      }();

      TodoComponent.ɵfac = function TodoComponent_Factory(t) {
        return new (t || TodoComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_todo_service__WEBPACK_IMPORTED_MODULE_1__["TodoService"]));
      };

      TodoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: TodoComponent,
        selectors: [["app-todo"]],
        decls: 13,
        vars: 0,
        consts: [[1, "page-layout", "overflow-hidden", "pb-8"], [1, "header-bg", "dark-blue"], [1, "content"], ["fxLayout", "row wrap", "fxLayoutAlign", "center center", 1, "content-header"], ["fxFlex", "1 1 0%", 1, "search-box", "mat-elevation-z2"], ["mat-icon-button", ""], ["type", "text", "placeholder", "Search for a task", 3, "keyup"], ["serachTerm", ""], [1, "content-body"], [1, "content-card", "default-bg", "height-100vh", 3, "perfectScrollbar"]],
        template: function TodoComponent_Template(rf, ctx) {
          if (rf & 1) {
            var _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-icon");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "search");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "input", 6, 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup", function TodoComponent_Template_input_keyup_8_listener() {
              _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r1);

              var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](9);

              return ctx.updateSearchTerm(_r0.value);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "router-outlet");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        directives: [_angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_2__["DefaultLayoutDirective"], _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_2__["DefaultLayoutAlignDirective"], _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_2__["DefaultFlexDirective"], _angular_material_button__WEBPACK_IMPORTED_MODULE_3__["MatButton"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_4__["MatIcon"], ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__["PerfectScrollbarDirective"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterOutlet"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3RvZG8vdG9kby90b2RvLmNvbXBvbmVudC5zY3NzIn0= */"],
        changeDetection: 0
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TodoComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-todo',
            templateUrl: './todo.component.html',
            styleUrls: ['./todo.component.scss'],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush
          }]
        }], function () {
          return [{
            type: _todo_service__WEBPACK_IMPORTED_MODULE_1__["TodoService"]
          }];
        }, null);
      })();
      /***/

    }
  }]);
})();
//# sourceMappingURL=views-todo-todo-module-es5.js.map