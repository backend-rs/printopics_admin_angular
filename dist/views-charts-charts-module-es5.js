(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-charts-charts-module"], {
    /***/
    "./src/app/views/charts/charts.component.ts":
    /*!**************************************************!*\
      !*** ./src/app/views/charts/charts.component.ts ***!
      \**************************************************/

    /*! exports provided: ChartsComponent */

    /***/
    function srcAppViewsChartsChartsComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ChartsComponent", function () {
        return ChartsComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/fesm2015/core.js");
      /* harmony import */


      var _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/flex-layout/flex */
      "./node_modules/@angular/flex-layout/esm2015/flex.js");
      /* harmony import */


      var _angular_material_card__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/material/card */
      "./node_modules/@angular/material/fesm2015/card.js");
      /* harmony import */


      var _angular_material_divider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/material/divider */
      "./node_modules/@angular/material/fesm2015/divider.js");
      /* harmony import */


      var ng2_charts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ng2-charts */
      "./node_modules/ng2-charts/fesm2015/ng2-charts.js");

      var ChartsComponent = /*#__PURE__*/function () {
        function ChartsComponent() {
          _classCallCheck(this, ChartsComponent);

          this.sharedChartOptions = {
            responsive: true,
            // maintainAspectRatio: false,
            legend: {
              display: false,
              position: 'bottom'
            }
          };
          this.chartColors = [{
            backgroundColor: '#3f51b5',
            borderColor: '#3f51b5',
            pointBackgroundColor: '#3f51b5',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
          }, {
            backgroundColor: '#eeeeee',
            borderColor: '#e0e0e0',
            pointBackgroundColor: '#e0e0e0',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
          }, {
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
          }];
          /*
          * Bar Chart
          */

          this.barChartLabels = ['1', '2', '3', '4', '5', '6', '7'];
          this.barChartType = 'bar';
          this.barChartLegend = true;
          this.barChartData = [{
            data: [5, 6, 7, 8, 4, 5, 5],
            label: 'Series A',
            borderWidth: 0
          }, {
            data: [5, 4, 4, 3, 6, 2, 5],
            label: 'Series B',
            borderWidth: 0
          }];
          this.barChartOptions = Object.assign({
            scaleShowVerticalLines: false,
            scales: {
              xAxes: [{
                gridLines: {
                  color: 'rgba(0,0,0,0.02)',
                  zeroLineColor: 'rgba(0,0,0,0.02)'
                }
              }],
              yAxes: [{
                gridLines: {
                  color: 'rgba(0,0,0,0.02)',
                  zeroLineColor: 'rgba(0,0,0,0.02)'
                },
                position: 'left',
                ticks: {
                  beginAtZero: true,
                  suggestedMax: 9
                }
              }]
            }
          }, this.sharedChartOptions); // Horizontal Bar Chart

          this.barChartHorizontalType = 'horizontalBar';
          this.barChartHorizontalOptions = Object.assign({
            scaleShowVerticalLines: false,
            scales: {
              xAxes: [{
                gridLines: {
                  color: 'rgba(0,0,0,0.02)',
                  zeroLineColor: 'rgba(0,0,0,0.02)'
                },
                ticks: {
                  beginAtZero: true,
                  suggestedMax: 9
                }
              }],
              yAxes: [{
                gridLines: {
                  color: 'rgba(0,0,0,0.02)',
                  zeroLineColor: 'rgba(0,0,0,0.02)'
                }
              }]
            }
          }, this.sharedChartOptions); // Bar Chart Stacked

          this.barChartStackedOptions = Object.assign({
            scaleShowVerticalLines: false,
            tooltips: {
              mode: 'index',
              intersect: false
            },
            responsive: true,
            scales: {
              xAxes: [{
                gridLines: {
                  color: 'rgba(0,0,0,0.02)',
                  zeroLineColor: 'rgba(0,0,0,0.02)'
                },
                stacked: true,
                ticks: {
                  beginAtZero: true
                }
              }],
              yAxes: [{
                gridLines: {
                  color: 'rgba(0,0,0,0.02)',
                  zeroLineColor: 'rgba(0,0,0,0.02)'
                },
                stacked: true
              }]
            }
          }, this.sharedChartOptions);
          /*
          * Line Chart Options
          */

          this.lineChartData = [{
            data: [5, 5, 7, 8, 4, 5, 5],
            label: 'Series A',
            borderWidth: 1
          }, {
            data: [5, 4, 4, 3, 6, 2, 5],
            label: 'Series B',
            borderWidth: 1
          }];
          this.lineChartLabels = ['1', '2', '3', '4', '5', '6', '7'];
          this.lineChartOptions = Object.assign({
            animation: false,
            scales: {
              xAxes: [{
                gridLines: {
                  color: 'rgba(0,0,0,0.02)',
                  zeroLineColor: 'rgba(0,0,0,0.02)'
                }
              }],
              yAxes: [{
                gridLines: {
                  color: 'rgba(0,0,0,0.02)',
                  zeroLineColor: 'rgba(0,0,0,0.02)'
                },
                ticks: {
                  beginAtZero: true,
                  suggestedMax: 9
                }
              }]
            }
          }, this.sharedChartOptions);
          this.lineChartLegend = false;
          this.lineChartType = 'line';
          this.lineChartPointsData = [{
            data: [6, 5, 8, 8, 5, 5, 4],
            label: 'Series A',
            borderWidth: 1,
            fill: false,
            pointRadius: 10,
            pointHoverRadius: 15,
            showLine: false
          }, {
            data: [5, 4, 4, 2, 6, 2, 5],
            label: 'Series B',
            borderWidth: 1,
            fill: false,
            pointRadius: 10,
            pointHoverRadius: 15,
            showLine: false
          }];
          this.lineChartPointsOptions = Object.assign({
            scales: {
              xAxes: [{
                gridLines: {
                  color: 'rgba(0,0,0,0.02)',
                  zeroLineColor: 'rgba(0,0,0,0.02)'
                }
              }],
              yAxes: [{
                gridLines: {
                  color: 'rgba(0,0,0,0.02)',
                  zeroLineColor: 'rgba(0,0,0,0.02)'
                },
                ticks: {
                  beginAtZero: true,
                  suggestedMax: 9
                }
              }]
            },
            elements: {
              point: {
                pointStyle: 'rectRot'
              }
            }
          }, this.sharedChartOptions); // Bubble Chart

          this.bubbleChartData = [{
            data: [{
              x: 4,
              y: 4,
              r: 15
            }, {
              x: 6,
              y: 12,
              r: 30
            }, {
              x: 5,
              y: 4,
              r: 10
            }, {
              x: 8,
              y: 4,
              r: 6
            }, {
              x: 7,
              y: 8,
              r: 4
            }, {
              x: 3,
              y: 13,
              r: 14
            }, {
              x: 5,
              y: 6,
              r: 8
            }, {
              x: 7,
              y: 2,
              r: 10
            }],
            label: 'Series A',
            borderWidth: 1
          }];
          this.bubbleChartType = 'bubble';
          this.bubbleChartLabels = ['1', '2', '3', '4', '5', '6', '7'];
          this.bubbleChartLegend = true;
          this.bubbleChartOptions = Object.assign({
            animation: false,
            scales: {
              xAxes: [{
                gridLines: {
                  color: 'rgba(0,0,0,0.02)',
                  zeroLineColor: 'rgba(0,0,0,0.02)'
                }
              }],
              yAxes: [{
                gridLines: {
                  color: 'rgba(0,0,0,0.02)',
                  zeroLineColor: 'rgba(0,0,0,0.02)'
                },
                ticks: {
                  beginAtZero: true,
                  suggestedMax: 9
                }
              }]
            }
          }, this.sharedChartOptions); // Doughnut

          this.doughnutChartColors = [{
            backgroundColor: ['#f44336', '#3f51b5', '#ffeb3b', '#4caf50', '#2196f']
          }];
          this.doughnutChartLabels = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
          this.doughnutChartData = [350, 450, 100];
          this.doughnutChartType = 'doughnut';
          this.doughnutOptions = Object.assign({
            elements: {
              arc: {
                borderWidth: 0
              }
            }
          }, this.sharedChartOptions);
          /*
          * Radar Chart Options
          */

          this.radarChartLabels = ['Eating', 'Drinking', 'Sleeping', 'Designing', 'Coding', 'Cycling', 'Running'];
          this.radarChartData = [{
            data: [65, 59, 90, 81, 56, 55, 40],
            label: 'Series A',
            borderWidth: 1
          }, {
            data: [28, 48, 40, 19, 96, 27, 100],
            label: 'Series B',
            borderWidth: 1
          }];
          this.radarChartType = 'radar';
          this.radarChartColors = [{
            backgroundColor: 'rgba(36, 123, 160, 0.2)',
            borderColor: 'rgba(36, 123, 160, 0.6)',
            pointBackgroundColor: 'rgba(36, 123, 160, 0.8)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(36, 123, 160, 0.8)'
          }, {
            backgroundColor: 'rgba(244, 67, 54, 0.2)',
            borderColor: 'rgba(244, 67, 54, .8)',
            pointBackgroundColor: 'rgba(244, 67, 54, .8)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(244, 67, 54, 1)'
          }];
          /*
          * Pie Chart Options
          */

          this.pieChartLabels = ['Download Sales', 'In-Store Sales', 'Mail Sales'];
          this.pieChartData = [300, 500, 100];
          this.pieChartType = 'pie';
          this.pieChartColors = [{
            backgroundColor: ['rgba(255, 217, 125, 0.8)', 'rgba(36, 123, 160, 0.8)', 'rgba(244, 67, 54, 0.8)']
          }];
        }

        _createClass(ChartsComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
          /*
          * Bar Chart Event Handler
          */

        }, {
          key: "barChartClicked",
          value: function barChartClicked(e) {}
        }, {
          key: "barChartHovered",
          value: function barChartHovered(e) {}
          /*
          * Line Chart Event Handler
          */

        }, {
          key: "lineChartClicked",
          value: function lineChartClicked(e) {}
        }, {
          key: "lineChartHovered",
          value: function lineChartHovered(e) {}
          /*
          * Doughnut Chart Event Handler
          */

        }, {
          key: "doughnutChartClicked",
          value: function doughnutChartClicked(e) {}
        }, {
          key: "doughnutChartHovered",
          value: function doughnutChartHovered(e) {}
          /*
          * Rader Chart Event Handler
          */

        }, {
          key: "radarChartClicked",
          value: function radarChartClicked(e) {}
        }, {
          key: "radarChartHovered",
          value: function radarChartHovered(e) {}
          /*
          * Pie Chart Event Handler
          */

        }, {
          key: "pieChartClicked",
          value: function pieChartClicked(e) {}
        }, {
          key: "pieChartHovered",
          value: function pieChartHovered(e) {}
        }]);

        return ChartsComponent;
      }();

      ChartsComponent.ɵfac = function ChartsComponent_Factory(t) {
        return new (t || ChartsComponent)();
      };

      ChartsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: ChartsComponent,
        selectors: [["app-charts"]],
        decls: 73,
        vars: 50,
        consts: [["fxLayout", "row wrap"], ["fxFlex", "100", "fxFlex.gt-sm", "33"], [1, "p-0"], [1, ""], [1, "card-title-text"], ["baseChart", "", 1, "chart", 3, "datasets", "labels", "options", "colors", "legend", "chartType"], ["baseChart", "", 1, "chart", 3, "datasets", "labels", "options", "legend", "chartType"], ["baseChart", "", 1, "chart", 3, "data", "labels", "options", "colors", "chartType"], ["baseChart", "", 1, "chart", 3, "data", "labels", "options", "colors", "chartType", "chartHover", "chartClick"], ["baseChart", "", 1, "chart", 3, "datasets", "labels", "legend", "colors", "chartType", "chartHover", "chartClick"]],
        template: function ChartsComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-card", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-card-title", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Vertical Bar chart");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "mat-divider");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-card-content");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "canvas", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "mat-card", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "mat-card-title", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Horizontal Bar chart");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "mat-divider");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "mat-card-content");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "canvas", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "mat-card", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "mat-card-title", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Stacked Bar chart");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "mat-divider");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "mat-card-content");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "canvas", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "mat-card", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "mat-card-title", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Basic Line chart");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "mat-divider");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "mat-card-content");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "canvas", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "mat-card", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "mat-card-title", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "Point Line chart");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "mat-divider");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "mat-card-content");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "canvas", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "mat-card", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "mat-card-title", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "Bubble chart");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "mat-divider");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "mat-card-content");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](48, "canvas", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "mat-card", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "mat-card-title", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "Doughnut chart");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "mat-divider");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "mat-card-content");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "canvas", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "mat-card", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "mat-card-title", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Pie chart");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "mat-divider");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "mat-card-content");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "canvas", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("chartHover", function ChartsComponent_Template_canvas_chartHover_64_listener($event) {
              return ctx.pieChartHovered($event);
            })("chartClick", function ChartsComponent_Template_canvas_chartClick_64_listener($event) {
              return ctx.pieChartClicked($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "mat-card", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "mat-card-title", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "Radar chart");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](70, "mat-divider");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "mat-card-content");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "canvas", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("chartHover", function ChartsComponent_Template_canvas_chartHover_72_listener($event) {
              return ctx.radarChartHovered($event);
            })("chartClick", function ChartsComponent_Template_canvas_chartClick_72_listener($event) {
              return ctx.radarChartClicked($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("datasets", ctx.barChartData)("labels", ctx.barChartLabels)("options", ctx.barChartOptions)("colors", ctx.chartColors)("legend", ctx.barChartLegend)("chartType", ctx.barChartType);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("datasets", ctx.barChartData)("labels", ctx.barChartLabels)("options", ctx.barChartHorizontalOptions)("colors", ctx.chartColors)("legend", ctx.barChartLegend)("chartType", ctx.barChartHorizontalType);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("datasets", ctx.barChartData)("labels", ctx.barChartLabels)("options", ctx.barChartStackedOptions)("colors", ctx.chartColors)("legend", ctx.barChartLegend)("chartType", ctx.barChartType);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("datasets", ctx.lineChartData)("labels", ctx.lineChartLabels)("options", ctx.lineChartOptions)("colors", ctx.chartColors)("legend", ctx.lineChartLegend)("chartType", ctx.lineChartType);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("datasets", ctx.lineChartPointsData)("labels", ctx.lineChartLabels)("options", ctx.lineChartPointsOptions)("colors", ctx.chartColors)("legend", ctx.lineChartLegend)("chartType", ctx.lineChartType);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("datasets", ctx.bubbleChartData)("labels", ctx.bubbleChartLabels)("options", ctx.bubbleChartOptions)("legend", ctx.bubbleChartLegend)("chartType", ctx.bubbleChartType);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.doughnutChartData)("labels", ctx.doughnutChartLabels)("options", ctx.doughnutOptions)("colors", ctx.doughnutChartColors)("chartType", ctx.doughnutChartType);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.pieChartData)("labels", ctx.pieChartLabels)("options", ctx.doughnutOptions)("colors", ctx.doughnutChartColors)("chartType", ctx.pieChartType);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("datasets", ctx.radarChartData)("labels", ctx.radarChartLabels)("legend", false)("colors", ctx.chartColors)("chartType", ctx.radarChartType);
          }
        },
        directives: [_angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_1__["DefaultLayoutDirective"], _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_1__["DefaultFlexDirective"], _angular_material_card__WEBPACK_IMPORTED_MODULE_2__["MatCard"], _angular_material_card__WEBPACK_IMPORTED_MODULE_2__["MatCardTitle"], _angular_material_divider__WEBPACK_IMPORTED_MODULE_3__["MatDivider"], _angular_material_card__WEBPACK_IMPORTED_MODULE_2__["MatCardContent"], ng2_charts__WEBPACK_IMPORTED_MODULE_4__["BaseChartDirective"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL2NoYXJ0cy9jaGFydHMuY29tcG9uZW50LmNzcyJ9 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ChartsComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-charts',
            templateUrl: './charts.component.html',
            styleUrls: ['./charts.component.css']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "./src/app/views/charts/charts.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/views/charts/charts.module.ts ***!
      \***********************************************/

    /*! exports provided: AppChartsModule */

    /***/
    function srcAppViewsChartsChartsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppChartsModule", function () {
        return AppChartsModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/fesm2015/common.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/fesm2015/router.js");
      /* harmony import */


      var _angular_material_card__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/material/card */
      "./node_modules/@angular/material/fesm2015/card.js");
      /* harmony import */


      var _angular_material_list__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/material/list */
      "./node_modules/@angular/material/fesm2015/list.js");
      /* harmony import */


      var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/flex-layout */
      "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
      /* harmony import */


      var ng2_charts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ng2-charts */
      "./node_modules/ng2-charts/fesm2015/ng2-charts.js");
      /* harmony import */


      var _charts_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./charts.component */
      "./src/app/views/charts/charts.component.ts");
      /* harmony import */


      var _charts_routing__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./charts.routing */
      "./src/app/views/charts/charts.routing.ts");

      var AppChartsModule = function AppChartsModule() {
        _classCallCheck(this, AppChartsModule);
      };

      AppChartsModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: AppChartsModule
      });
      AppChartsModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function AppChartsModule_Factory(t) {
          return new (t || AppChartsModule)();
        },
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_material_list__WEBPACK_IMPORTED_MODULE_4__["MatListModule"], _angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__["FlexLayoutModule"], ng2_charts__WEBPACK_IMPORTED_MODULE_6__["ChartsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_charts_routing__WEBPACK_IMPORTED_MODULE_8__["ChartsRoutes"])]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppChartsModule, {
          declarations: [_charts_component__WEBPACK_IMPORTED_MODULE_7__["ChartsComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_material_list__WEBPACK_IMPORTED_MODULE_4__["MatListModule"], _angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__["FlexLayoutModule"], ng2_charts__WEBPACK_IMPORTED_MODULE_6__["ChartsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppChartsModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_material_list__WEBPACK_IMPORTED_MODULE_4__["MatListModule"], _angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__["FlexLayoutModule"], ng2_charts__WEBPACK_IMPORTED_MODULE_6__["ChartsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_charts_routing__WEBPACK_IMPORTED_MODULE_8__["ChartsRoutes"])],
            declarations: [_charts_component__WEBPACK_IMPORTED_MODULE_7__["ChartsComponent"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "./src/app/views/charts/charts.routing.ts":
    /*!************************************************!*\
      !*** ./src/app/views/charts/charts.routing.ts ***!
      \************************************************/

    /*! exports provided: ChartsRoutes */

    /***/
    function srcAppViewsChartsChartsRoutingTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ChartsRoutes", function () {
        return ChartsRoutes;
      });
      /* harmony import */


      var _charts_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./charts.component */
      "./src/app/views/charts/charts.component.ts");

      var ChartsRoutes = [{
        path: '',
        component: _charts_component__WEBPACK_IMPORTED_MODULE_0__["ChartsComponent"],
        data: {
          title: 'Charts'
        }
      }];
      /***/
    }
  }]);
})();
//# sourceMappingURL=views-charts-charts-module-es5.js.map