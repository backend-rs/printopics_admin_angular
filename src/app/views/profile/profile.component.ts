import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { DataService } from 'app/shared/services/dataservice.service';
import { isThisQuarter } from 'date-fns';
import { ApiService } from 'app/shared/services/api.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {

  activeView: string = 'overview';

  // Doughnut
  doughnutChartColors: any[] = [{
    backgroundColor: ['#fff', 'rgba(0, 0, 0, .24)',]
  }];

  total1: number = 500;
  data1: number = 200;
  doughnutChartData1: number[] = [this.data1, (this.total1 - this.data1)];

  total2: number = 1000;
  data2: number = 400;
  doughnutChartData2: number[] = [this.data2, (this.total2 - this.data2)];

  doughnutChartType = 'doughnut';
  doughnutOptions: any = {
    cutoutPercentage: 85,
    responsive: true,
    maintainAspectRatio: true,
    legend: {
      display: false,
      position: 'bottom'
    },
    elements: {
      arc: {
        borderWidth: 0,
      }
    },
    tooltips: {
      enabled: false
    }
  };
  user: any;
  userId: any;
  currentUser: string;
  email: any;
  phoneNumber: any;
  userImg: any;
  isLoading: boolean = false;

  constructor(private router: ActivatedRoute, private route: Router, private dataservice: DataService,
    private apiservice: ApiService) {
    this.user = dataservice.getOption();
  }

  resetPasswordLink(data) {
    this.route.navigate(['profile/settings']);
  }

  getUserById() {
    this.isLoading = true;
    this.apiservice.getCurrentUser({ id: this.user._id }).subscribe((res: any) => {
      this.currentUser = res.data.userName
      this.phoneNumber = res.data.phoneNumber
      this.email = res.data.email
      this.userImg = res.data.avatarImages
      this.isLoading = false;
    });
  }

  ngOnInit() {
    this.userId = localStorage.userId
    this.activeView = this.router.snapshot.params['view']
    this.getUserById();
  }

  back() {
    if (this.user.role == 'P') {
      this.route.navigate(['tables/photographer']);
    } else {
      this.route.navigate(['tables/filter']);
    }
  }
}