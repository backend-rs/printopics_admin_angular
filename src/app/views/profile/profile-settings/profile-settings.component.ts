import { Component, OnInit } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { Router } from '@angular/router';
import { ApiService } from 'app/shared/services/api.service';
import { DataService } from 'app/shared/services/dataservice.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-profile-settings',
  templateUrl: './profile-settings.component.html',
  styleUrls: ['./profile-settings.component.css']
})
export class ProfileSettingsComponent implements OnInit {
  public uploader: FileUploader = new FileUploader({ url: 'upload_url' });
  public hasBaseDropZoneOver: boolean = false;
  user: any;
  userResponse: any;

  show: boolean = true;
  hide: boolean = true;

  userPass: any = {};
  formData = new FormData();
  imgResponse: any;
  fileData: any = [];
  msg: string;
  imagePath;
  imgURL: any;
  isLoading: boolean = false;

  genders: any[] = [
    { name: 'Male', value: 'male' },
    { name: 'Female', value: 'female' },
    { name: 'Other', value: 'other' }
  ];

  imgsURL: any;
  allfileData: any = [];
  profileShow: any;

  constructor(
    private dataservice: DataService,
    private route: Router,
    private apiservice: ApiService,
    private snack: MatSnackBar,
  ) {
    this.user = dataservice.getOption();
    this.profileShow = this.user.isCurrent
  }

  ngOnInit() {
    this.getUserById()
  }

  getUserById() {
    this.isLoading = true;
    this.apiservice.getCurrentUser({ id: this.user._id }).subscribe((res: any) => {
      this.user = res.data
      this.isLoading = false;
    });
  }

  resetPassword(data) {
    let model = {
      _id: this.user._id,
      oldPassword: data.oldPassword,
      newPassword: data.newPassword
    }
    this.apiservice.resetPassword(model).subscribe(res => {
      this.userResponse = res;
      if (this.userResponse.isSuccess === true) {
        this.snack.open('Password Updated Successfully!', 'OK', { duration: 4000 });
        if (this.userResponse.role === 'P') {
          this.route.navigate(['tables/photographer']);
        } else {
          this.route.navigate(['tables/filter']);
        }
      } else {
        let msg = "Something Went Wrong";
        this.snack.open(msg, 'OK', { duration: 4000 });
      }
    });
  }

  updateUser(data) {
    this.apiservice.updateUser(data).subscribe(res => {
      this.userResponse = res;
      if (this.userResponse.isSuccess === true) {
        this.snack.open('User Updated Successfully!', 'OK', { duration: 4000 })
        if (this.userResponse.role === 'P') {
          this.route.navigate(['tables/photographer']);
        } else {
          this.route.navigate(['tables/filter']);
        }
      } else {
        let msg = "Something Went Wrong";
        this.snack.open(msg, 'OK', { duration: 4000 });
      }
    });
  }

  uploadProfile() {
    this.isLoading = true;
    this.apiservice.uploadUserImage(this.user._id, this.formData).subscribe(res => {
      this.imgResponse = res;
      if (this.imgResponse.isSuccess === true) {
        this.snack.open('Image Uploaded Successfully !', 'OK', { duration: 4000 });
        location.reload();
        // this.route.navigateByUrl('/profile', { skipLocationChange: true }).then(() => {
        //   this.route.navigate(['profile/settings']);
        //   this.isLoading = false;
        // })
      } else {
        let msg = "Something Went Wrong!";
        this.snack.open(msg, 'OK', { duration: 4000 });
        this.isLoading = false;
      }
    });

  };

  uploadImages() {
    this.isLoading = true;
    this.apiservice.uploadImages(this.user._id, this.formData).subscribe(res => {
      this.imgResponse = res;
      if (this.imgResponse.isSuccess === true) {
        this.snack.open('Image Uploaded Successfully !', 'OK', { duration: 4000 });
        location.reload();
      } else {
        let msg = "Something Went Wrong!";
        this.snack.open(this.imgResponse.error, 'OK', { duration: 4000 });
        this.isLoading = false;
      }
    });

  };

  deleteImage(data) {
    let model = {
      userId: this.user._id,
      imageId: data._id
    }
    this.isLoading = true;
    this.apiservice.deleteImage(model).subscribe(res => {
      this.imgResponse = res;
      if (this.imgResponse.isSuccess === true) {
        this.snack.open('Image Delete Successfully!!!', 'OK', { duration: 4000 });
        location.reload();
      } else {
        this.snack.open(this.imgResponse.error, 'OK', { duration: 4000 });
      }
      this.isLoading = false;
    });

  };


  // filesSelected($event) {
  //   for (let file of $event.target.files) {
  //     this.allfileData.push(file)
  //     this.fileData = file
  //     // this.fileData = this.allfileData;
  //     if ($event.target.files.length === 0)
  //       return;
  //     var reader = new FileReader();
  //     this.imagePath = file;
  //     reader.readAsDataURL(file);
  //     reader.onload = (_event) => {
  //       this.imgsURL = reader.result;
  //     }
  //     var mimeType = file.type;
  //     if (mimeType.match(/image\/*/) == null) {
  //       this.msg = " only images are supported";
  //       return
  //     }
  //     this.uploadImages();
  //   }
  // }

  filesSelected(event) {
        this.fileData = event.target.files[0];
        this.formData.append('image', this.fileData);
        if (event.target.files.length === 0)
          return;
        var reader = new FileReader();
        this.imagePath = event.target.files;
        reader.readAsDataURL(event.target.files[0]);
        reader.onload = (_event) => {
          this.imgsURL = reader.result;
        }
        var mimeType = event.target.files[0].type;
        if (mimeType.match(/image\/*/) == null) {
          this.msg = " only images are supported";
          return
        }
        this.uploadImages();
      }

  fileSelect(event) {
    this.fileData = event.target.files[0];
    this.formData.append('image', this.fileData);

    if (event.target.files.length === 0)
      return;
    var reader = new FileReader();
    this.imagePath = event.target.files;
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
    var mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.msg = " only images are supported";
      return;
    }

  }


  back() {
    if (this.user.role == 'P') {
      this.route.navigate(['tables/photographer']);
    } else {
      this.route.navigate(['tables/filter']);
    }
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

}
