import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatProgressBar } from '@angular/material/progress-bar';
import { MatButton } from '@angular/material/button';
import { ApiService } from 'app/shared/services/api.service';
import { AnonymousSubscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  @ViewChild(MatProgressBar, { static: false }) progressBar: MatProgressBar;
  @ViewChild(MatButton, { static: false }) submitButton: MatButton;

  signinForm: FormGroup;
  credentials = {
    email: '',
    password: '',
  };
  isLoading: boolean = false;
  userData: any = {};
  constructor(
    private router: Router,
    private apiservice: ApiService,
  ) { }

  
  signIn() {
    this.isLoading = true
    this.apiservice.login(this.credentials).subscribe((res:any) => {
      this.userData = res;
      if (this.userData.isSuccess === true && this.userData.data.role === "SA") {
        localStorage.setItem('apiToken', this.userData.data.token);
        localStorage.setItem('userId', this.userData.data.id);
        this.isLoading = false;
        this.router.navigate(['dashboard/analytics']);
      }
      else {
        alert('Wrong username or password!');
        return this.isLoading = false;
      };
    });
  }

  ngOnInit() {
    this.signinForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      rememberMe: new FormControl(false)
    });
  }

}
