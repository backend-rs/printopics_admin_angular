import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { TablesService } from 'app/views/tables/tables.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'app/shared/services/dataservice.service';
import { ApiService } from 'app/shared/services/api.service';

@Component({
  selector: 'app-user-manage',
  templateUrl: './user-manage.component.html',
  styleUrls: ['./user-manage.component.scss']
})
export class UserManageComponent implements OnInit {

  formData = {};
  userFrom: FormGroup;
  user: any = {
    userName: '',
    // lastName: '',
    email: '',
    password: '',
    phoneNumber: '',
    sex: '',
    addressLine1: '',
    role: '',
    city: '',
    country: '',
    zipCode: '',
    // lat: '',
    // long: '',
    // stripeToken: '',
    // stripeKey: '',
    // ssn: '',
    // deviceToken: '',
  };

  roles: any[] = [
    { name: 'SuperAdmin', value: 'SA' },
    { name: 'Admin', value: 'A' },
    { name: 'User', value: 'U' }
  ];

  genders: any[] = [
    { name: 'Male', value: 'male' },
    { name: 'Female', value: 'female' },
    { name: 'Other', value: 'other' }
  ];

  FormData: any;
  submitted: boolean;
  usersData: any = {};
  responseData: any;
  res: any[];
  show: boolean = true;

  message: string = 'User Added Successfully!';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  userResponse: any;

  constructor(private service: TablesService,
    public dataRoute: ActivatedRoute,
    private dataservice: DataService,
    private apiservice: ApiService,
    private snack: MatSnackBar,
    private route: Router) {
    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;
  }

  back() {
    this.route.navigate(['tables/filter']);
  }
  
  ngOnInit() {
    this.user = this.dataservice.getOption();

    this.userFrom = new FormGroup({
      userName: new FormControl('', Validators.required),
      // lastName: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      password: new FormControl(''),
      phoneNumber: new FormControl('', Validators.required),
      sex: new FormControl('', Validators.required),
      addressLine1: new FormControl(''),
      role: new FormControl('', Validators.required),
      city: new FormControl(''),
      country: new FormControl(''),
      zipCode: new FormControl(''),
      // lat: new FormControl(''),
      // long: new FormControl(''),
      // stripeToken: new FormControl(''),
      // stripeKey: new FormControl(''),
      // ssn: new FormControl('',),
      // deviceToken: new FormControl(''),

    });
  }

  addUsers() {
    this.apiservice.addUsers(this.user).subscribe(res => {
      this.userResponse = res;
      console.log('response from server = > ', this.userResponse);

      if (this.userResponse.isSuccess === true && this.userResponse.data !== "Email already resgister") {
        this.snack.open(this.message, 'OK', { duration: 4000 })
        this.route.navigate(['tables/filter']);
      } else {
        if (this.userResponse.isSuccess === true && this.userResponse.data === "Email already resgister") {
          this.snack.open(this.userResponse.data, 'OK', { duration: 4000 })
        }
        else {
          let msg = "Something Went Wrong";
          this.snack.open(msg, 'OK', { duration: 4000 });
        }

      }
    });
  }

  updateUser() {
    this.apiservice.updateUser(this.user).subscribe(res => {
      this.userResponse = res;
      if (this.userResponse.isSuccess === true) {
        this.snack.open('User Updated Successfully!', 'OK', { duration: 4000 })
        this.route.navigate(['tables/filter']);
      } else {
        let msg = "Something Went Wrong";
        this.snack.open(msg, 'OK', { duration: 4000 });
      }
    });
  }

  onSubmit() {
    this.submitted = true;
    if (!this.user._id) {
      return this.addUsers();
    } else if (this.user._id) {
      return this.updateUser();
    }
  }
}

