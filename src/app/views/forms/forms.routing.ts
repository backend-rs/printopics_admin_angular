import { Routes } from '@angular/router';

import { BasicFormComponent } from './basic-form/basic-form.component';
import { RichTextEditorComponent } from './rich-text-editor/rich-text-editor.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { WizardComponent } from './wizard/wizard.component';
import { UserManageComponent } from './user-manage/user-manage.component';
import { PhotographerManageComponent } from './photographer-manage/photographer-manage.component';

export const FormsRoutes: Routes = [
  {
    path: '',
    children: [{
      path: 'basic',
      component: BasicFormComponent,
      data: { title: 'Basic', breadcrumb: 'BASIC' }
    }, {
      path: 'editor',
      component: RichTextEditorComponent,
      data: { title: 'Editor', breadcrumb: 'EDITOR' }
    }, {
      path: 'upload',
      component: FileUploadComponent,
      data: { title: 'Upload', breadcrumb: 'UPLOAD' }
    }, {
      path: 'wizard',
      component: WizardComponent,
      data: { title: 'Wizard', breadcrumb: 'WIZARD' }
    }, {
      path: 'user',
      component: UserManageComponent,
      data: { title: 'User', breadcrumb: 'User' }
    }, {
      path: 'photographer',
      component: PhotographerManageComponent,
      data: { title: 'User', breadcrumb: 'Photographer' }
    }]
  }
];