import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotographerManageComponent } from './photographer-manage.component';

describe('PhotographerManageComponent', () => {
  let component: PhotographerManageComponent;
  let fixture: ComponentFixture<PhotographerManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotographerManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotographerManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
