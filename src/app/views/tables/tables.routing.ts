import { Routes } from '@angular/router';

import { FullscreenTableComponent } from './fullscreen-table/fullscreen-table.component';
import { PagingTableComponent } from './paging-table/paging-table.component';
import { FilterTableComponent } from './filter-table/filter-table.component';
import { MaterialTableComponent } from './material-table/material-table.component';
import { UserReportsComponent } from './user-reports/user-reports.component';
import { PermissionsComponent } from './permissions/permissions.component';
import { PhotographerComponent } from './photographer/photographer.component';
import { OrderReportsComponent } from './order-reports/order-reports.component';

export const TablesRoutes: Routes = [
  {
    path: '',
    children: [{
      path: 'fullscreen',
      component: FullscreenTableComponent,
      data: { title: 'Fullscreen', breadcrumb: 'FULLSCREEN' }
    }, {
      path: 'paging',
      component: PagingTableComponent,
      data: { title: 'Paging', breadcrumb: 'PAGING' }
    }, {
      path: 'user',
      component: UserReportsComponent,
      data: { title: 'Reports', breadcrumb: 'UserReports' }
    }, {
      path: 'order',
      component: OrderReportsComponent,
      data: { title: 'Reports', breadcrumb: 'OrderReports' }
    }, {
      path: 'permissions',
      component: PermissionsComponent,
      data: { title: 'Permissions', breadcrumb: 'Permissions' }
    }, {
      path: 'photographer',
      component: PhotographerComponent,
      data: { title: 'Photographer', breadcrumb: 'Photographer' }
    }, {
      path: 'filter',
      component: FilterTableComponent,
      data: { title: 'Filter', breadcrumb: 'User' }
    }, {
      path: 'mat-table',
      component: MaterialTableComponent,
      data: { title: 'Material TAble', breadcrumb: 'Material Table' }
    }]
  }
];
