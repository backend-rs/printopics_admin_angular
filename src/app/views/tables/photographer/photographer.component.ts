import { Component, OnInit } from '@angular/core';
import { TablesService } from '../tables.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DataService } from 'app/shared/services/dataservice.service';
import { AppConfirmService } from 'app/shared/services/app-confirm/app-confirm.service';
import { ApiService } from 'app/shared/services/api.service';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';

@Component({
  selector: 'app-photographer',
  templateUrl: './photographer.component.html',
  styleUrls: ['./photographer.component.scss']
})
export class PhotographerComponent implements OnInit {
  rows: any = [];
  columns: any = [];
  temp: any = [];

  isLoading: boolean;
  usersData: any = {};

  pageSize: number;
  pageNo = 1;
  submitted: any;
  userResponse: any;
  message: string = 'User Deleted Successfully!';
  list: any = [];
  getAllUser: any = [];


  constructor(
    private service: TablesService,
    public route: Router,
    private snack: MatSnackBar,
    private dataservice: DataService,
    private confirmService: AppConfirmService,
    private apiservice: ApiService,
    private loader: AppLoaderService
  ) { }

  ngOnInit() {
    sessionStorage.clear();
    this.getUsers()
  }

  getUsers() {
    this.isLoading = true;
    this.loader.open();
    this.apiservice.getUsers().subscribe((res) => {
      this.loader.close();
      this.list = res
      this.list.data.forEach(element => {
        if(element.role == 'P'){
          this.getAllUser.push(element)
        }
      });
      this.temp = this.getAllUser;
      this.rows = this.temp;
      this.isLoading = false;
    });
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    var columns = Object.keys(this.temp[0]);
    columns.splice(columns.length - 1);

    if (!columns.length)
      return;

    const rows = this.temp.filter(function (d) {
      for (let i = 0; i <= columns.length; i++) {
        let column = columns[i];
        if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
          return true;
        }
      }
    });
    this.rows = rows;
  }


  deleteUser(data) {
    this.confirmService.confirm({ message: `Delete ${data.userName}?` }).subscribe(res => {
      if (res) {
        this.loader.open();
        this.isLoading = true;
        this.apiservice.deleteUser({ id: data._id }).subscribe(res => {
          this.userResponse = res;
          if (this.userResponse.isSuccess === true) {
            this.loader.close();
            this.snack.open(this.message, 'OK', { duration: 4000 });
            this.route.navigateByUrl('/tables', { skipLocationChange: true }).then(() => {
              this.route.navigate(['tables/filter']);
            })
          } else {
            this.loader.close();
            let msg = "Something Went Wrong!";
            this.snack.open(msg, 'OK', { duration: 4000 });
          }
        })
      }
    })
  }

  userProfile(data) {
    data.isSelect = false;
    this.dataservice.setOption(data);
    this.route.navigate(['profile/settings']);
  }

  add() {
    let data: any = {};
    data.isSelect = true;
    this.dataservice.setOption(data);
    this.route.navigate(['forms/photographer']);
  }

  edit(data) {
    data.isSelect = false;
    this.dataservice.setOption(data);
    this.route.navigate(['forms/photographer']);
  }
}
