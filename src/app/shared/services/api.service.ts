import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { User } from '../models/user.model';
import { Category } from '../models/category.model';
import { Tag } from '../models/tag.model';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    root = environment.apiURL;
    userResponse: any;
    usersData: any;
    categoryResponse: any;
    tagResponse: any;
    token: string = ''


    constructor(private http: HttpClient) {
        this.token = localStorage.getItem('apiToken');
    }
    // --------------------access token------------------------
    getHeaders() {
        let header
        if (this.token != '') {
            header = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'x-access-token': this.token
                })
            }
        } else {
            console.log('token not found')
        }
        return header;

    }
    // header = {
    //     headers: new HttpHeaders({
    //         'Content-Type': 'application/json',
    //         'x-access-token': this.token
    //     })

    // };

    login(model): Observable<User> {
        const subject = new Subject<User>();
        this.http.post(`${this.root}/users/login`, model, { headers: null }).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }
    addCategory(model): Observable<Category[]> {
        const subject = new Subject<Category[]>();
        this.http.post(`${this.root}/categories/add`, model, this.getHeaders()).subscribe((responseData) => {
            this.categoryResponse = responseData;
            subject.next(this.categoryResponse);
            console.log('add category response', this.categoryResponse);
            console.log('response', responseData)
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();

    }


    addTag(model): Observable<Tag[]> {
        const subject = new Subject<Tag[]>();
        this.http.post(`${this.root}/tags/add`, model, this.getHeaders()).subscribe((responseData) => {
            this.tagResponse = responseData;
            subject.next(this.tagResponse);
            console.log('add category response', this.tagResponse);
            console.log('response', responseData)
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();

    }
    getCategory(): Observable<Category[]> {
        const subject = new Subject<Category[]>();
        this.http.get(`${this.root}/categories/list`, this.getHeaders()).subscribe((responseData) => {
            this.categoryResponse = responseData;
            console.log('get category API=>>', responseData);
            subject.next(this.categoryResponse.data);

        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getTag(): Observable<Tag[]> {
        const subject = new Subject<Tag[]>();
        this.http.get(`${this.root}/tags/list`, this.getHeaders()).subscribe((responseData) => {
            this.tagResponse = responseData;
            console.log('get category API=>>', responseData);
            subject.next(this.tagResponse.data);

        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getUsers(): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.get(`${this.root}/users/getUsers?pageNo=1&pageSize=1000`, { headers: null }).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getUserById(id): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.get(`${this.root}/users/getById/${id}`, this.getHeaders()).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse.items);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    getCurrentUser(data): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.post(`${this.root}/users/getById`, data, { headers: null }).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    deleteImage(data): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.post(`${this.root}/users/deleteImage`, data, { headers: null }).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    uploadUserImage(id, model): Observable<any> {
        const subject = new Subject<any>();
        this.http.put(`${this.root}/users/uploadProfilePic/${id}`, model, {
            headers: new HttpHeaders({
                'enctype': 'multipart/form-data',
                'Accept': 'application/json',
                'x-access-token': this.token
            })
        }).subscribe((response) => {
            // this.tagResponse = response;
            subject.next(response);
            // console.log('add category response', this.tagResponse);
            console.log('response', response)
        }, (error) => {
            console.log('error', error)
            subject.next(error.error);
        }
        );
        return subject.asObservable();

    }

    uploadImages(id, model): Observable<any> {
        const subject = new Subject<any>();
        this.http.put(`${this.root}/users/uploadPics/${id}`, model, {
            headers: new HttpHeaders({
                'enctype': 'multipart/form-data',
                'Accept': 'application/json',
                'x-access-token': this.token
            })
        }).subscribe((response) => {
            subject.next(response);
            console.log('response', response)
        }, (error) => {
            console.log('error', error)
            subject.next(error.error);
        }
        );
        return subject.asObservable();

    }

    addUsers(data): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.post(`${this.root}/users/create`, data, this.getHeaders()).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    getParents(): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.get(`${this.root}/users/list?pageNo=1&pageSize=1000`, { headers: null }).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse.items);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    
    deleteUser(id): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.post(`${this.root}/users/delete`, id, { headers: null }).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse);

        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    updateUser(model): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.post(`${this.root}/users/update`, model, { headers: null }).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    
    resetPassword(model): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.post(`${this.root}/users/changePassword`, model, this.getHeaders()).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    getChild(): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.get(`${this.root}/child/list`, this.getHeaders()).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse.items);

        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    updateCategory(_id, model): Observable<Category[]> {
        const subject = new Subject<Category[]>();
        this.http.put(`${this.root}/categories/update/${_id}`, model, this.getHeaders()).subscribe((responseData) => {
            this.categoryResponse = responseData;
            console.log('categoryupdate rwsponse>>>>>>', responseData);

            subject.next(this.categoryResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();

    }

    updateTag(_id, model): Observable<Tag[]> {
        const subject = new Subject<Tag[]>();
        this.http.put(`${this.root}/tags/update/${_id}`, model, this.getHeaders()).subscribe((responseData) => {
            this.tagResponse = responseData;
            // console.log('userslist', responseData);

            subject.next(this.tagResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();

    }
}

