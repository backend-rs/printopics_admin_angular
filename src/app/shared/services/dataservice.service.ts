import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    private data: any = {};

    setOption(option) {
        this.data = option;
        sessionStorage.setItem('currentUser', JSON.stringify(this.data));
    }

    getOption() {
        if (this.data.isSelect) {
            return this.data = {}
        } else if (sessionStorage.currentUser) {
            return this.data = JSON.parse(sessionStorage.getItem('currentUser'))
        } else {
            return this.data;
        }
    }


    constructor() { }
}
